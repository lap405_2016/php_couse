<?php
  $page = 15;
	$the_title = 'Cookies';
	$the_content = '<p>Cookies เป็นtext file ที่มีการเก็บไว้ใน Client computer และมันถูกเก็บเพื่อจุดประสงค์ในการติดตาม PHP transparently สนับสนุน HTTP Cookies</p>     
	                <p>มี 3 ขั้นตอนในการระบุผู้ใช้งาน</p>
	                <p><ul>1) Server Script จะส่งชุดของ Cookie ไปยัง Browser เช่น ชื่อ อายุ </ul></p>
	                <p><ul>2) Server จะเก็บข้อมูลไว้บนเครื่องเพื่อใช้ในอนาคต</ul></p>
	                <p><ul>3) เมื่อเบราว์เซอร์ในครั้งต่อไปจะส่งคำขอไปยังเว็บเซิร์ฟเวอร์ใด ๆ แล้วมันจะส่งข้อมูลคุกกี้เหล่านั้นไปยังเซิร์ฟเวอร์และเซิร์ฟเวอร์ใช้ข้อมูลเพื่อระบุผู้ใช้</ul></p>
	                <p>บทนี้จะสอนวิธีการตั้งค่าคุกกี้, วิธีการเข้าถึงพวกเขาและวิธีการที่จะลบออก</p>
	                <h3><b>The Anatomy of a Cookie</b></h3>
	                <p>Cookie มักจะตั้งค่าใน HTTP header (แม้ว่า JavaScript จะสามารถตั้งค่าโดยตรงบน Browser) PHP Script ซึ่งตั้งค่า Cookie อาจจะส่ง headerที่มีลักษณะบางอย่าง เช่นนี้</p>
	                <pre class="result notranslate">
HTTP/1.1 200 OK
Date: Fri, 04 Feb 2000 21:03:38 GMT
Server: Apache/1.3.9 (UNIX) PHP/4.0b3
Set-Cookie: name=xyz; expires=Friday, 04-Feb-07 22:03:38 GMT; 
                 path=/; domain=tutorialspoint.com
Connection: close
Content-Type: text/html
</pre> 
                    <p>จากที่เห็น Set-Cookie มี name คู่กับ value,GMT date,path และ domain โดย name และ value จะเข้ารหัส URL   the expired field จะสั่งให้Browserลืมคุกกี้หลังจากเวลาที่ให้</p>
                    <p>ถ้า Browser มีการตั้งค่าให้เก็บ Cookie จนถึงวันหมดอายุ หากผู้ใช้Browserชี้ที่หน้าเว็บใด ๆ ที่ตรงกับPath และ Domain ของ Cookie มัจจะส่ง Cookie อีกครั้งถึง Server  header ของ Browser อาจจะมีลักษณะเช่นนี้</p>
	               <pre class="result notranslate">
GET / HTTP/1.0
Connection: Keep-Alive
User-Agent: Mozilla/4.6 (X11; I; Linux 2.2.6-15apmac ppc)
Host: zink.demon.co.uk:1126
Accept: image/gif, */*
Accept-Encoding: gzip
Accept-Language: en
Accept-Charset: iso-8859-1,*,utf-8
Cookie: name=xyz
</pre> 
                    <p>PHP script จะเข้าถึง Cookie ในตัวแปร $_COOKIE หรือ $HTTP_COOKIE_VARS[] ซึ่งถือเป็น Cookie name and value ทั้งหมด ในข้างต้น สามารถเข้าถึง Cookie ได้โดยใช้ $HTTP_COOKIE_VARS["name"]
                    <h3><b>Setting Cookies with PHP</b></h3>
                    <p>PHP มีฟังก์ชันที่ใช้ตั้งค่า Cookie คือ setcookie () โดยมีพารามิเตอร์อยู่ 6 ตัว และต้องเรียกกว่า tag html</p>
                    <pre class="result notranslate">
setcookie(name, value, expire, path, domain, security);
</pre> 
                    <p><ul><b>name:</b>  ชื่อของ Cookie และจะถูกเก็บไว้ในตัวแปรที่เรียกว่า HTTP_COOKIE_VARS ตัวแปรนี้ใช้ในขณะที่เข้าถึง Cookie</ul></p>
                    <p><ul><b>value:</b> เป็นค่าของตัวแปร name และเป็นเนื้อหาที่ต้องการจัดเก็บไว้</ul></p>
                    <p><ul><b>Expiry:</b> ระบุเวลาในอนาคต ในหน่วย วินาทีตั้งแต่  00:00:00 GMT เมื่อ 1 มกราคม 1970 หลังจากเวลานี้ Cookie จะไม่สามารถเข้าถึงได้ ถ้าค่านี้ไม่ได้มีการตั้งค่า มันจะหมดอายุเมื่อมีการ    ปิด Browser</ul></p>
                    <p><ul><b>Path:</b> ระยุ directory ที่ Cookie ใช้ได้ / คือการที่อนุญาต Cookie ใช้งานได้ทุก directory</ul></p>
                    <p><ul><b>Domain:</b> ใช้ระบุโดเมน ในโดเมนกว้างๆ และต้องมีอย่างน้อย 2 periods ในการใช้งานถูกต้อง Cookie ทั้งหมดที่ใช้ได้เท่านั้นสำหรับ host และ domain ซึ่งสร้างมัน</ul></p>
                    <p><ul><b>Security:</b> ตั้งค่าเป็น 1 เพื่อระบุให้ Cookie ถูกส่งด้วย HTTPS ถ้าเคั้งค่าเป็น 0 คือจะส่งด้วย HTTP ทั่วไป</ul></p>
                    <p>ตัวอย่าง</p>
                    <pre class="prettyprint notranslate">
&lt;?php
   setcookie("name", "John Watkin", time()+3600, "/","", 0);
   setcookie("age", "36", time()+3600, "/", "",  0);
?&gt;
&lt;html&gt;
   
   &lt;head&gt;
      &lt;title&gt;Setting Cookies with PHP&lt;/title&gt;
   &lt;/head&gt;
   
   &lt;body&gt;
      &lt;?php echo "Set Cookies"?&gt;
   &lt;/body&gt;
   
&lt;/html&gt;
</pre> 
                    <h3><b>Accessing Cookies with PHP</b></h3>
                    <p>PHP มีหลายทางในการเข้าถึง Cookies ซึ่งวิธีง่ายที่สุด คือใช้ $_COOKIE หรือ $HTTP_COOKIE_VARS</p>
                    <p>ตัวอย่าง</p>
                    <pre class="prettyprint notranslate">
&lt;html&gt;
   
   &lt;head&gt;
      &lt;title&gt;Accessing Cookies with PHP&lt;/title&gt;
   &lt;/head&gt;
   
   &lt;body&gt;
      
      &lt;?php
         echo $_COOKIE["name"]. "&lt;br /&gt;";
         
         /* is equivalent to */
         echo $HTTP_COOKIE_VARS["name"]. "&lt;br /&gt;";
         
         echo $_COOKIE["age"] . "&lt;br /&gt;";
         
         /* is equivalent to */
         echo $HTTP_COOKIE_VARS["name"] . "&lt;br /&gt;";
      ?&gt;
      
   &lt;/body&gt;
&lt;/html&gt;
</pre> 
                     <p>สามารถใช้ <b>isset()</b> ในการเช็คว่ามีการตั้งค่าหรือยัง </p>
                     <pre class="prettyprint notranslate">
&lt;html&gt;
   
   &lt;head&gt;
      &lt;title&gt;Accessing Cookies with PHP&lt;/title&gt;
   &lt;/head&gt;
   
   &lt;body&gt;
      
      &lt;?php
         if( isset($_COOKIE["name"]))
            echo "Welcome " . $_COOKIE["name"] . "&lt;br /&gt;";
         
         else
            echo "Sorry... Not recognized" . "&lt;br /&gt;";
      ?&gt;
      
   &lt;/body&gt;
&lt;/html&gt;
</pre>
                      <h3><b>Deleting Cookie with PHP</b></h3>
                      <p>การลบ Cookie เรียก setcookie() ด้วย name อย่างเดียว แต่มันทำงานได้ไม่ดี อย่างไรก็ตามไม่ควรจะเป็นที่พึ่ง</p>
                      <p>ปลอดภัยที่สุดคือตั้งค่า วันหมดอายุให้มีวันหมดอายุ</p>
                      <pre class="prettyprint notranslate">
&lt;?php
   setcookie( "name", "", time()- 60, "/","", 0);
   setcookie( "age", "", time()- 60, "/","", 0);
?&gt;
&lt;html&gt;
   
   &lt;head&gt;
      &lt;title&gt;Deleting Cookies with PHP&lt;/title&gt;
   &lt;/head&gt;
   
   &lt;body&gt;
      &lt;?php echo "Deleted Cookies" ?&gt;
   &lt;/body&gt;
   
&lt;/html&gt;
</pre> 
';
?>

<?php include('single.php'); ?>