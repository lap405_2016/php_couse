<?php
	$page = 7;
	$the_title = 'Loop Types';
	$the_content = '<p><ul>ใน PHP ลูปจะเป็นการประมวลผลบล็อคของโค้ดเดิมด้วยจำนวนที่จำกัด ซึ่ง PHP รองรับลูป 4 ประเภท คือ</ul></p>
	<ul><ul><p><li><b>for</b> - ลูปที่ประมวลผลบล็อคของโค้ดเดิมด้วยจำนวนที่จำกัด </li></p>
	<p><li><b>while</b> - ลูปที่ประมวลผลบล็อคของโค้ดเดิมถ้าในขณะนั้นเงื่อนไขที่กำหนดยังเป็นจริง  </li></p>
	<p><li><b>do...while</b> - ลูปที่ประมวลผลบล็อคของโค้ดเดิมหนึ่งครั้ง และจะทำซ้ำเมื่อเงื่อนไขที่กำหนดยังเป็นจริง </li></p>
	<p><li><b>foreach</b> - ลูปที่ประมวลผลบล็อคของโค้ดเดิมสำหรับแต่ละองค์ประกอบใน array </li></p></ul></ul>
	ซึ่งจะมี <b>continue</b> และ  <b>break</b> ที่ใช้ในการควบคุมการประมวลผลของลูป<br>
	
	<h3><b>The for loop statement</b></h3>
	<p><ul>For statement ใช้เมื่อทราบจำนวนรอบที่แน่ชัดในการประมวลบล็อคของโค้ด</ul><p>
	<p><ul><img src="images/php_for_loop.jpg"></ul></p>
	<h4><b>Syntax</b></h4>
	<p><pre>for (initialization; condition; increment){
	<ul>code to be executed;</ul>}</pre></p>
	<p>Initializer ใช้สำหรับตั้งค่า ค่าเริ่มต้นสำหรับเป็นตัวนับจำนวนรอบของลูป ตัวแปรจะถูกประกาศที่นี่เพื่อจุดประสงค์นี้ โดยทั่วไปนิยมตั้งชื่อเป็น $i</p>
	<h4><b>Example</b></h4>
	<p>ตัวอย่างนี้จะเป็นการสร้างการทำซ้ำ 5 รอบ และเปลี่ยนแปลงการค่าของตัวแปร 2 ตัว ในการทำซ้ำในแต่ละรอบ</p>
	<p><pre>&lt;?php
	<ul>$a = 0;<br>$b = 0;<br><br><z style="color:#0000FF;">for</z>( $i = 0; $i<5; $i++ ) {<br><ul>$a += 10;<br>$b += 5;</ul>}<br><br>echo (<z style="color:#009900;">"At the end of the loop a = $a and b = $b"</z>);</ul>?&gt;</pre></p>
	<p>ซึ่งจะทำให้ได้ผลลัพธ์ คือ</p>
	<p><pre>At the end of the loop a = 50 and b = 25</pre></p>
	
	<h3><b>The while loop statement</b></h3>
	<p><ul>While statement จะประมวลผลบล็อคของโค้ดเดิมถ้าในขณะนั้นเงื่อนไขที่กำหนดยังเป็นจริง</ul><p>
	<p><ul>ถ้าพบว่าการทดสอบเงื่อนไขเป็นจริง บล็อคของโค้ดจะถูกประมวลผล หลังจากนั้นจะทดสอบเงื่อนไขอีกครั้งหนึ่งและทำซ้ำไปเรื่อยๆจนกว่่าการทดสอบเงื่อนไขจะเป็นเท็จ</ul><p>
	<p><ul><img src="images/php_while_loop.jpg"></ul></p>
	<h4><b>Syntax</b></h4>
	<p><pre>while (condition) {
	<ul>code to be executed;</ul>}</pre></p>
	<h4><b>Example</b></h4>
	<p>ตัวอย่างนี้จะเป็นการลดค่าของตัวแปรในการทำซ้ำในแต่ละรอบของลูป และเมื่อตัวนับนับจนถึง 10 เมื่อการทดสอบเงื่อนไขเป็นเท็จลูปก็จะสิ้นสุด</p>
	<p><pre>&lt;?php
	<ul>$i = 0;<br>$num = 50;<br><br><z style="color:#0000FF;">while</z>( $i < 10) {<br><ul>$num--;<br>$i++;</ul>}<br><br>echo (<z style="color:#009900;">"Loop stopped at i = $i and num = $num"</z>);</ul>?&gt;</pre></p>
	<p>ซึ่งจะทำให้ได้ผลลัพธ์ คือ</p>
	<p><pre>Loop stopped at i = 10 and num = 40 </pre></p>
	
	<h3><b>The do...while loop statement</b></h3>
	<p><ul>Do...while statement จะประมวลผลบล็อคของโค้ดอย่างน้อยหนึ่งครั้ง  และหลังจากนั้นจะทำซ่ำไปเรื่อยๆขณะที่เงื่อนไขเป็นจริง</ul><p>
	<h4><b>Syntax</b></h4>
	<p><pre>do {
	<ul>code to be executed;</ul>}<br>while (condition);</pre></p>
	<h4><b>Example</b></h4>
	<p>ตัวอย่างนี้จะเป็นการเพิ่มค่าของตัวแปร i อย่างน้อยหนึ่งครั้ง และจะเพิ่มขึ้นอย่างต่อเนื่องในขณะที่ค่ายังน้อยกว่า 10</p>
	<p><pre>&lt;?php
	<ul>$i = 0;<br>$num = 0;<br><br><z style="color:#0000FF;">do</z> {<br><ul>$i++;</ul>}<br><br><z style="color:#0000FF;">while</z>( $i < 10);<br>echo (<z style="color:#009900;">"Loop stopped at i = $i"</z>);</ul>?&gt;</pre></p>
	<p>ซึ่งจะทำให้ได้ผลลัพธ์ คือ</p>
	<p><pre>Loop stopped at i = 10</pre></p>
	
	<h3><b>The foreach loop statement</b></h3>
	<p><ul>Foreach statement มักใช้สำหรับ array ในแต่ละรอบของการทำซ้ำ ค่าขององค์ประกอบ array ปัจจุบันจะถูกประมวลผล และ ตัวชี้ array จะถูกขยับไปทีละหนึ่งเมื่อทำซ้ำในรอบถัดไป</ul><p>
	<h4><b>Syntax</b></h4>
	<p><pre>foreach (array as value) {
	<ul>code to be executed;</ul>}<br>while (condition);</pre></p>
	<h4><b>Example</b></h4>
	<p>ตัวอย่างนี้จะเป็นการแสดงค่าขององค์ประกอบแต่ละตัวของ array</p>
	<p><pre>&lt;?php
	<ul>$array = array( 1, 2, 3, 4, 5);<br><br><z style="color:#0000FF;">foreach</z>( $array <z style="color:#0000FF;">as</z> $value ) {<br><ul>echo (<z style="color:#009900;">"Value is $value &lt;br&gt;"</z>);</ul>}<br></ul>?&gt;</pre></p>
	<p>ซึ่งจะทำให้ได้ผลลัพธ์ คือ</p>
	<p><pre>Value is 1<br>Value is 2<br>Value is 3<br>Value is 4<br>Value is 5</pre></p>
	
	<h3><b>The break statement</b></h3>
	<p><ul>ใน PHP break ใช้สำหรับการยุติการประมวลผลของลูปก่อนกำหนด</ul><p>
	<p><ul>break statement จะถูกวางไว้ภายในบล็อคโค้ด คุณสามารถควบคุมได้ว่าเมื่อใดที่อยากให้การประมวลผลของลูปสิ้นสุดโดยการใช้ break และ statement ที่อยู่ถัดจากลูปจะถูกประมวลผลต่อทันที</ul><p>
	<p><ul><img src="images/php_break_statement.jpg"></ul></p>
	<h4><b>Example</b></h4>
	<p>ตัวอย่างนี้จะเป็นการทำซ้ำจนกว่าตัวแปรจะมีค่าเป็น 10 แต่การทดสอบเงื่อนไขของ if จะเป็นจริงเมื่อตัวแปรมีค่าเป็น 3 ดังนั้นลูปจะถูกยกเลิกโดย break</p>
	<p><pre>&lt;?php
	<ul>$i = 0;<br><br><z style="color:#0000FF;">while</z>( $i < 10) {<br><ul>$i++;<br><z style="color:#0000FF;">if</z>( $i == 3 )<z style="color:#0000FF;">break</z>;</ul>}<br>echo (<z style="color:#009900;">"Loop stopped at i = $i"</z> );</ul>?&gt;</pre></p>
	<p>ซึ่งจะทำให้ได้ผลลัพธ์ คือ</p>
	<p><pre>Loop stopped at i = 3</pre></p>
	
	<h3><b>The continue statement</b></h3>
	<p><ul>ใน PHP <b>break</b> ใช้สำหรับการหยุดการประมวลผลของลูปในรอบนั้น แต่จะไม่ยุติการประมวลผลของลูป</ul><p>
	<p><ul>คล้ายกับ <b>break</b> statement <b>continue</b> จะถูกวางไว้ภายในบล็อคโค้ด ตามหลังเงื่อนไขการทดสอบ หลังจากการทำงานของ  <b>continue</b> statement โค้ดส่วนที่เหลือจะถูกข้ามการทำงาน แล้วไปทำงานที่รอบใหม่ของลูปนั้นแทน</ul><p>
	<p><ul><img src="images/php_continue_statement.jpg"></ul></p>
	<h4><b>Example</b></h4>
	<p>ตัวอย่างนี้จะเป็นการแสดงค่าขององค์ประกอบแต่ละตัวของ array แต่เมื่อเงื่อนไขของ if เป็นจริงจะข้ามการทำงานของรอบนั้นแล้วแสดงค่าของตัวถัดไป</p>
	<p><pre>&lt;?php
	<ul>$array = array( 1, 2, 3, 4, 5);<br><br><z style="color:#0000FF;">foreach</z>( $array <z style="color:#0000FF;">as</z> $value) {<br><ul><z style="color:#0000FF;">if</z>( $value == 3 )<z style="color:#0000FF;">continue</z>;<br>echo (<z style="color:#009900;">"Value is $value &lt;br&gt;"</z> );</ul>}</ul>?&gt;</pre></p>
	<p>ซึ่งจะทำให้ได้ผลลัพธ์ คือ</p>
	<p><pre>Value is 1<br>Value is 2<br>Value is 4<br>Value is 5</pre></p>
	';
?>

<?php include('single.php'); ?>