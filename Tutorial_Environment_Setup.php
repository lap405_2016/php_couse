<?php
	$the_title = 'Environment Setup';
	$the_content = '<p><ul>ในการพัฒนาและมีการเรียกใช้หน้าเว็บ PHP มีองค์ประกอบที่สำคัญที่ต้องทำการติดตั้งในคอมพิวเตอร์ของคุณสามประการ ดังนี้</ul></p>
	                <b>Web Server:</b> PHP จะทำงานแทบทุก WEB server รวมไปถึง Internet Information Server(IIS) ของ Microsoft แต่ส่วนใหญ่มักจะใช้เป็น Apache Server  
	                <p><ul>ดาวน์โหลดฟรีที่นี่ <a rel="nofollow" href="http://httpd.apache.org/download.cgi" target="_blank">http://httpd.apache.org/download.cgi</a></ul></p>
	                <p><b>Database:</b> PHP จะทำงานร่วมกันแทยทุกซอฟต์แวร์ของDatabase รวมไปถึง Oracle และ Sybase แต่ส่วนใหญ่จะใช้ MySQL กันมากที่สุด 
	                <p><ul>ดาวน์โหลดฟรีที่นี่ <a rel="nofollow" target="_blank" href="http://www.mysql.com/downloads/">http://www.mysql.com/downloads/</a></ul></p>
	                <p><b>PHP Parser:</b> การโปรเซสคำสั่ง PHP ต้องมีการติดตั้งการแสดงผล HTMLที่ส่งไปยังเบราเซอร์ โดยจะแนะนำวิธีการติดตั้ง PHP Parser ในคอมพิวเตอร์ของคุณ 
	                <h3>การติดตั้ง PHP Parser</h3>
	                <p><ul>ก่อนการติดตั้งต้องเช็คว่ามีการตั้งค่าที่เหมาะสมในเครื่องของคุณแล้วในการพัฒนาโปรแกรมเว็บด้วย PHP</ul></p>
	                <pre>http://127.0.0.1/info.php</pre>
	                <p><ul>ถ้าหน้านี้แสดงข้อมูลการติดตั้งเกี่ยวกับ PHP ซึ่งจะหมายความว่ามี PHP และ Web Server ได้มีการติดตั้งอย่างถูกต้อง มิฉะนั้นจะต้องทำการติดตั้ง PHP ใหม่บนเครื่องของคุณ</ul></p>
	                <p>ในส่วนนี้จะบอกวิธีการติดตั้ง PHP และ config PHP อยู่ 4 แบบ</p>
	                <p><ul><a href="https://www.tutorialspoint.com/php/php_installation_linux.htm">การติดตั้ง PHP บน Linux หรือ Unix with Apache</a></ul></p>
	                <p><ul><a href="https://www.tutorialspoint.com/php/php_installation_mac.htm">การติดตั้ง PHP บน Mac OS X with Apache</a></ul></p>
	                <p><ul><a href="https://www.tutorialspoint.com/php/php_installation_windows_iis.htm">การติดตั้ง PHP บน Window NT/2000/XP with IIS</a></ul></p>
	                <p><ul><a href="https://www.tutorialspoint.com/php/php_installation_windows_apache.htm">การติดตั้ง PHP บน Windows NT/2000/XP with  Apache</a></ul></p>
	                <h3>การกำหนดค่า Apache</h3>
	                <p><ul>ถ้าในส่วนนี้มีการใช้ตัว Apache รันเป็นตัว Web Server จะมีการแนะนำการตั้งค่า Apache</ul></p>
	                <p><ul>คลิ๊กได้ที่นี่ --> <a href="https://www.tutorialspoint.com/php/php_apache_configuration.htm">PHP  Configuration in Apache Server</a></ul><p>
	                <h3>ไฟล์การกำหนดค่า php.ini</h3>
	                <p><ul>การกำหนดค่าไฟล์ใน php.ini เป็นวิธีสุดท้ายและจะส่งผลกระทบกับการทำงานของ PHP</ul></p>
	                <p><ul>คลิ๊กได้ที่นี่ --> <a href="https://www.tutorialspoint.com/php/php_ini_configuration.htm">PHP.INI File Configuration</a></ul></p>
	                <h3>การกำหนดค่า Window IIS</h3>
	                <p><ul>การกำหนดค่า IIS บน Window ของคุณสามารถดูจากคู่มืออ้างอิง IIS</ul><p>';



?>

<?php include('single.php'); ?>