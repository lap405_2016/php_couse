<?php
	$page = 20;
	$the_title = 'Predefined Variables';
	$the_content = '<p>PHP จะเตรียมตัวแปรไว้จำนวนมากซึ่งถูกกำหนดค่าไว้ล่วงหน้า (predefined variables) เพื่อใช้งานใน script ใด ๆ  นอกจากนั้น PHP ก็มี predefined array ซึ่งเก็บตัวแปร
	จากสภาพแวดล้อม web server และ input ที่ได้รับจาก user ด้วย ซึ่ง array นี้เรียกว่า superglobals</p>
	<p>ตัวแปรต่อไปนี้ทั้งหมดสามารถใช้ได้อัตโนมัติในทุก ๆ สถานการณ์</p>
	
	<h3>PHP Superglobals</h3>
	<table class="table table-bordered">
	<tr>
		<th width="10%" style="background:#eee; border:1px solid grey;">Sr.No</th>
		<th style="background:#eee; border:1px solid grey;">Variable &amp; Description</th>
	</tr>
	<tr>
		<td style="border:1px solid grey;">1</td>
		<td style="border:1px solid grey;"><b>$GLOBALS</b>
		<p>เก็บ reference ของทุก ๆ ตัวแปรที่ใช้ได้  และเป็นตัวแปรในระดับ global ของ script ซึ่ง keys ของ array คือชื่อของ global variable</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">2</td>
		<td style="border:1px solid grey;"><b>$_SERVER</b>
		<p>เป็น array ที่มีข้อมูล เช่น headers, paths และ script locations ซึ่งรายการของข้อมูลนี้จะถูกสร้างโดย web server และบาง web server ก็อาจจะไม่ได้มีข้อมูลครบตามตารางทั้งหมด  สามารถดูข้อมูลได้เพิ่มเติมในรายการตัวแปรทั้งหมดของ SERVER variables ในตารางถัดไป</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">3</td>
		<td style="border:1px solid grey;"><b>$_GET</b>
		<p>array ที่เก็บตัวแปรที่ถูกส่งไปให้ script ปัจจุบันผ่าน method HTTP GET</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">4</td>
		<td style="border:1px solid grey;"><b>$_POST</b>
		<p>array ที่เก็บตัวแปรที่ถูกส่งไปให้ script ปัจจุบันผ่าน method HTTP POST</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">5</td>
		<td style="border:1px solid grey;"><b>$_FILES</b>
		<p>array ที่เก็บไฟล์ที่ได้ถูกอัพโหลดไปให้ script ปัจจุบันผ่าน method HTTP POST</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">6</td>
		<td style="border:1px solid grey;"><b>$_REQUEST</b>
		<p>array ที่ประกอบด้วย contents $_GET, $_POST, และ $_COOKIE</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">7</td>
		<td style="border:1px solid grey;"><b>$_COOKIE</b>
		<p>array ที่เก็บตัวแปรที่ถูกส่งไปให้ script ปัจจุบันผ่านทาง HTTP cookies</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">8</td>
		<td style="border:1px solid grey;"><b>$_SESSION</b>
		<p>array ที่เก็บตัวแปร session ที่สามารถใช้งานได้ใน script ปัจจุบัน</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">9</td>
		<td style="border:1px solid grey;"><b>$_PHP_SELF</b>
		<p>เป็นตัวแปรประเภท string ที่เก็บชื่อของ PHP script ที่ถูกเรียกใช้</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">10</td>
		<td style="border:1px solid grey;"><b>$php_errormsg</b>
		<p>$php_errormsg เป็นตัวแปรที่เก็บข้อความของ error ล่าสุด ซึ่งจะถูกสร้างโดย PHP</p>
		</td>
	</tr>
	</table>

	<h3>Server Variables: $_SERVER</h3>
	<p>$_SERVER เป็น array ที่มีข้อมูล เช่น headers, paths และ script locations ซึ่งรายการของข้อมูลนี้จะถูกสร้างโดย web server และบาง web server ก็อาจจะไม่ได้มีข้อมูลครบตามตารางทั้งหมด</p>
	
	<table class="table table-bordered">
	<tr>
		<th width="10%" style="background:#eee; border:1px solid grey;">Sr.No</th>
		<th style="background:#eee; border:1px solid grey;">Variable &amp; Description</th>
	</tr>
	<tr>
		<td style="border:1px solid grey;">1</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;PHP_SELF&#39;]</b>
		<p>ชื่อของ script ปัจจุบันที่ถูกประมวลผลอยู่</p></td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">2</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;argv&#39;]</b>
		<p>array ของ argument ที่ถูกส่งไปให้ script โดยเมื่อ script ถูก run บน command line array นี้จะเป็น parameter ในรูปแบบของภาษา C
		และเมื่อถูกเรียกผ่าน Get method ตัวแปรนี้จะเก็บข้อมูลเป็น query string</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">3</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;argc&#39;]</b>
		<p>เก็บตัวเลขของ command line parameters ที่ถูกประมวลผลอยู่ผ่านทาง command line</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">4</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;GATEWAY_INTERFACE&#39;]</b>
		<p>เก็บสิ่งที่เป็นข้อมูลจำเพาะทางด้าน CGI ที่ server ใช้ ตัวอย่างเช่น &#39;CGI/1.1&#39;.</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">5</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;SERVER_ADDR&#39;]</b>
		<p>IP address ของ server ที่ทำงานประมวลผล script นั้นอยู่</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">6</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;SERVER_NAME&#39;]</b>
		<p>ชื่อของ host server ที่ประมวลผล script อยู่  โดยถ้าประมวลผลบน virtual host ตัวแปรนี้จะเป็นชื่อที่ถูกตั้งค่าไว้ให้ virtual host</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">7</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;SERVER_SOFTWARE&#39;]</b>
		<p>ข้อความที่ได้รับจาก header เมื่อตอบสนองกับ request ต่าง ๆ เพื่อการทำ Server identification</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">8</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;SERVER_PROTOCOL&#39;]</b>
		<p>ชื่อและการเปลี่ยนแปลงข้อมูล protocol ผ่านทางหน้าที่ถูก requset ตัวอย่างเช่น &#39;HTTP/1.0&#39;</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">9</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;REQUEST_METHOD&#39;]</b>
		<p>request method ที่ถูกเรียกใช้ผ่านหน้านี้ ตัวอย่างเช่น &#39;GET&#39;, &#39;HEAD&#39;, &#39;POST&#39;, &#39;PUT&#39;</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">10</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;REQUEST_TIME&#39;]</b>
		<p>timestamp ที่เริ่มการ request (สามารถใช้งานตัวแปรนี้ได้ตั้งแต่ PHP 5.1.0 เป็นต้นไป)</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">11</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;QUERY_STRING&#39;]</b>
		<p>การ query string ต่าง ๆ ที่เกิดขึ้นในหน้าที่ใช้งาน</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">12</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;DOCUMENT_ROOT&#39;]</b>
		<p>directory ที่เป็น document root ของ script ที่กำลังประมวลผลอยู่เบื้องหลัง  ซึ่งสามารถกำหนดได้โดย server&#39;s configuration file</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">13</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;HTTP_ACCEPT&#39;]</b>
		<p>Contents of the Accept: header จากการ request ปัจจุบันถ้าหากมีการ request เกิดขึ้น</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">14</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;HTTP_ACCEPT_CHARSET&#39;]</b>
		<p>Contents of the Accept-Charset: header จากการ request ปัจจุบันถ้าหากมีการ request เกิดขึ้น  ตัวอย่าง: &#39;iso-8859-1,*,utf-8&#39;</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">15</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;HTTP_ACCEPT_ENCODING&#39;]</b>
		<p>Contents of the Accept-Encoding: header จากการ request ปัจจุบันถ้าหากมีการ request เกิดขึ้น  ตัวอย่าง: &#39;gzip&#39;</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">16</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;HTTP_ACCEPT_LANGUAGE&#39;]</b>
		<p>Contents of the Accept-Language: header จากการ request ปัจจุบันถ้าหากมีการ request เกิดขึ้น  ตัวอย่าง: &#39;en&#39;</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">17</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;HTTP_CONNECTION&#39;]</b>
		<p>Contents of the Connection: header จากการ request ปัจจุบันถ้าหากมีการ request เกิดขึ้น  ตัวอย่าง: &#39;Keep-Alive&#39;</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">18</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;HTTP_HOST&#39;]</b>
		<p>Contents of the Host: header จากการ request ปัจจุบันถ้าหากมีการ request เกิดขึ้น</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">19</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;HTTP_REFERER&#39;]</b>
		<p>address ของหน้า (ถ้ามี) ที่ถูกกล่าวถึง user agent ของหน้าปัจจุบัน</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">20</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;HTTP_USER_AGENT&#39;]</b>
		<p>string ที่แสดงถึง user agent ที่ใช้เพื่อเข้าถึง  ตัวอย่างเช่น: Mozilla/4.5 [en] (X11; U; Linux 2.2.9 i586).</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">21</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;HTTPS&#39;]</b>
		<p>จะไม่ว่างถ้าหาก script ถูก query ผ่านทาง HTTPS protocol</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">22</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;REMOTE_ADDR&#39;]</b>
		<p>IP address จาก user ปัจจุบันที่กำลังชมหน้านั้นอยู่</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">23</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;REMOTE_HOST&#39;]</b>
		<p>ชื่อของ host ซึ่งได้จาก user ปัจจุบันที่กำหลังชมหน้านั้นอยู่ 	reverse dns lookup จะทำให้ได้รับผลลัพธ์เป็น REMOTE_ADDR ของ user</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">24</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;REMOTE_PORT&#39;]</b>
		<p>port ที่ถูกใช้บนอุปกรณณ์ของ user เพื่อติดต่อกับ web server</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">25</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;SCRIPT_FILENAME&#39;]</b>
		<p>absolute pathname ของ script ที่ถูกประมวลผลล่าสุด</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">26</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;SERVER_ADMIN&#39;]</b>
		<p>คือค่าที่ส่งไปให้ SERVER_ADMIN (สำหรับ Apache) โดยตรง ในไฟล์ configuration ของ web server</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">27</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;SERVER_PORT&#39;]</b>
		<p>port บน server machine ที่ถูกใช้โดย web server สำหรับการติดต่อ  ซึ่งสำหรับค่า default จะเป็น port &#39;80&#39;.</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">28</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;SERVER_SIGNATURE&#39;]</b>
		<p>String ที่เก็บข้อมูลเกี่ยวกับ server version, ชื่อของ virtual host ที่ถูกเพิ่มไปยัง server-generation ต่าง ๆ ถ้าได้ทำหาร enable ไว้</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">29</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;PATH_TRANSLATED&#39;]</b>
		<p>path ของไฟล์ของระบบสำหรับ script ที่ประมวลผล</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">30</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;SCRIPT_NAME&#39;]</b>
		<p>เก็บ path ของ script ล่าสุด  ซึ่งเป็นประโยชน์มากเมื่อต้องการเขียนโปรแกรมที่ต้องมีการเรียกใช้ path ของตัวเอง</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">31</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;REQUEST_URI&#39;]</b>
		<p>URI สำหรับการใช้คำสั่งเพื่อเข้าถึงหน้านี้  ตัวอย่างเช่น &#39;/index.html&#39;</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">32</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;PHP_AUTH_DIGEST&#39;]</b>
		<p>เมื่อทำงานภายใต้ Apache เป็น module ที่ใช้ Digest HTTP authentication ตัวแปรนี้ถูกตั้งค่าเป็น header &#39;Authorization&#39; ซึ่งถูกส่งโดยผู้ใช้งาน</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">33</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;PHP_AUTH_USER&#39;]</b>
		<p>เมื่อทำงานภายใต้ Apache หรือ IIS (ISAPI บน PHP 5) เป็น module ที่ใช้ Digest HTTP authentication ตัวแปรนี้ถูกตั้งค่าเป็น username ซึ่งได้รับจากผู้ใช้งาน</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">34</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;PHP_AUTH_PW&#39;]</b>
		<p>เมื่อทำงานภายใต้ Apache หรือ IIS (ISAPI บน PHP 5) เป็น module ที่ใช้ Digest HTTP authentication ตัวแปรนี้ถูกตั้งค่าเป็น password ซึ่งได้รับจากผู้ใช้งาน</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">35</td>
		<td style="border:1px solid grey;"><b>$_SERVER[&#39;AUTH_TYPE&#39;]</b>
		<p>เมื่อทำงานภายใต้ Apache เป็น module ที่ใช้ Digest HTTP authentication ตัวแปรนี้ถูกตั้งค่าเป็นชนิดของการ authentication</p></td>
	</tr>
	</table>

	';
?>

<?php include('single.php'); ?>