<?php
	$page = 33;
	$the_title = 'Phalcon Form';
	$the_content = "
	<div>
		<p>
			<ul>
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbspในบทเรียนนี้จะสอนการใช้ สร้าง Form ด้วย Volt template และ Form ตามแบบ PHP ธรรมดา โดยทั้งสองแบบนี้จะมีความแตกต่างทางด้านไวยกรณ์ ผู้ใช้สามารถเลือกได้ตามความถนัดของตนเอง และจะแสดงตัวอย่างการรับข้อมูลจากฟอร์มที่สร้างมา
			</ul>
		</p>
	</div>
	<div>
		<p>
		<ul>
			<h3>1. สร้าง Form ตามแบบ Volt template </h3><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbspตัวอย่าง คำสั่งสำหรับสร้าง Form ป้อนข้อมูล บน Volt template หรือ ไฟล์นามสกุล (.volt)
			<ul> <br>
				<b>Form : Volt template</b> <br><br>
				
				<img src='images/tutorial-phalcon-basic-9-1.png' class='img-responsive' > <br><br>
				
				<b>ตัวอย่างคำสั่ง</b> <br><br>

				<img src='images/Capture8.PNG' class='img-responsive' > <br><br>
			</ul>
			<h3>2. สร้าง Form ตามแบบ PHP ธรรมดา </h3><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbspตัวอย่าง คำสั่งสำหรับสร้าง Form ป้อนข้อมูล บน View ธรรมดา หรือ ไฟล์นามสกุล (.php)
			<ul> <br>
				<b>Form : PHP ธรรมดา</b> <br><br>
				
				<img src='images/tutorial-phalcon-basic-9-2.png' class='img-responsive' > <br><br>
				
				<b>ตัวอย่างคำสั่ง</b> <br><br>

				<img src='images/Capture9.PNG' class='img-responsive' > <br><br>
			</ul>
			<h3>3. คำสั่งการรับข้อมูลจากฟอร์ม (Form) ที่เราป้อนข้อมูล </h3><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbspหลังจากที่เรากดปุ่ม Submit บนฟอร์มรับข้อมูลมาแล้ว ข้อมูลที่เราป้อนเข้าไปจะถูกส่งไปยัง attion ที่เราระบุภายในคำสั่งฟอร์ม โดยเลือกรูปแบบการส่งข้อมูลแบบ POST ซึ่งเราสามารถเขียนคำสั่ง ตามตัวอย่างต่อไปนี้
			<ul> <br>
				
				<b>ตัวอย่างคำสั่ง</b> <br><br>

				<img src='images/Capture10.PNG' class='img-responsive' > <br><br>
			</ul>
			
		</ul>
		</p>
	</div>";
?>

<?php include('single.php'); ?>