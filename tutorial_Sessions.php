<?php
    $page = 16;
	$the_title = 'Sessions';
	$the_content = '<br><ul>คือตัวแปรในภาษา PHP ซึ่งมีคุณสมบัติทุกอย่างเหมือนกับตัวแปรปกติทั้งเก็บค่าข้อความหรือตัวเลข ตลอดทั้งตัวแปรที่เป็น Object ต่างๆได้</ul>
	<p>แต่จะมีความพิเศษกว่าตัวแปรปกติคือ ตัวแปรเซสชั่นและค่าตัวแปรจะยังคงอยู่ไม่ว่าเราจะเปลี่ยนหน้าจากหน้าหนึ่งไป สู่อีกหน้าหนึ่งซึ่งเป็นข้อดีของ</p>
	<p>ตัวแปรนี้</p>
	</br>
	<p><u>การเริ่มใช้งาน session</u></p>
	<p>- การเรียกใช้งาน session สามารถเริ่มการใช้งานโดยการเริ่มต้น session โดยเรียกดังนี้ session_start()</p>
	<p>- การใช้งานตัวแปรของ session จะคล้ายกับการใช้งานตัวแปร array โดยมีรูปแบบดังนี้ $_SESSION[]</p>
	<p>ในตัวอย่างการใช้งาน session ต่อไปนี้จะมีการใช้ isset() ซึ่งเป็นฟังก์ชันสำหรับใช้เพื่อตรวจสอบว่า ตัวแปรนั้นๆได้ถูกกำหนดขึ้น และมีค่าที่ไม่ใช่ null หรือไม่</p>
    <pre>&lt?php<br>   session_start();<br><br>   if( isset( $_SESSION[\'counter\'] ) ) {<br>      $_SESSION[\'counter\'] += 1;<br>   }else {<br>      $_SESSION[\'counter\'] = 1;<br>   }<br><br>   $msg = "You have visited this page ".  $_SESSION[\'counter\'];<br>   $msg .= " in this session.";<br>?&gt<br><br>&lt?php  echo ( $msg );  ?&gt<br></pre><br>
    <p>Result:<pre>You have visited this page 1 in this session.<br></pre></p>
	<p><u>การลบตัวแปร session</u></p>
    <p>- การลบค่าเซสชั่นใช้คำสั่ง unset(); ในการลบเฉพาะตัวแปรเดียว</p>
    <p>- การลบค่าเซสชั่นใช้คำสั่ง session_destroy(); ในการลบตัวแปร session ทั้งหมด</p>
    <pre>&lt?php<br>   unset($_SESSION[\'counter\']);<br>?&gt<br></pre>
    <pre>&lt?php<br>   session_destroy();<br>?&gt<br></pre><br>
    <p><u>ความแตกต่างระหว่าง session กับ cookie</u></p>
    <p>- cookie กับ session ต่างกันที่การเก็บข้อมูล</p>
    <p>- cookie จะเก็บข้อมูลในลักษณะ text file ที่ฝั่ง Client</p>
    <p>- session เก็บที่ฝั่ง server แต่การ identify ของ server นั้นก็จะมีการส่ง cookie พิเศษ(session cookie) มาเก็บยังฝั่ง client เพื่อใช้ในการ identify ว่า client นั้นๆมี session ID เป็นค่าใด</p>
    <p>จากตัวอย่างต่อไปนี้ ฟังก์ชัน htmlspecialchars() เป็นฟังก์ชันสำหรับเปลี่ยน predefined characters เป็น HTML entities ซึ่งจะใช้ในการแสดงค่า session ID(SID)</p>
    <pre>&lt?php<br>   session_start();<br><br>   if( isset( $_SESSION[\'counter\'] ) ) {<br>      $_SESSION[\'counter\'] += 1;<br>   }else {<br>      $_SESSION[\'counter\'] = 1;<br>   }<br><br>   $msg = "You have visited this page ".  $_SESSION[\'counter\'];<br>   $msg .= " in this session.";<br><br>   echo ( $msg );<br>?&gt<br><br>&ltp&gt<br>   To continue  click following link &ltbr/&gt<br><br>   &lta  href = "nextpage.php?&lt?php echo htmlspecialchars(SID); ?&gt"&gt<br>&lt/p&gt</pre>
    <p>Result:<pre>You have visited this page 1 in this session.<br>To continue click following link<br></pre></p>
    ';
?>

<?php include('single.php'); ?>