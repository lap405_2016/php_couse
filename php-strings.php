<?php
	$page = 9;
	$the_title = 'Strings';
	$the_content = '<p>Strings คือชุดของตัวอักษรที่เรียงต่อกัน  เช่น "PHP suppports string operations"</p>
		<p><b>NOTE</b> - นี่คือคำสั่งการใช้งาน  String ที่ถูกติดตั้งมากับ PHP <a rel="nofollow" target="_blank" href="http://in.php.net/manual/en/ref.strings.php">PHP String Functions</a></p>
		<p>ตัวอย่างการใช้งาน String</p>
		<pre>$string_1 = "This is a string in double quotes";<br>$string_2 = "This is a somewhat longer, singly quoted string";<br>$string_39 = "This string has thirty-nine characters";<br>$string_0 = ""; // a string with zero characters</pre>
		<p>การใช้เครื่องหมาย Single quoted (&#39;) หรือ Doubly quoted (&quot;) นั้นแตกต่างกัน  โดย Singly quoted จะเก็บสักลักษณ์รวมทั้งตัวอักษรที่อยู่ภายในเครื่องหมายไว้ทุกตัวอักษร
		แต่สำหรับเครื่องหมาย Double quoted เราสามารถจะใส่ตัวแปรไปภายในเครื่องหมายได้  เพื่อให้ print ค่าของตัวแปรออกมาร่วมกับคำต่าง ๆ</p>
		<pre>&lt?php <br>&emsp;&emsp;&emsp;$variable = "name"; <br>&emsp;&emsp;&emsp;$literally = &#39;My $variable will not print!\\n&#39;;
		   <br>&emsp;&emsp;&emsp;print($literally); <br>&emsp;&emsp;&emsp;print &quot;&ltbr /&gt&quot;; 
		   <br>&emsp;&emsp;&emsp;$literally = "My $variable will print!\\n"; 
		   <br>&emsp;&emsp;&emsp;print($literally); <br>?&gt</pre>
		<p>ซึ่งผลลัพธ์ที่ได้จะออกมาเป็น</p>
		<pre>My $variable will not print!\n<br>My name will print</pre>
		<p>Strings ที่ถูกคั่นโดยเครื่องหมาย Double quotes (เช่น "this") จะถูก preprocess ไปสองกรณีโดย PHP ได้แก่</p>
		<ui class="list"><li>มีการขึ้นต้นด้วยเครื่องหมาย backslash (\) ซึงจะถูกแทนที่ด้วยอักขระพิเศษ</li>
			<li>เป็นชื่อของตัวแปร โดยมีการขึ้นต้นด้วยเครื่องหมาย $ จะถูกแทนที่ด้วยค่าของตัวแปรนั้นในรูปแบบ string</li>
		</ui>
		<p>นี่คือตัวอย่างการแทนที่ของแบบแรก</p>
		<ui class="list"><li>\n&emsp;ถูกแทนที่ด้วยการขึ้นบรรทัดใหม่</li>
			<li>\t&emsp;ถูกแทนที่ด้วย tab</li>
			<li>\$&emsp;ถูกแทนที่ด้วยเครื่องหมาย dollar sign ($)</li>
			<li>\&quot&emsp;ถูกแทนที่ด้วยเครื่องหมาย double-quote (&quot)</li>
			<li>\\\&emsp;ถูกแทนที่ด้วยเครื่องหมาย backslash (\)</li>
		</ui>
		
		<h3>Operator เพื่อการนำ String มาต่อกัน</h3>
		<p>การนำตัวแปร string 2 ตัวมาต่อกันทำได้โดยการใช้จุด (.)</p>
		<pre>&lt?php <br>&emsp;&emsp;&emsp;$string1 = "Hello World";<br>&emsp;&emsp;&emsp;$string2 = "1234"; 
			<br>&emsp;&emsp;&emsp;echo $string1 . " " . $string2; <br>?&gt</pre>
		<p>ซึ่งผลลัพธ์ที่ได้จะออกมาเป็น</p>
		<pre>Hello World 1234</pre>
		<p>จะเห็นว่ามีการใช้เครื่องหมายจุดถึงสองครั้ง  นั่นก็เพราะเราได้ทำการเพิ่ม string ตัวที่ 3 เข้าไปด้วย คือช่องว่างระหว่างระหว่าง string1 และ string2 นั่นเอง</p>
		
		<h3>การใช้คำสั่ง strlen()</h3>
		<p>คำสั่ง strlen() ถูกใช้เมื่อต้องการหาความยาวของ string ใด ๆ</p>
		<p>เช่นเมื่อต้องการหาความยาวของ string "Hello world!"</p>
		<pre>&lt?php <br>&emsp;&emsp;&emsp;echo strlen("Hello world!"); <br>?&gt</pre>
		<p>ซึ่งผลลัพธ์ที่ได้จะออกมาเป็น</p>
		<pre>12</pre>
		<p>ความยาวของ string นี้มักจะถูกใช้ร่วมกับการใช้งาน loop หรือคำสั่งอื่น ๆ   เมื่อต้องการที่จะทำงานให้ได้จำนวนรอบ เท่ากับ ความยาวของ string</p>
		
		<h3>การใช้คำสั่ง strpos()</h3>
		<p>คำสั่ง strpos()  ใช้เพื่อหาตำแหน่งของ string หรือ character ภายใน string ใด ๆ </p>
		<p>ถ้าหากเจอคำที่เหมือนกับคำที่ต้องการหาแล้ว  จะคืนค่าออกมาเป็นตำแหน่งแรกที่เจอ  หรือถ้าไม่เจอจะคืนค่าเป็น FALSE</p>
		<p>เช่นเมื่อต้องการหาตำแหน่งงของคำว่า "world"</p>
		<pre>&lt?php <br>&emsp;&emsp;&emsp;echo strpos("Hello world!", "world"); <br>?&gt</pre>
		<p>ซึ่งผลลัพธ์ที่ได้จะออกมาเป็น</p>
		<pre>6</pre>
		<p>ผลลัพธ์ที่ได้คือ 6 เพราะคำว่า world อยู่ในตำแหน่งที่ 6 ของคำว่า "Hello world!"  และที่เป็น 6 ไม่ใช่ 7 ก็เพราะตำแหน่งแรกของ string นี้เริ่มต้นที่ 0 ไม่ใช่ 1</p>
		';
?>

<?php include('single.php'); ?>