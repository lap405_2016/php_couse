<?php
    $page = 21;
	$the_title = 'Regular expression';
	$the_content = '<p>Regular Expressions มีอะไรมากกว่าลำดับหรือรูปแบบตัวอักษรของมันเอง พวกเขาให้รากฐานสำหรับการทำงานรูปแบบจับคู่</p>
	                <p>การใช้ Regular Expression คุณสามารถที่จะค้นหา string โดยเฉพาะ ใน string อื่นๆ  คุณสามารถแทน string ด้วย string อื่นๆ และสามารถ split เป็นจำนวนมากๆ</p>
	                <p>PHP มีฟังก์ชั่นเจาะจง 2 เซต ของ Regular Expressions ซึ่งสามารถใช้มันได้ตามสะดวกของแต่ละคน</p> 
	                <h3><b>POSIX Regular Expressions</b></h3>
	                <p>โครงสร้างของ POSIX Regular Expressions ไม่ได้แตกต่างจาก คณิตศาสตร์ทั่วไป องค์ประกอบต่างๆจะแสดงออกถึงความซับซ้อนมากขึ้น</p>
	                <p>การ Regular Expression ที่ง่ายที่สุด คือการจับคู่ 1 string กับ 1 คำ เช่น g จะเป็น string ใน haggle,bag</p>
	                <p>เราจะแนะนำฟังก์ชั่น ดังนี้ </p>
	                <h4><b>Brackets</b></h4>
	                <p>Bracket ([ ]) มีความหมายพิเศษ เมื่อนำมาใช้ใน Regular Expession โดยจะใช้ในการหาช่วงของ ตัวอักษรต่างๆ</p>
	                <table class="table table-bordered">
<tr>
<th width="10%">Sr.No</th>
<th width="90%">Expression &amp; Description</th>
</tr>
<tr>
<td>1</td>
<td><p><b>[0-9]</b></p>
<p>มันจะจับคู่เลขตั้งแต่ 0-9</p></td>
</tr>
<tr>
<td>2</td>
<td><p><b>[a-z]</b></p>
<p>มันจะจับคู่ตั้งแต่ a-z ตัวพิมพ์เล็ก</p></td>
</tr>
<tr>
<td>3</td>
<td><p><b>[A-Z]</b></p>
<p>มันจะจับคู่ตั้งแต่ a-z ตัวพิมพ์ใหญ่</p></td>
</tr>
<tr>
<td>4</td>
<td><p><b>[a-Z]</b></p>
<p>มันจะจับคู่ตั้งแต่ a(พิมพ์เล็ก)- z(ตัวพิมพ์ใหญ่)</p></td>
</tr>
</table>
                     <p>ซึ่งยังสามารถใช้เป็น [0-3] หรือ [b-v] ก็ได้</p>
                     <h4><b>Quantifiers</b></h4>
                     <p>ความถี่ หรือ ตำแหน่งของลำดับBrackets และ อักษรตัวเดียว สามารถแทนด้วย อักขระพิเศษ ตัวอักษรพิเศษที่มีความหมายที่เจาะจง  เครื่องหมาย +, *,? {int. range} และ $ ตามลำดับ</p>
                     <table class="table table-bordered">
<tr>
<th width="10%">Sr.No</th>
<th width="90%">Expression &amp; Description</th>
</tr>
<tr>
<td>1</td>
<td><p><b>p+</b></p>
<p>มันจะจับคู่กับ string โดย ที่มี p เป็นอย่างน้อย</p></td>
</tr>
<tr>
<td>2</td>
<td><p><b>p*</b></p>
<p>มันจะจับคู่กับ string ที่มี 0 หรือมากกว่าของ P</p></td>
</tr>
<tr>
<td>3</td>
<td><p><b>p?</b></p>
<p>มันจะจับคู่กับ string ที่มี 0 หรือมากกว่าของ P เป็นเพียงทางเลือกในการใช้งานของ p*</p></td>
</tr>
<tr>
<td>4</td>
<td><p><b>p{<b>N</b>}</b></p>
<p>มันจะจับคู่ string ตามลำดับ N ของ P</p></td>
</tr>
<tr>
<td>5</td>
<td><p><b>p{2,3}</b></p>
<p>มันจะจับคู่ string ที่ลำดับ 2 หรือ 3 ของ P</p></td>
</tr>
<tr>
<td>6</td>
<td><p><b>p{2, }</b></p>
<p>มันจะจับคู่ string ที่ลำดับอย่างน้อย 2 หรือมากกว่าใน ของ P</p></td>
</tr>
<tr>
<td>7</td>
<td><p><b>p$</b></p>
<p>มันจะจับคู่ string ด้วย p ที่สุดท้ายของมัน</p></td>
</tr>
<tr>
<td>8</td>
<td><p><b><b>^</b>p</b></p>
<p>มันจะจับคู่ string ด้วย p ที่เริ่มของมัน</p></td>
</tr>
</table>
                         <h4><b>Example</b></h4>
                         <p>จากตัวอย่างจะ clear concept เกี่ยวกับการจับคู่อักษร</p>
                         <table class="table table-bordered">
<tr>
<th width="10%">Sr.No</th>
<th width="90%">Expression &amp; Description</th>
</tr>
<tr>
<td>1</td>
<td><p><b>[^a-zA-Z]</b></p>
<p>มันจะจับคู่ string ที่ไม่ได้อยู่ตั้งแต่ a-z และ A-Z</p></td>
</tr>
<tr>
<td>2</td>
<td><p><b>p.p</b></p>
<p>มันจะจับคู่ string ที่มี P ตามโดตัวอักษรอื่นๆ ในทางกลับกันจะตามด้วย pอีก</p></td>
</tr>
<tr>
<td>3</td>
<td><p><b>^.{2}$</b></p>
<p>มันจะจับคู่กับ string ที่มีการผสมอักษร 2ตัว</p></td>
</tr>
<tr>
<td>4</td>
<td><p><b>&lt;b&gt;(.*)&lt;/b&gt;</b></p>
<p>มันจะจับคู่ string ล้อมรอบ ภายใน &lt;b&gt; and &lt;/b&gt;.</p></td>
</tr>
<tr>
<td>5</td>
<td><p><b>p(hp)*</b></p>
<p>มันจะจับคู่ string ที่มี p และตามด้วย 0 หรือมากกว่าของลำดับใน PHP</p></td>
</tr>
</table>
                         <h4><b>Predefined Character Ranges</b></h4>
                         <p>สำหรับความสะดวกในการเขียนโปรแกรมของคุณหลายช่วงตัวอักษรที่กำหนดไว้ล่วงหน้าหรือที่เรียกว่า Character classes ที่ใช้อยู่ Character classes ช่วงของตัวอักษรทั้งหมด เช่น ตัวอักษร หรือ ชุดจำนวนเต็ม</p>
                         <table class="table table-bordered">
<tr>
<th width="10%">Sr.No</th>
<th width="90%">Expression &amp; Description</th>
</tr>
<tr>
<td>1</td>
<td><p><b>[[:alpha:]]</b></p>
<p>มันจะจับคู่ string ที่มีอยู่ ตั้งแต่ aA ไป ZZ</p></td>
</tr>
<tr>
<td>2</td>
<td><p><b>[[:digit:]]</b></p>
<p>มันจะจับคู่ กับ จำนวนที่มีอยู่ ตั้งแต่ 0 ไป 9</p></td>
</tr>
<tr>
<td>3</td>
<td><p><b>[[:alnum:]]</b></p>
<p>มันจะจับคู่ทั้ง aA ไป ZZ และ 0 ไป 9</p></td>
</tr>
<tr>
<td>4</td>
<td><p><b>[[:space:]]</b></p>
<p>มันจะจับคู่ string ที่มี พื้นที่</p></td>
</tr>
</table>
                         <h3><b>Regexp POSIX Functions ของ PHP</b></h3>
                         <p>PHP มี 7 ฟังก์ชั่น สำหรับการ ค้นหา string โดยใช้ POSIX-style regular expressions</p>
                         <table class="table table-bordered">
<tr>
<th width="10%">Sr.No</th>
<th>Function &amp; Description</th>
</tr>
<tr>
<td>1</td>
<td><b>ereg()</b>
<p>The ereg() function ค้นหา string ที่ระบุโดย string สำหรับ string ที่ระบุโดยรูปแบบ เป็น จริงเมื่อพบ และเป็นเท็จเมื่อเป็นเงื่อนไข อื่นๆ</p>
</td>
</tr>
<tr>
<td>2</td>
<td><b>ereg_replace()</b>
<p>The ereg_replace() function ค้นหา string ที่ระบุโดยรูปแบบ และ แทนที่ด้วยการเปลี่ยนรูป หากมีการพบ</p>
</td>
</tr>
<tr>
<td>3</td>
<td><b>eregi()</b>
<p>The eregi() function ค้นหาทั้ง string ที่ระบุรูปแบบสำหรับ string โดย string การค้นหาไม่ sensitive</p>
</td>
</tr>
<tr>
<td>4</td>
<td><b>eregi_replace()</b>
<p>The eregi_replace() function การทำงานเหมือน ereg_replace() ยกเว้นการค้นหาสำหรับรูปแบบใน string มันไม่ sensitive</p>
</td>
</tr>
<tr>
<td>5</td>
<td><b>split()</b>
<p>The split() function จะแบ่ง string ออกมา ขอบเขตแต่ละอย่างอยู่บนพื้นฐานของการเกิดรูปแบบ string</p>
</td>
</tr>
<tr>
<td>6</td>
<td><b>spliti()</b>
<p>The spliti() function การทำงานอยู่ในลักษณะเดียวกับ split() ยกเว้นมันไม่ sensitive</p>
</td>
</tr>
<tr>
<td>7</td>
<td><b>sql_regcase()</b>
<p>The sql_regcase() function สามารถผ่าน utility function แปลงแต่ละตัวอักษรใน input parameter ในวงเล็บ ที่มีทั้งสองตัวอักษร</p>
</td>
</tr>
</table>
                                 <h3><b>PERL Style Regular Expressions</b></h3>
                                 <p>Perl-style regular expressions คล้ายกับ POSIX  The POSIX syntax สามารถใช้แทนกันได้ด้วย the Perl-style regular expression functions ในความเป็นจริง คุณสามารถใช้ในปริมาณที่ใช้ได้ใน POSIX section ก่อนหน้า</p>
                                 <p>ให้อธิบายแนวคิดที่ใช้ใน PERL regular expressions หลังจากนั้นจะแนะนำด้วย regular expression related functions</p>
                                 <h4><b>Meta characters</b></h4>
                                 <p>meta characters เป็นตัวอักษรง่ายๆ โดย \ ทำหน้าที่เป็นความหมายพิเศษ เช่น สามารถค้นหาจำนวนเงินเยอะๆ โดยใช้ <b>\d</b>  meta character: <b>/([\d]+)000/</b> โดย <b>\d</b> จะค้นหาสำหรับ string ที่เป็นตัวเลข</p>
                                 <p>จาก list สามารถใช้ใน PERL Style Regular Expressions</p>
                                 <pre class="result notranslate">
<b>Character		Description</b>
.              a single character
\s             a whitespace character (space, tab, newline)
\S             non-whitespace character
\d             a digit (0-9)
\D             a non-digit
\w             a word character (a-z, A-Z, 0-9, _)
\W             a non-word character
[aeiou]        matches a single character in the given set
[^aeiou]       matches a single character outside the given set
(foo|bar|baz)  matches any of the alternatives specified
</pre> 
                                 <h4><b>Modifiers</b></h4>
                                 <p>การปรับเปลี่ยนหลากหลายนั้นสามารถทำให้งานของคุณด้วย regexps ง่ายมากขึ้น เหมือนในการค้นหาหลายๆบรรทัด ฯลฯ</p>
                                 <pre class="result notranslate">
<b>Modifier	Description</b>
i 	Makes the match case insensitive
m 	Specifies that if the string has newline or carriage
	return characters, the ^ and $ operators will now
	match against a newline boundary, instead of a
	string boundary
o 	Evaluates the expression only once
s 	Allows use of . to match a newline character
x 	Allows you to use white space in the expression for clarity
g 	Globally finds all matches
cg 	Allows a search to continue even after a global match fails
</pre> 
                                 <h3><b>Regexp PERL Compatible Functions ของ PHP</b></h3>
                                 <p>PHP มีฟังก์ชั่นต่อไปนี้ สำหรับการค้นหา string ใช้ Perl-compatible regular expressions</p>
                                 <table class="table table-bordered">
<tr>
<th width="10%">Sr.No</th>
<th>Function &amp; Description</th>
</tr>
<tr>
<td>1</td>
<td><b>preg_match()</b>
<p>The preg_match() function ค้นหา string สำหรับรูปแบบ ถ้ามีรูปแบบนั้นอยู่เป็นจริง นอกเหนือจากนี้เป็นเท็จ</p>
</td>
</tr>
<tr>
<td>2</td>
<td><b>preg_match_all()</b>
<p>The preg_match_all() function จับคู่เหตุการณ์ที่เกิดขึ้นทั้งหมดของรูปแบบใน string</p>
</td>
</tr>
<tr>
<td>3</td>
<td><b>preg_replace()</b>
<p>The preg_replace() function จะทำงานคล้ายๆ ereg_replace() ยกเว้น regular expressions สามารถใช้ในรูปแบบ และ แทนที่ input parameter</p>
</td>
</tr>
<tr>
<td>4</td>
<td><b>preg_split()</b>
<p>The preg_split() function ทำงานคล้ายกับ split() ยกเว้น regular expressions ถูกยอมรับด้วย input parameter สำหรับ รูปแบบ</p>
</td>
</tr>
<tr>
<td>5</td>
<td><b>preg_grep()</b>
<p>The preg_grep() function ค้นหาทั้งหมดของ input_array จะคืนค่า การจับคู่ทั้งหมดของรูปแบบของ regexp</p>
</td>
</tr>
<tr>
<td>6</td>
<td><b>preg_ quote()</b>
<p>Quote regular expression characters</p>
</td>
</tr>
</table>
';
?>

<?php include('single.php'); ?>