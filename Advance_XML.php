<?php
  $page = 27;
	$the_title = 'XML';
	$the_content = '<p> XML เป็น markup language ซึ่งเหมือนกับ HTML โดย เอกสารของ XML เป็นแบบ plain text และ อยู่ใน tag < และ > มีสองความแตกต่างระหว่าง XML และ HTML</p>
	                <p><ul>XML ไม่ได้กำหนดชุด tag ที่ต้องใช้</ul></p>
	                <p><ul>XML จะจุกจิกมากกับโครงสร้างของเอกสาร</ul></p>
	                <p>XML ให้อิสระมากกว่า HTML HTML มีบางชุดของ tag &lt;a&gt;&lt;/a&gt;gเป็น tag ที่ล้อม link &lt;p&gt; เป็น tag ที่ เริ่มย่อหน้าใหม่ ในXML สามารถใช้ tag ที่คุณต้องการ ใส่ &lt;rating&gt;&lt;/rating&gt; tags ของ rating หนัง &lt;height&gt;&lt;/height&gt; tags ความสูงของคน ดังนั้น XML ให้คุณเลือก tag ของคุณเอง</p>
	                <p>XML เข้มงวดมากเมื่อมันเป็นโครงสร้าง document HTML ช่วยให้คุณเล่นได้อย่างรวดเร็วใน tag เปิดปิด แต่มันไม่ใช่กรณีที่มี XML</p>
	                <h3><b>HTML list ที่ไม่ได้ใช้ XML</b></h3>
	                <pre class="prettyprint notranslate">
&lt;ul&gt;
   &lt;li&gt;Braised Sea Cucumber
   &lt;li&gt;Baked Giblets with Salt
   &lt;li&gt;Abalone with Marrow and Duck Feet
&lt;/ul&gt;
</pre> 
                    <p>ไม่มี XML ที่ใช้ เพราะ ไม่มีtag ปิด ของ li ทุกครั้งที่เปิด ต้องมีการปิดใน XML
                    <h3><b>HTML list ที่ใช้ XML</b></h3>
                    <pre class="prettyprint notranslate">
&lt;ul&gt;
   &lt;li&gt;Braised Sea Cucumber&lt;/li&gt;
   &lt;li&gt;Baked Giblets with Salt&lt;/li&gt;
   &lt;li&gt;Abalone with Marrow and Duck Feet&lt;/li&gt;
&lt;/ul&gt;
</pre> 
                    <h3><b>Parsing an XML Document</b></h3>
                    <p>SimpleXML module ทำให้แยก XML Document ดี ง่าย การสร้าง SimpleXML จาก XML document เก็บใน string ผ่าน string  simplexml_load_string( ) มันจะ return เป็น SimpleXML object</p>
                    <h4><b>example</b></h4>
                    <pre class="prettyprint notranslate">
&lt;html&gt;
   &lt;body&gt;
      
      &lt;?php
         $note=&lt;&lt;&lt;XML
         
         &lt;note&gt;
            &lt;to&gt;Gopal K Verma&lt;/to&gt;
            &lt;from&gt;Sairamkrishna&lt;/from&gt;
            &lt;heading&gt;Project submission&lt;/heading&gt;
            &lt;body&gt;Please see clearly &lt;/body&gt;
         &lt;/note&gt;
         
         XML;
         $xml=simplexml_load_string($note);
         print_r($xml);
      ?&gt;
		
   &lt;/body&gt;
&lt;/html&gt;
</pre> 
                    <h4><b>result</b></h4>
                    <pre>SimpleXMLElement Object ( [to] => Gopal K Verma [from] => Sairamkrishna [heading] => Project submission [body] => Please see clearly )</pre>
                    <p><b>NOTE</b> − คุณสามารถใช้ฟังก์ชั่น simplexml_load_file( filename) ถ้ามี XML ใน file.</p>
                    <p>ข้อมูลทั้งหมดของ XML parsing function เช็คได้ที่ <a href="https://www.tutorialspoint.com/php/php_function_reference.htm">PHP Function Reference</a></p>
                    <h3><b>Generating an XML Document</b></h3>
                    <p>SimpleXML ดีสำหรับการแยก XML documents ที่มีอยู่ แต่คุณไม่สามารถใช้มันสร้างอันใหม่จากการ scratch</p>
                    <p>ทางที่ง่ายที่สุดสำหรับการ gen XML document คือ สร้าง PHP array ซึ่งโครงสร้าง mirror ของ XML document และผ่าน array มีองค์ประกอบเหมาะสมกับรูปแบบ</p>
                    <h4><b>example</b></h4>
                    <pre class="prettyprint notranslate tryit">
&lt;?php
   $channel = array(\'title\' =&gt; "What is For Dinner",
      \'link\' =&gt; \'http://menu.example.com/\',
      \'description\' =&gt; \'Choose what to eat tonight.\');
   
   print "&lt;channel&gt;\n";
   
   foreach ($channel as $element =&gt; $content) {
      print " &lt;$element&gt;";
      print htmlentities($content);
      print "&lt;/$element&gt;\n";
   }
   
   print "&lt;/channel&gt;";
?&gt;
</pre> 
                    <h4><b>result</b></h4>
                    <pre class="result notranslate">
&lt;channel&gt;
   &lt;title&gt;What is For Dinner&lt;/title&gt;
   &lt;link&gt;http://menu.example.com/&lt;/link&gt;
   &lt;description&gt;Choose what to eat tonight.&lt;/description&gt;
&lt;/channel&gt;
</pre> 
';
?>

<?php include('single.php'); ?>