<?php
	$page = 22;
	$the_title = 'Error & Exception Handling';
	$the_content = '<p>Error Handling เป็นการทำงานที่จับข้อผิดพลาดที่เกิดจากโปรแกรมของคุณแล้วนำไปแก้ปัญหาด้วยวิธีที่เหมาะสม ถ้าคุณต้องการที่จะจัดการกับปัญหาอย่างถูกต้องมันอาจนำมาซึ่งผลพวงที่ไม่คาดคิดมากมาย</p>
					<p>การจัดการกับปัญหาเป็นเรื่องที่ง่ายมากสำหรับ PHP</p>
					<h3><b>Using die() function</b></h3>
					<p>ขณะที่พัฒนาโปรแกรมภาษา PHP คุณควรตรวจสอบเงื่อนไขของปัญหาที่เป็นไปได้ทั้งหมด ก่อนที่จะพัฒนาต่อรวมถึงการแก้ปัญหาอย่างเหมาะสมเมื่อจำเป็น</p>
					<p>ทดลองทำตามตัวอย่างนี้โดยทั้งกรณีที่มีและไม่มีไฟล์ <b>/tmp/test.xt</b></p>
					<p><pre>&lt;?php <br><br><ul><z style="color:#0000FF;">if</z>(!file_exists(<z style="color:#009900;">"/tmp/test.txt"</z>)) {<br><ul><z style="color:#0000FF;">die</z>(<z style="color:#009900;">"File not found"</z>);</ul>}<z style="color:#0000FF;">else</z> { <br><ul>$file = fopen(<z style="color:#009900;">"/tmp/test.txt"</z>,<z style="color:#009900;">"r"</z>);  <br><z style="color:#0000FF;">print</z> <z style="color:#009900;">"Opend file sucessfully"</z>;</ul>}<br><z style="color:#990000;">// Test of the code here.</z></ul>?&gt;</pre></p>
					<p>ด้วยวิธีนี้คุณสามารถที่จะพัฒนาโปรแกรมได้อย่าวมีประสิทธีภาพ ด้วยการใช้เทคนิคข้างต้นคุณสามารถที่จะหยุดการทำงานของโปรแกรมเมื่อใดก็ตามที่เกิดความผิดพลาดขึ้นและสามารถที่จะแสดงข้อความที่ก่อให้เกิดความเข้าใจได้มากขึ้นและสะดวกแก่ผู้ใข้</p>
					<h3><b>Defining Custom Error Handling Function</b></h3>
					<p>คุณสามารถที่จะพัฒนาฟังก์ชันที่ใช้จัดการกับปัญหาขึ้นเอง ใน PHP มีเฟรมเวิร์คให้คุณสามารถกำหนดฟังก์ขันในการจัดการกับปัญหาได้</p>
					<p>ฟังก์ชันนี้ต้องสามารถที่จะจัดการอย่างน้อย 2 พารามิเตอร์ (error level และ error message) แต่สามารถรองรับได้ถึง 5 พารามิเตอร์ (optionally: ไฟล์, เลขบรรทัด และเนื้อหาของความผิดพลาด)</p>
					<h4><b>Syntax</b></h4>
					<p><pre>error_function(error_level,error_message, error_file,error_line,error_context);</pre></p>
					
					<table class="table table-bordered">
					<tbody><tr><th width="10%">Sr.No</th><th>Parameter &amp; Description</th></tr>
					<tr><td>1</td><td><p><b>error_level</b></p><p>จำเป็น - ระบุระดับการรายงานผลความผิดพลาดสำหรับผู้ใช้ในการกำหนดปัญหา ซึ่งต้องเป็นตัวเลข</p></td></tr>
					<tr><td>2</td><td><p><b>error_message</b></p><p>จำเป็น - ระบุข้อความความผิดพลาดสำหรับผู้ใช้ในการกำหนดปัญหา</p></td></tr>
					<tr><td>3</td><td><p><b>error_file</b></p><p>ไม่จำเป็น - ระบุชื่อของไฟล์ที่ก่อให้เกิดปัญหาขึ้น</p></td></tr>
					<tr><td>4</td><td><p><b>error_line</b></p><p>ไม่จำเป็น - ระบุหมายเลขบรรทัดที่ซึ่งเกิดปัญหาขึ้น</p></td></tr>
					<tr><td>5</td><td><p><b>error_context</b></p><p>ไม่จำเป็น - ระบุ array ที่ซึ่งประกอบด้วยตัวแปรทุกตัวพร้อมด้วยค่าของตัวแปรเหล่านั้นที่ใช้งานอยู่ขณะเกิดปัญหาขึ้น</p></td></tr></tbody></table>
					<h4><b>Possible Error levels</b></h4>
					<p>ระดับการรายงานผลความผิดพลาดเหล่านี้เป็นประเภทต่างๆของความผิดพลาด การจัดการกับปัญหาที่ผู้ใช้กำหนดสามารถใช้ได้เช่นกัน ซึ่งค่าเหล่านี้สามารถใช้ร่วมกันได้ด้วย ตัวจัดการ (|)</p>
					
					<table class="table table-bordered">
					<tbody><tr><th width="10%">Sr.No</th><th>Constant  &amp; Description</th><th>Value</th></tr>
					<tr><td>1</td><td><p><b>.E_ERROR</b></p><p>เป็นความผิดพลาด run-time แบบร้ายแรง การประมวลของสคริปถูกหยุด</p></td><td>1</td></tr>
					<tr><td>2</td><td><p><b>E_WARNING</b></p><p>เป็นความผิดพลาด run-time แบบไม่ร้ายแรง การประมวลของสคริปไม่ถูกหยุด</p></td><td>2</td></tr>
					<tr><td>3</td><td><p><b>E_PARSE</b></p><p>เป็นปัญหาความผิดพลาดของ Compile-time parse การวิเคราะห์ความผิดพลาดจะถูกสร้างขึ้นโดย ตัววิเคราะห์</p></td><td>4</td></tr>
					<tr><td>4</td><td><p><b>E_NOTICE</b></p><p>เป็นการแจ้งเตือน Run-time เมื่อสคริปพบบางสิ่งที่อาจก่อให้เกิดความผิดพลาด แต่ก็สามารถเกิดขึ้นได้ในกรณีที่สคริปทำงานปกติ</p></td><td>8</td></tr>
					<tr><td>5</td><td><p><b>E_CORE_ERROR</b></p><p>เป็นความผิดพลาดแบบร้ายแรงที่เกิดขึ้นระหว่าง PHP เริ่มต้นการทำงาน</p></td><td>16</td></tr>
					<tr><td>6</td><td><p><b>E_CORE_WARNING</b></p><p>เป็นความผิดพลาด run-time แบบไม่ร้ายแรงที่เกิดขึ้นระหว่าง PHP เริ่มต้นการทำงาน</p></td><td>32</td></tr>
					<tr><td>7</td><td><p><b>E_USER_ERROR</b></p><p>เป็นความผิดพลาดแบบร้ายแรงที่เกิดขึ้นโดยผู้ใช้ ซึ่งความผิดพลาดนี้มีความคล้ายคลืงกับ E_ERROR ที่เซ็ตโดยผู้พัฒนา โดยการใช้ฟังก์ชัน trigger_error()</p></td><td>256</td></tr>
					<tr><td>8</td><td><p><b>E_USER_WARNING</b></p><p>เป็นการเตือนแบบไม่ร้ายแรงที่เกิดขึ้นโดยผู้ใช้ ซึ่งมีความคล้ายคลืงกับ E_WARNING ที่เซ็ตโดยผู้พัฒนา โดยการใช้ฟังก์ชัน trigger_error()</p></td><td>512</td></tr>
					<tr><td>9</td><td><p><b>E_USER_NOTICE</b></p><p>เป็นการเตือนที่เกิดขึ้นโดยผู้ใช้ ซึ่งมีความคล้ายคลืงกับ E_NOTICE ที่เซ็ตโดยผู้พัฒนา โดยการใช้ฟังก์ชัน trigger_error()</p></td><td>1024</td></tr>
					<tr><td>10</td><td><p><b>E_STRICT</b></p><p>เป็นการเตือน Run-time ซึ่งเป็นการเปิดใช้งานให้ PHP แนะนำการเปลี่ยนแปลงโค้ดของคุณซึ่งจะเป็นการมั่นใจได้ว่าโค้ดของคุณจะมีการเข้ากันได้ของการทำงานร่วมกันที่ดีที่สุด</p></td><td>2048</td></tr>
					<tr><td>11</td><td><p><b>E_RECOVERABLE_ERROR</b></p><p>เป็นความผิดพลาดร้ายแรงที่สามารถเข้าถึงได้ ซึ่งมีความคล้ายคลึงกับ E_ERROR แต่สามารถเข้าถึงได้โดยการกำหนดปัญหาของผู้ใช้</p></td><td>4096</td></tr>
					<tr><td>12</td><td><p><b>E_ALL</b></p><p>เป็นทุกความผิดพลาดและการเตือน ยกเว้นระดับ E_STRICT (E_STRICT จะอยู่ในส่วนของ E_ALL บน PHP 6.0)</p></td><td>8191</td></tr>
					</tbody></table>
					
					<p>ระดับของความผิดพลาดทั้งหมดดังกล่าวนี้สามารถเซตโดยการใช้ PHP ไลบรารี่ทังก์ชันที่มาฝังมาในตัวดังตัวอย่างด้านล่างนี้ ซึ่งระดับสามารถเป็นค่าใดก็ได้ที่กำหนดไว้ในตารางด้านบน</p>
					<p><pre>int error_reporting ( [int $level] )</pre></p>
					<p>ด้านล่างนี้เป็นตัวอย่างซึ่งคุณสามารถที่จะสร้างฟังก์ชันเพื่อจัดการกับความผิดพลาด</p>
					<p><pre>&lt;?php <br><br><ul><z style="color:#0000FF;">function</z> handleError($errno, $errstr,$error_file,$error_line) {<br><ul>echo <z style="color:#009900;">"&lt;b&gt;Error:&lt;/b&gt; [$errno] $errstr - $error_file:$error_line"</z>);<br>echo <z style="color:#009900;">"&lt;br&gt;";</z>;<br>echo <z style="color:#009900;">"Terminating PHP Script"</z>; <br><br><z style="color:#0000FF;">die</z>();</ul>}</ul>?&gt;</pre></p>
					<p>เมื่อใดที่คุณกำหนดการจัดการความผิดพลาดขึ้นเอง คุณจำเป็นต้องเซ็ตให้มีการใช้งาน PHP ไลบรารี่ทังก์ชันที่มาฝังมาในตัวที่เป็นฟังก์ชันชื่อว่า <b>set_error_handler</b> ถึงตอนนี้ให้ลองทดสอบตัวอย่างนี้โดยการเรียกใช้ฟัง์ชันที่ไม่มีอยู่</p>
					<p><pre>&lt;?php <br><ul>error_reporting( E_ERROR );<br><br><z style="color:#0000FF;">function</z> handleError($errno, $errstr,$error_file,$error_line) {<br><ul>echo <z style="color:#009900;">"&lt;b&gt;Error:&lt;/b&gt; [$errno] $errstr - $error_file:$error_line"</z>);<br>echo <z style="color:#009900;">"&lt;br&gt;";</z>;<br>echo <z style="color:#009900;">"Terminating PHP Script"</z>; <br><br><z style="color:#0000FF;">die</z>();</ul>} <br><br><z style="color:#990000;">//set error handler</z> <br>set_error_handler(<z style="color:#009900;">"handleError"</z>); <br><br><z style="color:#990000;">//trigger error</z> <br>myFunction();</ul>?&gt;</pre></p>
					
					<h3><b>Exceptions Handling</b></h3>
					<p>PHP 5 มีในส่วนของข้อยกเว้น(exception) ซึ่งคล้ายคลึงกับภาษาโปรแกรมอื่นๆ ข้อยกเว้นเป็นสิ่งที่มีความสำคัญ และทำให้การควบคุมการจัดการความผิดพลาดดีมากยิ่งขึ้น</p>
					<p>ซึ่งคำสั่งที่เกี่ยวข้องกับข้อยกเว้นมีดังนี้</p>
					<ul><p><li><b>Try</b> - ฟังก์ชันที่ใช้งานข้อยกเว้นควรอยู่ในบล็อค <b>try</b> ถ้าข้อยกเว้นไม่เกิด โปรแกรมจะทำงานตามปกติ แต่ถ้าเมื่อใดที่เกิดข้อยกเว้น ข้อยกเว้นจะถูกโยน (<b>thrown</b>)</li></p>
					<p><li><b>Throw</b> - นี่เป็นสิ่งที่เกิดหลังจากเกิดข้อยกเว้น  การเกิด throw แต่ละครั้งต้องมีอย่างน้อย 1 <b>catch</b></li></p>
					<p><li><b>Catch</b> - บล็อคของ <b>catch</b> รับข้อยกเว้นที่เกิด throw มาแล้วสร้าง object ที่ประกอบด้วยข้อมูลเกี่ยวกับข้อยกเว้นนั้น</li></p></ul>
					<p>เมื่อข้อยกเว้นเกิด throw ขึ้นมา โค้ดจะไม่ถูกประมวลผล และ PHP จะพยายามหาบล็อคแรกที่จับคู่กับ <b>catch</b> ถ้าไม่คู่ที่เข้ากันได้เลย ข้อผิดพลาดแบบรุนแรงที่เกิดขึ้นจะก่อให้เกิดปัญหา "Uncaught Exception" ซึ่ง </p>
					<ul><p><li>ข้อยกเว้นอาจจะเกิดการ throw และ catch ได้ โค้ดอาจจะอยู่ในบล็อคของ try</li></p>
					<p><li>เมื่อมีการใช้บล็อค try จะต้องมีการใช้บล็อค catch อย่างน้อย 1 บล็อค เสมอ ซึ่งสามารถใช้บล็อค catch หลายๆบล็อคเพื่อ catch ปัญหาประเภทต่างๆกัน</li></p>
					<p><li>ข้อยกเว้นนอกจากจะสามารถ throw เพื่อ catch กับปัญหาต่างๆแล้วยังสามารถ re-throw ในบล็อค catch ได้ด้วย</li></p></ul>
					<h4><b>Example</b></h4>
					<p><pre>&lt;?php <ul><br><z style="color:#0000FF;">try</z> {<br><ul>$error = <z style="color:#009900;">\'Always throw this error\'</z>);<br><z style="color:#0000FF;">throw new </z><z style="color:#990066;">Exception</z>($error);<br><br><z style="color:#990000;">// Code following an exception is not executed.</z><br>echo <z style="color:#009900;">\'Never executed\'</z>;</ul>}<z style="color:#0000FF;">catch</z> (<z style="color:#990066;">Exception</z> $e) {	<br><ul>echo <z style="color:#009900;">\'Caught exception: \'</z>,  $e->getMessage(), 	<z style="color:#009900;">"\n"</z>;</ul>}<br><br><z style="color:#990000;">// Continue execution</z> <br>echo <z style="color:#009900;">\'Hello World\'</z>);</ul>?&gt;</pre></p>
					<p>จากโค้ดตัวอย่างข้างต้น ฟังก์ชัน $e->getMessage ใช้สำหรับเก็บข้อความของความผิดพลาด<br>ซึ่งคลาส ของข้อยกเว้น(exception)มีฟังก์ชันมากมายดังนี้</p>
					<ul><p><li><b>getMessage()</b> - ข้อความของความผิดพลาด</li></p>
					<p><li><b>getCode()</b> - โค้ดของความผิดพลาด</li></p>
					<p><li><b>getFile()</b> - ชื่อไฟล์ของแหล่งที่มา</li></p>
					<p><li><b>getLine()</b> - บรรทัดของแหล่งที่มา</li></p>
					<p><li><b>getTrace()</b> - n array ของ backtrace()</li></p>
					<p><li><b>getTraceAsString()</b> - การจัดรูปแบบสตริงของ trace</li></p></ul>
					<h4><b>Creating Custom Exception Handler</b></h4>
					<p>คุณสามารถกำหนดตัวจัดการความผิดพลาดตามที่คุณต้องการได้ ใช้ฟังก์ชันด้านล่างนี้เพื่อเป็นการกำหนดฟังก์ชันตัวจัดการความผิดพลาด</p>
					<p><pre>string set_exception_handler ( callback $exception_handler )</pre></p>
					<p><b>exception_handler</b> เป็นชื่อของฟังก์ชันที่จะถูกเรียกเมื่อข้อผิดพลาดแบบ uncaught เกิดขึ้น ฟังก์ชันนี้ต้องถูกกำหนดก่อนที่จะมีการเรียกใช้ฟังก์ชัน set_exception_handler()</p>
					<h4><b>Example</b></h4>
					<p><pre>&lt;?php <ul><br><z style="color:#0000FF;">function</z> exception_handler($exception) {<br><ul>echo <z style="color:#009900;">"Uncaught exception: "</z>, $exception->getMessage(), <z style="color:#009900;">"\n"</z>;</ul>}<br><br>set_exception_handler(<z style="color:#009900;">\'exception_handler\'</z>);<br><z style="color:#0000FF;">throw new </z><z style="color:#990066;">Exception</z>(<z style="color:#009900;">\'Uncaught Exception\'</z>);<br><br>echo <z style="color:#009900;">"Not Executed\n"</z>;</ul>?&gt;</pre></p>
					';
?>

<?php include('single.php'); ?>