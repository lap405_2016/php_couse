<?php
	$page = 13;
	$the_title = 'Files & I/O';
	$the_content = '<p>บทนี้จะอธิบายคำสั่งในการใช้งาน files และ I/O ได้แก่</p>
	<ul class="list"><li><p>Opening a file</p></li>
	<li><p>Reading a file</p></li>
	<li><p>Writing a file</p></li>
	<li><p>Closing a file</p></li></ul>
	
	<h3>Opening and Closing files</h3>
	<p>คำสั่ง <b>fopen()</b> ของ PHP ใช้เพื่อเปิดไฟล์ซึ่งต้องการ argument 2 ตัว ได้แก่ตัวแรกคือชื่อของไฟล์  และตัวถัดมาคือ Mode ที่ใช้ในการประมวลผล</p>
	<p>Files Mode ต่าง ๆ สามารถเลือกใช้ได้ 1 แบบจากทั้งหมด 6 แบบ</p>
	<table class="table table-bordered">
	<tr>
	<th width="10%"  style="background:#eee; border:1px solid grey;">Mode</th>
	<th  style="background:#eee; border:1px solid grey;">Purpose</th>
	</tr>
	<tr>
		<td style="border:1px solid grey;">r</td>
		<td style="border:1px solid grey;">เปิดไฟล์เพื่อ่านเท่านั้น
		<p>มี file pointer ที่ชี้ไปที่จุดเริ่มต้นของไฟล์</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">r+</td>
		<td style="border:1px solid grey;">เปิดไฟล์เพื่ออ่านหรือแก้ไข
		<p>มี file pointer ที่ชี้ไปที่จุดเริ่มต้นของไฟล์</p></td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">w</td>
		<td style="border:1px solid grey;">เปิดไฟล์เพื่อแก้ไขเท่านั้น
		<p>มี file pointer ที่ชี้ไปที่จุดเริ่มต้นของไฟล์ และข้อมูลในไฟล์จะถูกตัดทิ้ง ทำให้ได้ไฟล์ที่ว่างเปล่า</p>
		<p>ถ้าไม่พบไฟล์นั้นจะสร้างไฟล์ใหม่ให้โดยอัตโนมัติในชื่อเดียวกัน</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">w+</td>
		<td style="border:1px solid grey;">เปิดไฟล์เพื่ออ่านหรือแก้ไข
		<p>มี file pointer ที่ชี้ไปที่จุดเริ่มต้นของไฟล์</p>
		<p>มี file pointer ที่ชี้ไปที่จุดเริ่มต้นของไฟล์ และข้อมูลในไฟล์จะถูกตัดทิ้ง ทำให้ได้ไฟล์ที่ว่างเปล่า</p>
		<p>ถ้าไม่พบไฟล์นั้นจะสร้างไฟล์ใหม่ให้โดยอัตโนมัติในชื่อเดียวกัน</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">a</td>
		<td style="border:1px solid grey;">เปิดไฟล์เพื่อแก้ไขโดยเท่านั้น
		<p>มี file pointer ที่ชี้ไปที่จุดสิ้นสุดของไฟล์</p>
		<p>ถ้าไม่พบไฟล์นั้นจะสร้างไฟล์ใหม่ให้โดยอัตโนมัติในชื่อเดียวกัน</p>
		</td>
	</tr>
	<tr>
		<td style="border:1px solid grey;">a+</td>
		<td style="border:1px solid grey;">เปิดไฟล์เพื่ออ่านหรือแก้ไข
		<p>มี file pointer ที่ชี้ไปที่จุดสิ้นสุดของไฟล์</p>
		<p>ถ้าไม่พบไฟล์นั้นจะสร้างไฟล์ใหม่ให้โดยอัตโนมัติในชื่อเดียวกัน</p>
		</td>
	</tr>
	</table>
	<p>ถ้าหากการเปิดไฟล์ล้มเหลว  <b>fopen</b> จะคืนค่าเป็น <b>false</b> ไม่อย่างนั้นจะคืนค่าเป็น <b>file pointer</b> ที่ใช้เพื่ออ่าน  หรือแก้ไขไฟล์นั้น ๆ แทน</p>
	<p>หลังจากเปลี่ยนแปลงข้อมูลภายในไฟล์เรียบร้อยแล้วสามารถใช้คำสั่ง <b>fclose()</b> เพื่อปิด  โดยคำสั่ง <b>fclose()</b> นั้นต้องการ file pointer เป็น argument 
	และจะ return <b>true</b> เมื่อปิดไฟล์นั้นได้สำเร็จ  ในทางกลับกันถ้าปิดไฟล์ล้มเหลวก็จะ return <b>false</b> แทน</p>
	
	<h3>Reading a file</h3>
	<p>เมื่อได้ทำการเปิดไฟล์จากคำสั่ง <b>fopen</b> ไปครั้งหนึ่งแล้ว  เราสามารถจะอ่านข้อความในไฟล์นั้นได้ด้วยคำสั่ง <b>fread()</b> ซึ่งคำสั่งนี้ต้องการ 2 arguments เช่นกัน ได้แก่ file pointer และ file expressed ที่ต้องการอ่านขนาดเป็น bytes</p>
	<p>ขนาดของไฟล์นั้นสามารถทราบได้โดยการใช้คำสั่ง <b>filesize()</b> ซึ่งต้องการชื่อของไฟล์เป็น argument ซึ่งจะ return file expressed ที่ต้องการอ่านขนาดเป็น bytes</p>
	<p>ดังนั้นการจะอ่านไฟล์ในภาษา PHP จะมีลำดับขั้นตอนดังนี้</p>
	<ul class="list"><li><p>เปิดไฟล์โดยการใช้คำสั่ง <b>fopen()</b></p></li>
		<li><p>ดึงขนาดของไฟล์จากคำสั่ง <b>filesize()</b></p></li>
		<li><p>อ่าน content ในไฟล์โดยใช้คำสั่ง <b>fread()</b></p></li>
		<li><p>ปิดไฟล์โดยใช้คำสั่ง <b>fclose()</b></p></li>
	</ul>
	<p>ซึ่งสามารถดูจากตัวอย่างด้านล่าง  ซึ่งต้องการจะดึงเอา content จากไฟล์มาแสดงบน web page</p>
<pre class="prettyprint notranslate">
&lt;html&gt;

   &lt;head&gt;
      &lt;title&gt;Reading a file using PHP&lt;/title&gt;
   &lt;/head&gt;
   
   &lt;body&gt;
      
      &lt;?php
         $filename = "tmp.txt";
         $file = fopen( $filename, "r" );
         
         if( $file == false ) {
            echo ( "Error in opening file" );
            exit();
         }
         
         $filesize = filesize( $filename );
         $filetext = fread( $file, $filesize );
         fclose( $file );
         
         echo ( "File size : $filesize bytes" );
         echo ( "&lt;pre&gt;$filetext&lt;/pre&gt;" );
      ?&gt;
      
   &lt;/body&gt;
&lt;/html&gt;
</pre> 
	
	<p>ซึ่งทำให้ได้ผลลัพธ์ดังนี้</p>
	<iframe onload="resizeFrame(this)" class="result" src="sample_read_file.php" height="150px" width="100%" style="border: 1px solid #d6d6d6;"></iframe>

	<h3>Writing a file</h3>
	<p>ไฟล์ใหม่ถูกเขียนได้ด้วยการเขียนต่อจากข้อความที่มีอยู่แล้วในไฟล์  โดยการใช้คำสั่ง <b>fwrite()</b> ซึ่งต้องการ 2 arguments ได้แก่ <b>file pointer</b> และข้อความที่ต้องการเขียน  
	และยังสามารถเพิ่ม argument ได้อีกตัวหนึ่งเป็น integer คือความยาวของข้อความที่ต้องการให้เขียน  โดยถ้าหากมีการระบุความยาวของข้อความ การเขียนนั้นก็จะหยุดลงเมื่อถึงความยาวของข้อความที่เรากำหนด</p>
	<p>ดังตัวอย่างนี้ ที่จะสร้างไฟล์ text ขึ้นมาใหม่  แล้วเขียนข้อความสั่น ๆ ลงไป</p>
	<pre class="prettyprint notranslate">
&lt;?php
   $filename = "/home/user/guest/newfile.txt";
   $file = fopen( $filename, "w" );
   
   if( $file == false ) {
      echo ( "Error in opening new file" );
      exit();
   }
   fwrite( $file, "This is  a simple test\n" );
   fclose( $file );
?&gt;
&lt;html&gt;
   
   &lt;head&gt;
      &lt;title&gt;Writing a file using PHP&lt;/title&gt;
   &lt;/head&gt;
   
   &lt;body&gt;
      
      &lt;?php
         $filename = "newfile.txt";
         $file = fopen( $filename, "r" );
         
         if( $file == false ) {
            echo ( "Error in opening file" );
            exit();
         }
         
         $filesize = filesize( $filename );
         $filetext = fread( $file, $filesize );
         
         fclose( $file );
         
         echo ( "File size : $filesize bytes" );
         echo ( "$filetext" );
         echo("file name: $filename");
      ?&gt;
      
   &lt;/body&gt;
&lt;/html&gt;
</pre> 

	<p>ซึ่งทำให้ได้ผลลัพธ์ดังนี้</p>
	<iframe onload="resizeFrame(this)" class="result" src="sample_write_file.php" height="100px" width="100%" style="border: 1px solid #d6d6d6;"></iframe>
	<p>เราได้รวบรวมเอาคำสั่งต่าง ๆ ที่เกี่ยวข้องกับ file input and output เอาไว้ในหัวข้อ <a rel="nofollow" target="_blank" href="https://www.tutorialspoint.com/php/php_file_system_functions.htm">PHP File System Function</a></p>
	';
?>

<?php include('single.php'); ?>