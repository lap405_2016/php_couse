<?php
   $page = 18;
	$the_title = 'File Uploading';
	$the_content = '<p>PHP script สามารถใช้งานร่วมกับ HTML form ได้หลายอย่าง  รวมทั้งอนุญาติให้ผู้ใช้งาน upload file ไปสู่ server   files เริ่มต้นจะถูก upload ไปสู่
	temporary directory และ ส่งต่อไปย้ายปลายทางที่กำหนดไว้ใน PHP script</p>
	<p>ข้อมูลใน <b>phpinfo.php</b> จะอธิบาย temporary directory ซึ่งใช้ในการ upload ผ่านทาง <b>upload_tmp_dir</b> และขนาดสูงสุดที่สามารถใช้ในการ upload ได้ในส่วน
	<b>upload_max_filesize</b> โดย parameter เหล่านี้สามารถตั้งค่าได้ผ่านไฟล์ <b>php.ini</b></p>
	<p>กระบวนการในการ upload file จึงมีขั้นตอนดังนี้</p>
	<ul class="list">
		<li><p>ผู้ใช้งานเปิดหน้า HTML ที่มีข้อความ ,ปุ่ม browse, และปุ่ม submit</p></li>
		<li><p>ผู้ใช้งานคลิ้กปุ่ม browse และเลือกไฟล์ที่ต้องการ upload จาก local PC</p></li>
		<li><p>path ของไฟล์ที่ถูกเลือกปรากฏขึ้น และผู้ใช้งานคลิ้กปุ่ม submit</p></li>
		<li><p>ไฟล์ที่ถูกเลือกถูกส่งไปยัง temporary directory บน server</p></li>
		<li><p>PHP script ทำการตรวจสอบว่าไฟล์ได้ upload สู่ temporary directory และ copy ไฟล์นั้นไปสู่ directory ปลายทางที่ต้องการ</p></li>
		<li><p>php script ยืนยันกลับไปถึง user ว่าได้ทำการ upload สำเร็จ</p></li>
	</ul>
	<p>ซึ่งจำเป็นทั้ง temporary location และ final location ที่จะต้องได้รับ permission ให้สามารถ write ได้  ถ้าหากถูกกำหนดให้สามารถ read ได้เพียงอย่างเดียว  การ upload จะล้มเหลว</p>
	<p>ไฟล์ที่ถูกอัพโหลดสามารถเป็น text file, รูปภาพ  หรือ เอกสารใด ๆ ก็ได้</p>
	
	<h3>สร้าง form สำหรับการ upload</h3>
	<p>ตามตัวอย่างโค้ด HTM ด้านล่างคือการสร้าง form สำหรับการ upload  โดย form นี้มี method attribute ตั้งไว้เพื่อ <b>post</b> และ enctype attribute กำหนดไว้ที่ <b>multipart/form-data</b></p>
<pre class="prettyprint notranslate tryit">
&lt;?php
   if(isset($_FILES[&#39;image&#39;])){
      $errors= array();
      $file_name = $_FILES[&#39;image&#39;][&#39;name&#39;];
      $file_size =$_FILES[&#39;image&#39;][&#39;size&#39;];
      $file_tmp =$_FILES[&#39;image&#39;][&#39;tmp_name&#39;];
      $file_type=$_FILES[&#39;image&#39;][&#39;type&#39;];
      $file_ext=strtolower(end(explode(&#39;.&#39;,$_FILES[&#39;image&#39;][&#39;name&#39;])));
      
      $expensions= array("jpeg","jpg","png");
      
      if(in_array($file_ext,$expensions)=== false){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
      
      if($file_size > 2097152){
         $errors[]=&#39;File size must be excately 2 MB&#39;;
      }
      
      if(empty($errors)==true){
         move_uploaded_file($file_tmp,"images/".$file_name);
         echo "Success";
      }else{
         print_r($errors);
      }
   }
?&gt;
&lt;html&gt;
   &lt;body&gt;
      
      &lt;form action="" method="POST" enctype="multipart/form-data"&gt;
         &lt;input type="file" name="image" /&gt;
         &lt;input type="submit"/&gt;
      &lt;/form&gt;
      
   &lt;/body&gt;
&lt;/html&gt;
</pre> 
	<p>ซึ่งให้ผลลัพธ์ดังนี้</p>
	<iframe onload="resizeFrame(this)" class="result" src="sample_upload_form.php" height="100px" width="100%" style="border: 1px solid #d6d6d6;"></iframe>

	<h3>สร้าง script สำหรับการ upload</h3>
	<p>มีตัวแปรแบบ global ของ PHP ชื่อว่า <b>$_FILES</b> ซึ่งเป็นตัวแปรที่เป็นตัวแปร array 2 มิติ และเก็บข้อมูลในการ upload เอาไว้  ซึ่งถ้าหากชื่อ attribute ของ input มีค่าเป็น <b>file</b> PHP จะสร้างตัวแปรขึ้นมา 5 ตัวแปรได้แก่</p>
	<ul class="list">
		<li><p><b>$_FILES[&#39;file&#39;][&#39;tmp_name&#39;]</b> &minus; ไฟล์ที่ถูก upload ใน temporary directory บน web server</p></li>
		<li><p><b>$_FILES[&#39;file&#39;][&#39;name&#39;]</b> &minus; ชื่อจริงของไฟล์ที่ถูก upload</p></li>
		<li><p><b>$_FILES[&#39;file&#39;][&#39;size&#39;]</b> &minus; ขนาดเป็น bytes ของไฟล์ที่ upload</p></li>
		<li><p><b>$_FILES[&#39;file&#39;][&#39;type&#39;]</b> &minus; MIME type ของไฟล์ที่ upload</p></li>
		<li><p><b>$_FILES[&#39;file&#39;][&#39;error&#39;]</b> &minus; error code ที่เกี่ยวข้องในการ upload ไฟล์</p></li>
	</ul>
	
	<h4>ตัวอย่าง</h4>
	<p>ตัวอย่างข้างล่างจะอนุญาติให้ upload รูปภาพ และจะให้ผลลัพธ์เป็นข้อมูลของไฟล์ที่ถูกอัพโหลด</p>
<pre class="prettyprint notranslate tryit">
&lt;?php
   if(isset($_FILES[&#39;image&#39;])){
      $errors= array();
      $file_name = $_FILES[&#39;image&#39;][&#39;name&#39;];
      $file_size = $_FILES[&#39;image&#39;][&#39;size&#39;];
      $file_tmp = $_FILES[&#39;image&#39;][&#39;tmp_name&#39;];
      $file_type = $_FILES[&#39;image&#39;][&#39;type&#39;];
      $file_ext=strtolower(end(explode(&#39;.&#39;,$_FILES[&#39;image&#39;][&#39;name&#39;])));
      
      $expensions= array("jpeg","jpg","png");
      
      if(in_array($file_ext,$expensions)=== false){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
      
      if($file_size &gt; 2097152) {
         $errors[]=&#39;File size must be excately 2 MB&#39;;
      }
      
      if(empty($errors)==true) {
         move_uploaded_file($file_tmp,"images/".$file_name);
         echo "Success";
      }else{
         print_r($errors);
      }
   }
?&gt;
&lt;html&gt;
   &lt;body&gt;
      
      &lt;form action = "" method = "POST" enctype = "multipart/form-data"&gt;
         &lt;input type = "file" name = "image" /&gt;
         &lt;input type = "submit"/&gt;
			
         &lt;ul&gt;
            &lt;li&gt;Sent file: &lt;?php echo $_FILES[&#39;image&#39;][&#39;name&#39;];  ?&gt;
            &lt;li&gt;File size: &lt;?php echo $_FILES[&#39;image&#39;][&#39;size&#39;];  ?&gt;
            &lt;li&gt;File type: &lt;?php echo $_FILES[&#39;image&#39;][&#39;type&#39;] ?&gt;
         &lt;/ul&gt;
			
      &lt;/form&gt;
      
   &lt;/body&gt;
&lt;/html&gt;
</pre> 
	<p>ซึ่งจะให้ผลลัพธ์ดังนี้</p>
	<iframe onload="resizeFrame(this)" class="result" src="sample_upload_result.php" height="100px" width="100%" style="border: 1px solid #d6d6d6;"></iframe>
	';
?>

<?php include('single.php'); ?>