<?php
	$page = 2;
	$the_title = 'Syntax Overview';
	$the_content = '<h3><b>Escaping to PHP</b></h3>
					<ul><p>การเขียนโค้ด php นั้น มีวิธีการที่จะทำให้โค้ดในส่วนของ php แยกส่วนจากองค์ประกอบอื่นๆในหน้านั้น ซึ่งวิธีการนี้รู้จักกันในชื่อของ "escaping to PHP" ซึ่งมีอยู่ 4 วิธีดังนี้</p></ul>
					<h4><b>Canonical PHP tags</b></h4>
					<ul>เป็นรูปแบบที่นิยมใช้กันอย่างกว้างขวาง ซึ่งมีรูปแบบคือ</ul>
					<p><pre>&lt;?php <br>     ใส่คำสั่งของ PHP ลงในส่วนนี้ <br>?&gt;</pre></p>
					การใช้ในรูปแบบนี้ สามารถมั่นใจได้ว่าแท็ก php จะถูกตีความได้อย่างถูกต้อง<br><br>
					
					<h4><b>Short-open (SGML-style) tags</b></h4>
					<ul>แท็ก short หรือ short-open มีรูปแบบคือ</ul>
					<p><pre>&lt;? <br>     ใส่คำสั่งของ PHP ลงในส่วนนี้ <br>?&gt;</pre></p>
					การใช้งานแท็กย่อนี้จำเป็นต้องทำหนึ่งในสองสิ่งต่อไปนี้ก่อน เพื่อให้ PHP จดจำรูปแบบแท็ก
					<ul><li><p>ใช้การตั้งค่า <b>--enable-short-tags</b> ในตอน build PHP</p></li>
					<li><p>เซ็ตการตั้งค่า <b>short_open_tag</b> ใน ไฟล์ php.ini การตั้งค่านี้ต้องถูกปิดเพื่อที่จะสามารถใช้ XML ด้วย PHP เนื่องจาก syntax เดียวกันนี้ถูกใช้สำหรับแท็ก XML</p></li></ul>
					
					<h4><b>ASP-style tags</b></h4>
					<ul>เป็นการจำลองแท็กที่ถูกใช้โดย Active Server Pages เพื่อจำแนกบล็อคของโค้ด ซึ่งมีรูปแบบคือ</ul>
					<p><pre>&lt;%...%&gt;</pre></p>
					เพื่อที่จะสามารถใช้งานรูปแบบแท็กนี้ได้ คุณจำเป็นต้องตั้งค่า config ในไฟล์ php.ini<br><br>
					
					<h4><b>HTML script tags</b></h4>
					<ul>แท็ก HTML script มีรูปแบบคือ</ul>
					<p><pre><z style="color:#0000FF;">&lt;script</z> <z style="color:#990099;">language</z>=<z style="color:#009900;">"PHP"</z><z style="color:#0000FF;">&gt;</z>...<z style="color:#0000FF;">&lt;/script&gt;</z></pre></p>
					
					
					<h3><b>Commenting PHP Code</b></h3>
					<ul><p>Comment เป็นส่วนของโปรแกรมที่มีไว้ให้เฉพาะคนที่สามารถอ่านได้ โดยที่โปรแกรมจะไม่ทำการประมวลผลในส่วนนี้ โดยการ comment มีอยู่ 2 รูปแบบคือ</p></ul>
					<b>Single-line comments</b> - ใช้สำหรับการ comment อธิบายสั้นๆ หรือโน้ตเพื่ออธิบายเกี่ยวกับโค้ด  มีตัวอย่างดังนี้
					<p><pre>&lt;? <ul><z style="color:#990000;"> # This is a comment <br> # This is the second line of the comment<br> // This is a comment too. Each style comments only</z><br> <z style="color:#0000FF;">print</z> <z style="color:#009900;">"An example with single line comments"</z>;</ul>?&gt;</pre></p>
					
					<b>Multi-lines comments</b> - ใช้สำหรับการ comment เพื่อแสดง pseudocode อัลกอริทึม และการอธิบายเพิ่มเติมยามจำเป็น หรือโน้ตเพื่ออธิบายเกี่ยวกับโค้ด  มีตัวอย่างดังนี้
					<p><pre>&lt;? <ul><z style="color:#990000;"> /* This is a comment with multiline <br>    Author : Mohammad Mohtashim<br>    Purpose: Multiline Comments Demo<br>    Subject: PHP<br> */</z><br><br> <z style="color:#0000FF;">print</z> <z style="color:#009900;">"An example with multi line comments"</z>;</ul>?&gt;</pre></p>
					
					
					<h3><b>PHP is whitespace insensitive</b></h3>
					<ul><p>Whitespace คือสิ่งที่ป้อนแล้วไม่สามารถมองเห็นจากในหน้าจอได้ ประกอบไปด้วย spaces, tabs, และ carriage returns (end-of-line characters)</p> 
					<p>PHP whitespace insensitive หมายถึงไม่ว่าจะมี whitespace มากเพียงใดก็ไม่สำคัญ</p></ul>
					จากตัวอย่างด้านล่างนี้ แต่ละ statements เป็นการ assign ค่า 2 + 2 ให้กับตัวแปร $four มีค่าเท่ากัน
					<p><pre>$four = 2 + 2; <z style="color:#990000;"> // single space</z><br>$four <z style="color:#009900;">&lt;tab&gt;</z>= 2<z style="color:#009900;">&lt;tab&gt;</z> + <z style="color:#009900;">&lt;tab&gt;</z>2 ; <z style="color:#990000;"> // spaces and tabs</z><br>$four = <br>2+<br>2; <z style="color:#990000;"> // multiple lines</z></pre></p>
					
					
					<h3><b>PHP is case sensitive</b></h3>
					<p><pre>&lt;? <ul>$capital = 67;<br><z style="color:#0000FF;">print</z>(<z style="color:#009900;">"Variable capital is $capital&lt;br&gt;"</z>);<br><z style="color:#0000FF;">print</z>(<z style="color:#009900;">"Variable CaPiTaL is $CaPiTaL&lt;br&gt;"</z>);<br></ul>?>	</pre></p>
					ซึ่งจะให้ผลลัพธ์ดังนี้
					<p><pre>Variable capital is 67<br>Variable CaPiTaL is</pre></p>
					
					
					<h3><b>Statements are expressions terminated by semicolons</b></h3>
					<ul>Statement ใน PHP จะมีรูปแบบคือ expression แล้วตามด้วย semicolon(;)เสมอ  ลำดับของ statementที่ถูกต้องที่อยู่ภายในแท็ก PHP ถือว่าเป็นโค้ดที่ถูกต้อง</ul>ซึ่งด้านล่างนี้เป็นตัวอย่าง statement ของการ assign string ให้กับตัวแปร $greeting
					<p><pre>$greeting = "Welcome to PHP!";</pre></p>
					
					
					<h3><b>Braces make blocks</b></h3>
					<ul>คุณสามารถทำชุดลำดับของ statements ไว้ภายในคู่ของ curly braces ("{ }")</ul>
					โดยทั้ง 2 statement ด้านล่างนี้มีค่าเท่ากัน
					<p><pre><z style="color:#0000FF;">if</z> (3 == 2 + 1)<br><ul><z style="color:#0000FF;">print</z>(<z style="color:#009900;">"Good - I haven\'t totally lost my mind.&lt;br&gt;"</z>);</ul><z style="color:#0000FF;">if</z> (3 == 2 + 1) {	<ul><z style="color:#0000FF;">print</z>(<z style="color:#009900;">"Good - I haven\'t totally"</z>);<br><z style="color:#0000FF;">print</z>(<z style="color:#009900;">"lost my mind.&lt;br&gt;"</z>);</ul>}</pre></p>';
?>

<?php include('single.php'); ?>