<?php
	$page = 10;
	$the_title = 'Web Concepts';
	$the_content = '<p>เนื้อหาส่วนนี้จะแสดงให้เห็นว่า PHP สามารถจะแสดงเนื้อหาออกมาได้ยืดหยุ่นได้มากน้อยเพียงใด  และมีวิธีการอย่างไร
	เช่นการแสดงผลตามชนิดของ browser, การแสดงผลตามการสุ่มตัวเลข  หรือ  ตาม input ของผู้ใช้งาน  เป็นต้น</p>
	
	<h3>การแยกแยะ Browser และ Platform</h3>
	<p>PHP ได้สร้าง <b>environment variables</b> ที่มีประโยชน์ให้กับผู้ใช้ไว้ส่วนนึงแล้ว  ซึ่งสามารถเห็นได้ใน phpinfo.php 
	ซึ่งใช้ในการตั้งค่า PHP environment ต่าง ๆ</p>
	<p>หนึ่งใน environment variables ของ PHP คือ <b>HTTP_USER_AGENT</b> ซึ่งใช้เพื่อระบุ browser และ operating system ของผู้ใช้งาน</p>
	<p>โดย PHP มีคำสั่ง getenv() เพื่อเข้าถึงค่าของ environment variables ทั้งหมด  ซึ่งข้อมูลที่เห็บใน HTTP_USER_AGENT ก็สามารถที่จะใช้เพื่อสร้างการ
	แสดงผลที่เหมาะสมกับ browser แต่ละชนิด</p>
	<p>ด้านล่างนี้จะเป็นตัวอย่างการแสดงผลโดยการแยกแยะ browser และ operating system</p>
	<p><b>NOTE</b> - สำหรับคำสั่ง preg_match() สามารถอ่านคำอธิบายเพิ่มเติมได้ใน <a rel="nofollow" target="_blank" href="https://www.tutorialspoint.com/php/php_regular_expression.htm">PHP Regular expression</a></p>
	<pre class="prettyprint notranslate tryit">
&lt;html&gt;
   &lt;body&gt;
   
      &lt;?php
	 function getBrowser() { 
	    $u_agent = $_SERVER[&#39;HTTP_USER_AGENT&#39;]; 
	    $bname = &#39;Unknown&#39;;
	    $platform = &#39;Unknown&#39;;
	    $version = "";
	    
	    //First get the platform?
	    if (preg_match(&#39;/linux/i&#39;, $u_agent)) {
	       $platform = &#39;linux&#39;;
	    }elseif (preg_match(&#39;/macintosh|mac os x/i&#39;, $u_agent)) {
	       $platform = &#39;mac&#39;;
	    }elseif (preg_match(&#39;/windows|win32/i&#39;, $u_agent)) {
	       $platform = &#39;windows&#39;;
	    }
	    
	    // Next get the name of the useragent yes seperately and for good reason
	    if(preg_match(&#39;/MSIE/i&#39;,$u_agent) && !preg_match(&#39;/Opera/i&#39;,$u_agent)) {
	       $bname = &#39;Internet Explorer&#39;;
	       $ub = "MSIE";
	    } elseif(preg_match(&#39;/Firefox/i&#39;,$u_agent)) {
	       $bname = &#39;Mozilla Firefox&#39;;
	       $ub = "Firefox";
	    } elseif(preg_match(&#39;/Chrome/i&#39;,$u_agent)) {
	       $bname = &#39;Google Chrome&#39;;
	       $ub = "Chrome";
	    }elseif(preg_match(&#39;/Safari/i&#39;,$u_agent)) {
	       $bname = &#39;Apple Safari&#39;;
	       $ub = "Safari";
	    }elseif(preg_match(&#39;/Opera/i&#39;,$u_agent)) {
	       $bname = &#39;Opera&#39;;
	       $ub = "Opera";
	    }elseif(preg_match(&#39;/Netscape/i&#39;,$u_agent)) {
	       $bname = &#39;Netscape&#39;;
	       $ub = "Netscape";
	    }
	    
	    // finally get the correct version number
	    $known = array(&#39;Version&#39;, $ub, &#39;other&#39;);
	    $pattern = &#39;#(?&lt;browser&gt;&#39; . join(&#39;|&#39;, $known) . &#39;)[/ ]+(?&lt;version&gt;[0-9.|a-zA-Z.]*)#&#39;;
	    
	    if (!preg_match_all($pattern, $u_agent, $matches)) {
	       // we have no matching number just continue
	    }
	    
	    // see how many we have
	    $i = count($matches[&#39;browser&#39;]);
	    
	    if ($i != 1) {
	       //we will have two since we are not using &#39;other&#39; argument yet
	       
	       //see if version is before or after the name
	       if (strripos($u_agent,"Version") &lt; strripos($u_agent,$ub)){
	    	  $version= $matches[&#39;version&#39;][0];
	       }else {
	    	  $version= $matches[&#39;version&#39;][1];
	       }
	    }else {
	       $version= $matches[&#39;version&#39;][0];
	    }
	    
	    // check if we have a number
	    if ($version == null || $version == "") {$version = "?";}
	    return array(
	       &#39;userAgent&#39; =&gt; $u_agent,
	       &#39;name&#39;      =&gt; $bname,
	       &#39;version&#39;   =&gt; $version,
	       &#39;platform&#39;  =&gt; $platform,
	       &#39;pattern&#39;   =&gt; $pattern
	    );
	 }
	 
	 // now try it
	 $ua = getBrowser();
	 $yourbrowser = "Your browser: " . $ua[&#39;name&#39;] . " " . $ua[&#39;version&#39;] .
		" on " .$ua[&#39;platform&rsquo;] . " reports: &lt;br &gt;" . $ua[&#39;userAgent&#39;];
	 
	 print_r($yourbrowser);
      ?&gt;
   
   &lt;/body&gt;
&lt;/html&gt;</pre> 
	
	<p>ซึ่งจะแสดงผลไปตาม browser ที่ใช้</p>
	<pre style="background-color:white; ">Your browser: Google Chrome 54.0.2840.99 on windows reports: 
Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36
	</pre>
	
	<h3>การแสดงรูปภาพแบบสุ่ม</h3>
	<p>คำสั่ง <b>rand()</b> ถูกใช้เพื่อสุ่มตัวเลข  ซึ่งคำสั่งนี้สามารถระบุขอบเขตของตัวเลขที่ต้องการสุ่มได้ การจะสุ่มตัวเลขนี้ควรจะสุ่มแบบมีรูปแบบที่ชัดเจน  ดังนั้น
	ควรใช้คำสั่ง <b>srand()</b> ซึ่งสามารถกำหนดเวลาในการสุ่มได้</p>
	<p>ต่อไปจะแสดงตัวอย่างของการแสดงรูปภาพที่แตกต่างกันออกไป  โดยจะเปลี่ยนไปตามเวลาที่กำหนด</p>
	<pre class="prettyprint notranslate">
&lt;html&gt;
   &lt;body&gt;
   
      &lt;?php
         srand( microtime() * 1000000 );
         $num = rand( 1, 4 );
         
         switch( $num ) {
            case 1: $image_file = "/php/images/logo.png";
               break;
            
            case 2: $image_file = "/php/images/php.jpg";
               break;
            
            case 3: $image_file = "/php/images/logo.png";
               break;
            
            case 4: $image_file = "/php/images/php.jpg";
               break;
         }
         echo "Random Image : &lt;img src=$image_file /&gt;";
      ?&gt;
      
   &lt;/body&gt;
&lt;/html&gt;
</pre>
	<p>ซึ่งจะให้ผลลัพธ์ออกมาในลักษณะนี้</p>
	<iframe onload="resizeFrame(this)" class="result" src="random_image.php" height="200px" width=100% style="border: 1px solid #d6d6d6;"></iframe> 
	
	<h3>การใช้ HTML Forms</h3>
	<p>ที่สำคัญทีทำให้เราทราบว่าได้ทำการติดต่อกับ HTML forms และ PHP คือองค์ประกอบของหน้า HTML จะสามารถใช้งาน PHP scripts ได้อย่างอัตโนมัติ</p>
	<p>ลองศึกษาจากตัวอย่างด้านล่าง</p>
<pre class="prettyprint notranslate">
&lt;?php
   if( $_POST["name"] || $_POST["age"] ) {
      if (preg_match("/[^A-Za-z&#39;-]/",$_POST[&#39;name&#39;] )) {
         die ("invalid name and name should be alpha");
      }
      
      echo "Welcome ". $_POST[&#39;name&#39;]. "&lt;br /&gt;";
      echo "You are ". $_POST[&#39;age&#39;]. " years old.";
      
      exit();
   }
?&gt;
&lt;html&gt;
   &lt;body&gt;
   
      &lt;form action = &quot;&lt;?php <b>$_PHP_SELF</b> ?&gt;&quot; method = &quot;POST&quot;&gt;
         Name: &lt;input type = &quot;text&quot; name = &quot;name&quot; /&gt;
         Age: &lt;input type = &quot;text&quot; name = &quot;age&quot; /&gt;
         &lt;input type = &quot;submit&quot; /&gt;
      &lt;/form&gt;
      
   &lt;/body&gt;
&lt;/html&gt;
</pre> 
	<p>ซึ่งทำให้ได้ผลลัพธ์ดังนี้</p>
	<iframe onload="resizeFrame(this)" class="result" src="sample_input.php" height="170px" width="100%" style="border: 1px solid #d6d6d6;"></iframe> 
	<ul class="list"><li><p>ตัวแปร $_PHP_SELF เป็นตัวแปร default ของ PHP ซึ่งจะถูกใช้เพื่อรู้ชื่อของ script และเมื่อผู้ใช้คลิ้กที่ปุ่ม "submit"
	scipt ก็จะเรียกใช้ PHP script ชื่อนั้น และสร้างผลลัพธ์ออกมา</p></li>
	<li><p>method = "POST" ถูกใช้เพื่อ POST ข้อมูลของผู้ใช้ไปให้ server ของ script โดยมีอยู่สอง method ของการ post ข้อมูลไปสู่ server ซึ่งเรื่องนี้จะอธิบายในหัวข้อ <a rel="nofollow" target="_blank" href="https://www.tutorialspoint.com/php/php_get_post.htm">PHP GET&POST</a></p></li></ul>
	
	<h3>Browser Redirection</h3>
	<p>คำสั่ง header() จะส่ง HTTP headers ไปให้ browser และสามารถใช้เพื่อเข้าสู่ location อื่นๆ ได้  Redirection script นี้ควรจะอยู่บนสุดของ code เพื่อให้ส่วนอื่นสามารถดึงเอาไปใช้งานได้</p>
	<p>เป้าหมายจะถูกระบุโดย header <b>Location:</b> ซึ่งเป็น argument ของคำสั่ง header() หลังจากเรียกใช้คำสั่งนี้ คำสั่ง exit() ก็สามารถใช้งานได้เพื่อหยุดการประมวลผล </p>
	<p>ดังตัวอย่างด้านล่างซึ่งแสดงให้เห็นว่าสามารถเรียกใช้งาน browser เพื่อไปยัง web page อื่นได้อย่างไร</p>
<pre class="prettyprint notranslate">
&lt;?php
   if( $_POST["location"] ) {
      $location = $_POST["location"];
      header( "Location:$location" );
      
      exit();
   }
?&gt;
&lt;html&gt;
   &lt;body&gt;
   
      &lt;p&gt;Choose a site to visit :&lt;/p&gt;
      
      &lt;form action = &quot;&lt;?php <b>$_SERVER[&#39;PHP_SELF&#39;]</b> ?&gt;&quot; method =&quot;POST&quot;&gt;
         &lt;select name = "location"&gt;.
         
            &lt;option value = "http://www.tutorialspoint.com"&gt;
               Tutorialspoint.com
            &lt;/option&gt;
         
            &lt;option value = "http://www.google.com"&gt;
               Google Search Page
            &lt;/option&gt;
         
         &lt;/select&gt;
         &lt;input type = &quot;submit&quot; /&gt;
      &lt;/form&gt;
      
   &lt;/body&gt;
&lt;/html&gt;
</pre> 

	<p>ซึ่งจะให้ผลลัพธ์ดังนี้</p>
	<iframe class="result" src="browser_redirection.php" height="100px" width="100%" style="border: 1px solid #d6d6d6;"></iframe>
	
	<h3>การแสดงกล่องข้อความ "File Download"</h3>
	<p>ในบางครั้งถ้าเราอยากให้การแสดงผลเป็นแบบคลิ้กที่ link แล้วมี pop up กล่องข้อความ "File Download" ขึ้นมาแทนที่ content ที่ผู้ใช้งานกำลังดู ณ ปัจจุบันก็มีวิธีที่ง่ายโดยการใช้งานผ่าน HTTP header</p>
	<p>HTTP header จะแตกต่างจาก header อื่น ๆ ที่ส่ง <b>Content-Type</b> เป็น <b>text/html\n\n</b> แต่จะส่งเป็น <b>application/octet-stream</b> และชื่อของ file name จะถูกผูกติดไปด้วย</p>
	<p>ดังตัวอย่าง ถ้าเราต้องการสร้างไฟล์ที่สามารถ download ได้ชื่อว่า <b>Filename</b> จากการให้ link ลักษณะ code ก็จะเป็นดังตัวอย่างด้านล่าง</p>
<pre class="prettyprint notranslate">
#!/usr/bin/perl

# HTTP Header
print "<b>Content-Type:</b>application/octet-stream; name=\"FileName\"\r\n";
print "<b>Content-Disposition:</b> attachment; filename=\"FileName\"\r\n\n";

# Actual File Content
open( FILE, "&lt;FileName" );

while(read(FILE, $buffer, 100) ){
   print("$buffer");
}
</pre> 

	';
?>

<?php include('single.php'); ?>