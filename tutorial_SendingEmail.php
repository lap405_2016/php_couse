<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    background: #CAC5FF;
}
</style>

<?php
  $page = 17;
	$the_title = 'Sending Emails';
	$the_content = '<br><ul>การส่งอีเมล์เป็นเรื่องที่สามารถทำได้ง่ายมาก ๆ ไม่ต้องอาศัย Component เข้ามาช่วยแต่อย่างใด เพียงแต่มี Mail Server หรือ SMTP</ul>
	<p>ที่ใช้สำหรับการทดสอบการส่งอีเมล์ครับ ซึ่งเมื่อนำไปใช้งานจริงบน Web Server หรือ Web Hosting ตามแหล่งต่าง ๆ ทางผู้ให้บริารจะมีให้</p>
	<p>เราพร้อมใช้ได้ในทันทีครับ ไม่ต้องติดตั้งเพิ่มแต่อย่างใด</p>
	</br>
	<p><u>การส่งข้อความอีเมล์ธรรมดา</u></p>
	<p>รูปแบบของการส่งอีเมล์ใน php จะใช้ฟังก์ชัน mail() ซึ่งเราสามารถกำหนดพารามิเตอร์รูปแบบเมล์ต่างๆได้ในฟังก์ชันนี้ดังนี้</p>
	<pre>&nbsp;mail( to, subject, message, headers, parameters );</pre>
    <p>โดยที่พารามิเตอร์ต่างๆมีลักษณะเป็นดังนี้</p>
    <table>
      <tr>
        <th width="80" height="35"><center> Sr.No </center></th>
        <th width="400"><center> Parameter & Description </center></th>
      </tr>
      <tr height="85" valign="top">
        <td >&nbsp;&nbsp;1</td>
        <td>&nbsp;&nbsp;<b>to</b><br><br>&nbsp;&nbsp;ระบุผู้รับ หรือระบุอีเมล์ของผู้รับ</td>
      </tr>
      <tr height="85" valign="top">
        <td>&nbsp;&nbsp;2</td>
        <td>&nbsp;&nbsp;<b>subject</b><br><br>&nbsp;&nbsp;ระบุหัวเรื่องของอีเมล์</td>
      </tr>
      <tr height="85" valign="top">
        <td>&nbsp;&nbsp;3</td>
        <td>&nbsp;&nbsp;<b>message</b><br><br>&nbsp;&nbsp;ระบุเนื้อหาของอีเมล์ หรือข้อความของอีเมล์</td>
      </tr>
      <tr height="85" valign="top">
        <td>&nbsp;&nbsp;4</td>
        <td>&nbsp;&nbsp;<b>headers</b><br><br>&nbsp;&nbsp;ระบุส่วนอ้างอิงที่ส่วนหัวของอีเมล์ เช่น Cc.,Bcc.</td>
      </tr>
      <tr height="85" valign="top">
        <td>&nbsp;&nbsp;5</td>
        <td>&nbsp;&nbsp;<b>parameters</b><br><br>&nbsp;&nbsp;ระบุพารามิเตอร์เพิ่มเติมที่จะใช้ในโปรแกรมส่งอีเมล์</td>
      </tr>
    </table><br><br>
   	<p><u>การส่งข้อความอีเมล์ HTML</u></p>
   	<p>เป็นการส่งอีเมล์โดยกำหนดรูปแบบข้อความเป็นชนิด HTML ที่สามารถปรับแต่งข้อความ หรือแทรก Tag HTML ได้ และการส่งอีเมล์ในรูปแบบภาษาไทย</p>
    <pre>&lt;html&gt;

   &lt;head&gt;
      &lt;title&gt;Sending HTML email using PHP&lt;/title&gt;
   &lt;/head&gt;

   &lt;body&gt;

      &lt;?php
         $to = "xyz@somedomain.com";
         $subject = "This is subject";

         $message = "&lt;b&gt;This is HTML message.&lt;/b&gt;";
         $message .= "&lt;h1&gt;This is headline.&lt;/h1&gt;";

         $header = "From:abc@somedomain.com \r\n";
         $header .= "Cc:afgh@somedomain.com \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";

         $retval = mail ($to,$subject,$message,$header);

         if( $retval == true ) {
            echo "Message sent successfully...";
         }else {
            echo "Message could not be sent...";
         }
      ?&gt;

   &lt;/body&gt;
&lt;/html&gt;</pre><br>
   	<p><u>การส่งไฟล์แนบด้วยอีเมล์</u></p>
   	<p>เป็นการส่งอีเมล์โดยกำหนดรูปแบบข้อความเป็นชนิด HTML ที่สามารถปรับแต่งข้อความ หรือแทรก Tag HTML ได้ และการส่งอีเมล์ในรูปแบบภาษาไทย</p>

<pre>&lt;?php
    $email = new PHPMailer();
    $email->From      = \'you@example.com\';
    $email->FromName  = \'Your Name\';
    $email->Subject   = \'Message Subject\';
    $email->Body      = $bodytext;
    $email->AddAddress( \'destinationaddress@example.com\' );

    $file_to_attach = \'PATH_OF_YOUR_FILE_HERE\';

    $email->AddAttachment( $file_to_attach , \'NameOfFile.pdf\' );

    return $email->Send();
?&gt;</pre>
    ';
?>

<?php include('single.php'); ?>