<?php
  $page = 11;
	$the_title = 'GET & POST Methods';
	$the_content = '<p>มี 2 ทางที่ Client สามารถส่งบ้อมูลไปยัง Web Server</p>
	                <p><ul>1) The Get Method</ul></p>
	                <p><ul>2) The Post Method</ul></p>
	                <p>ก่อนการส่งข้อมูลไป จะมีการเข้ารหัสโดยจะเรียกว่า Url encoding โดยในรูปแบบนี้จะเป็น Name/Value มีการผูกกันและคู่ที่แตกต่างกันจะคั่นด้วย &
	                <pre class="result notranslate">name1=value1&amp;name2=value2&amp;name3=value3</pre>
                    <p>ที่ว่างจะถูกย้ายออกและถูกแทนที่ด้วยสัญลักษณ์ + และอันอื่นๆจะแทนที่ด้วยสัญลักษณ์อื่นๆจะถูกแทนที่ด้วยตัวเลขฐาน 16 หลังจากข้อมูลมีการเข้ารหัสแล้วจะทำการส่งไปยัง server
                    <h3><b>The GET Method</b></h3>
                    <p>วิธีการ Get จะส่งข้อมูลผู้ใช้ที่เข้ารหัสเข้ากับการร้องขอเพจ page และ ข้อมูลจะถูกแยกด้วยเครื่องหมาย ?</p>
                    <pre class="result notranslate">http://www.test.com/index.htm?name1=value1&amp;name2=value2</pre>
                    <p><ul>วิธีการ Get จะสร้าง string ที่จะปรากฎใน Server ของคุณ ใน location ของ Browser : box</ul></p>
                    <p><ul>วิธีการ Get มีข้อจำกัด คือ ได้ไม่เกิน 1024 ตัวอักษร</ul></p>
                    <p><ul>ไม่เคยใช้วิธี Get เมื่อคุณมีรหัสหรีอข้อมูลสำคัญที่ส่งไปยัง Server</ul></p>
                    <p><ul>Get ไม่สามารถใช้ส่งเป็นข้อมูลฐานสอง เช่น รูปภาพ เอกสารword ไปยัง Server</ul></p>
                    <p><ul>ย้อมูลที่ส่งโดยวิธี Get สามารถเข้าถึงโดยใช้ตัวแปร Query_String</ul></p>
                    <p><ul>PHP ให้ $_GET อาเรย์ในการเข้าถึงข้อมูลทั้งหมดที่ส่งโดยวิธีการ GET</ul></p>
                    <p>ตัวอย่าง</p>
                    <pre class="prettyprint notranslate">
&lt;?php
   if( $_GET["name"] || $_GET["age"] ) {
      echo "Welcome ". $_GET[\'name\']. "&lt;br /&gt;";
      echo "You are ". $_GET[\'age\']. " years old.";
      
      exit();
   }
?&gt;
&lt;html&gt;
   &lt;body&gt;
   
      &lt;form action = &quot;&lt;?php <b>$_PHP_SELF</b> ?&gt;&quot; method = &quot;GET&quot;&gt;
         Name: &lt;input type = &quot;text&quot; name = &quot;name&quot; /&gt;
         Age: &lt;input type = &quot;text&quot; name = &quot;age&quot; /&gt;
         &lt;input type = &quot;submit&quot; /&gt;
      &lt;/form&gt;
      
   &lt;/body&gt;
&lt;/html&gt;
</pre> 
                     <p>ผลลัพธ์</p>
                    <pre> <form action="" method="GET"><p>Name: <input type="text" name="name" value="adasf" disabled /></p><p>Age : <input type="text" name="age" value="18" disabled/></p><p>Welcome adasf</p><p>You are 18 years old.</p>
</form></pre>


                     <h3><b>The POST Method</b></h3>
                     <p>ข้อมูลจะถูกส่งผ่านทาง HTTP header ข้อมูลที่ถูกเข้ารหัสได้อธิบายไว้ใน case ของ GET และใส่ลงไปใน header เรียกว่า QUERY_STRING</p>
                     <p><ul>วิธีการ POST ไม่มีข้อจำกัดเกี่ยวกับขนาดข้อมูล</ul></p>
                     <p><ul>วิธีการ POST สามารถส่งเป็น ASCII เช่นเดียวกับ ข้อมูลฐานสอง</ul></p>
                     <p><ul>วิธีการ POST ส่งผ่าน HTTP header ซึ่งความปลอดภัยจะขึ้นกับ HTTP Protocol โดยใช้ Secure HTTP สามารถตรวจสอบความมั่นใจว่าข้อมูลมีความปลอดภัย</ul></p> 
                     <p><ul>PHP ให้ $_POST อาเรย์ ในการเข้าถึงข้อมูลทั้งหมดที่ส่งด้วยวิธีการ POST</ul></p>
                    <p>ตัวอย่าง</p>


                    <pre><br>&lt;?php</br><ul><br>
   if( $_POST["name"] || $_POST["age"] ) {
     if (preg_match("/[^A-Za-z\'-]/",$_POST[\'name\'] )) {
         die ("invalid name and name should be alpha");
      }
      echo "Welcome ". $_POST[\'name\']. "&lt;br /&gt;";
      echo "You are ". $_POST[\'age\']. " years old.";
      
      exit();
   }
?&gt;
&lt;html&gt;
   &lt;body&gt;
   
      &lt;form action = &quot;&lt;?php <b>$_PHP_SELF</b> ?&gt;&quot; method = &quot;POST&quot;&gt;
         Name: &lt;input type = &quot;text&quot; name = &quot;name&quot; /&gt;
         Age: &lt;input type = &quot;text&quot; name = &quot;age&quot; /&gt;
         &lt;input type = &quot;submit&quot; /&gt;
      &lt;/form&gt;
   
   &lt;/body&gt;
&lt;/html&gt;
</pre>
                    <p>ผลลัพธ์</p>
                    <pre> <form action="" method="GET"><p>Name: <input type="text" name="name" value="adasf" disabled /></p><p>Age : <input type="text" name="age" value="18" disabled/></p><p>Welcome adasf</p><p>You are 18 years old.</p>
</form></pre>
                    <h3><b>The $_REQUEST variable</b></h3>
                    <p>$_REQUEST เป็นตัวแปรที่เก็บค่าที่ได้จากพวก $_GET, $_POST และ $_COOKIE เราจะพูดถึง $_COOKIE เมื่อเราจะอธิบายเกี่ยวกับ cookies</p>
                    <p>$_REQUEST สามารถ get ผลลัพธ์ จาก ข้อมูลท่ส่ง ทั้งวิธีการ GET และ POST</p>
                    <p>ตัวอย่าง</p>
                    <pre class="prettyprint notranslate">
&lt;?php
   if( $_REQUEST["name"] || $_REQUEST["age"] ) {
      echo "Welcome ". $_REQUEST[\'name\']. "&lt;br /&gt;";
      echo "You are ". $_REQUEST[\'age\']. " years old.";
      exit();
   }
?&gt;
&lt;html&gt;
   &lt;body&gt;
      
      &lt;form action = &quot;&lt;?php <b>$_PHP_SELF</b> ?&gt;&quot; method = &quot;POST&quot;&gt;
         Name: &lt;input type = &quot;text&quot; name = &quot;name&quot; /&gt;
         Age: &lt;input type = &quot;text&quot; name = &quot;age&quot; /&gt;
         &lt;input type = &quot;submit&quot; /&gt;
      &lt;/form&gt;
      
   &lt;/body&gt;
&lt;/html&gt;
</pre> 
                    <p>ใน $_PHP_SELF มีชื่อเรียก self script ที่ถูกเรียกอยู่</p>
                    <p>ผลลัพธ์</p>
                    <pre> <form action="" method="GET"><p>Name: <input type="text" name="name" value="adasf" disabled /></p><p>Age : <input type="text" name="age" value="18" disabled/></p><p>Welcome adasf</p><p>You are 18 years old.</p>
</form></pre>







';
?>

<?php include('single.php'); ?>