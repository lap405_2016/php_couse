<?php
	$page = 31;
	$the_title = 'Phalcon Installation';
	$the_content = "
	<div> 
		<ul>
			<img src='images/phalconn.png' class='img-responsive' > 
		</ul>
	</div>
	<div>
		<p>
			<ul> &nbsp&nbsp&nbsp&nbsp&nbsp&nbspเป็นเครื่องมือที่ช่วยในการพัฒนาเวบแอพพลิเคชันด้วย PHP โดย Phalcon เฟรมเวิร์คที่รองรับในทุกขั้นตอนของการพัฒนา เช่น การออกแบบ UI ฐานข้อมูล การทดสอบยูนิตต่างๆ สถาปัตยกรรมการออกแบบเป็นแบบ MVC (Model-View-Control) ซึ่งมีข้อดีคือการพัฒนาแอพพลิเคชันจัดแบ่งเป็นส่วนๆ อย่างชัดเจน ทำให้การปรับปรุงแก้ไขทำได้ง่าย และสามารถร่วมกันพัฒนากับผู้อื่นได้
			และอีกทั้ง ยังรองรับ ORM (Object Relational Mapping) ทำให้การเข้าถึงฐานข้อมูล (ผ่าน model) ทำได้ง่าย และ รองรับ ODM (Object Document Mapper) สำหรับฐานข้อมูลแบบ NoSQL (Not Only SQL)</ul>
		</p>
		
	</div>

	<div>
		<p>
			<ul>
				<h3>
					<b> Installation </b><br>					
				</h3> 
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ก่อนที่จะเริ่มการติดตั้ง Phalcon จำเป็นต้องตรวจสอบสภาพแวดล้อมของระบบก่อน ซึ่งทางผู้จัดทำจะทำการแสดงตัวอย่างการติดตั้ง Phalcon ภายใน xampp <br><br>
				1. เข้าไปที่ http://localhost/xampp/ และเลือกเมนู Php > phpinfo()

				<img src='images/tutorial-phalcon-basic-1-2.png' class='img-responsive' > <br>

				เมื่อตรวจสอบเสร็จแล้วจะได้ เวอร์ชั่นของ PHP (5.4.19), PHP Compiler (MSVC9), และ PHP Architercture (x86)<br><br>

				2. เข้าไปที่เว็บหลัก http://phalconphp.com/en/download/windows/ เพื่อ เลือกดาวโหลดไฟล์ Phalcon Framework  <br><br>

				<img src='images/tutorial-phalcon-basic-1-3.png' class='img-responsive' > <br>

				เมื่อทำการแตกไฟล์ phalcon_x86_VC9_php5.4.0_1.3.4.zip ที่ได้มาหลังการดาวโหลด และเราจะได้ไฟล์ php_phalcon.dll <br><br>

				<img src='images/tutorial-phalcon-basic-1-4.png' class='img-responsive' > <br>

				3. ให้เราทำการ Copy ไฟล์ 'php_phalcon.dll' ไปไว้ในโฟล์เดอร์ Extension ของ PHP ตามตัวอย่างผมติดตั้ง XAMPP Server (D:\xampp\php\ext\) <br><br>

				<img src='images/tutorial-phalcon-basic-1-5.png' class='img-responsive' > <br>

				4. แก้ไขไฟล์ php.ini เพื่อทำการตั้งค่าให้ PHP Server รู้จักกับ Phalcon Framework v.1.3.4 นั้นสามารถทำงานได้ เโดยทำการพิ่มเติมคำสั่ง extension=php_phalcon.dll ไว้บรรทัดล่างสุดของไฟล์ จากนั้นบันทึกไฟล์ และ รีสตาร์ Web Server 1 ครั้ง <br><br>

				<img src='images/tutorial-phalcon-basic-1-6.png' class='img-responsive' > <br>

				5. ตรวจสอบการติดตั้งและการทำงาน Phalcon Framework v.1.3.4 บน PHP Server (localhost) ภายในเครื่องของเรา โดยเข้าไปที่ลิ้งค์ http://localhost/xampp/ และเลือกเมนู Php > phpinfo() จากนั้นทำการกดปุ่ม Ctrl + F และพิมพ์คำว่า Phalcon Framework หากติดตั้ง Phalcon Framework v.1.3.4 สำเร็จ ก็จะได้ ดังรูปภาพด้านล่าง <br><br>

				<img src='images/tutorial-phalcon-basic-1-7.png' class='img-responsive' > <br>



			</ul>
		</p>
		
	</div>

	";
?>

<?php include('single.php'); ?>