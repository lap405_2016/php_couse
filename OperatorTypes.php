<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    background: #CAC5FF;
}
</style>


<?php
  $page = 5;
	$the_title = 'Operator Types';
	$the_content = '<p>Operator หรือตัวดำเนินการนั้น ใช้เพื่อการแสดงผลทางคณิตศาสตร์ ตรรกศาสตร์ รวมถึงการเปรียบเทียบ และเงื่อนไขการทำงานต่างๆ</p>
	</br>
	<p><u>ประเภทของตัวดำเนินการใน PHP</u></p>
	<p>1. Arithmetic Operators หรือตัวดำเนินการทางคณิตศาตร์</p>
	<p>2. Comparison Operators หรือตัวดำเนินการทางการเปรียบเทียบ</p>
	<p>3. Logical (or Relational) Operators หรือตัวดำเนินการทางตรรกศาสตร์</p>
	<p>4. Assignment Operators หรือตัวดำเนินการการกำหนดค่า</p>
	<p>5. Conditional (or ternary) Operators หรือตัวดำเนินการการสร้างเงื่อนไขการทำงาน</p></br>

	<p><u>สัญลักษณ์ และการใช้งานตัวดำเนินการใน PHP</u></p>
    <p><b>Arithmetic Operators</b></p>
    <table>
      <tr>
        <th width="120" height="35"><center> Operator </center></th>
        <th width="300"><center> Description </center></th>
        <th width="200"><center> Example </center></th>
      </tr>
      <tr height="65" valign="top">
        <td >&nbsp;&nbsp;+</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการทางคณิตศาตร์ "บวก" ทำการบวก หรือเพิ่มค่า&nbsp;&nbsp;<br>&nbsp;&nbsp;ระหว่างตัวถูกดำเนินการ&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;A + B will give 30</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;-</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการทางคณิตศาตร์ "ลบ" ทำการลบ หรือลดค่า&nbsp;&nbsp;<br>&nbsp;&nbsp;ระหว่างตัวถูกดำเนินการ&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;A - B will give -10</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;*</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการทางคณิตศาตร์ "คูณ" ทำการคูณ หรือทวีค่า&nbsp;&nbsp;<br>&nbsp;&nbsp;ระหว่างตัวถูกดำเนินการ&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;A * B will give 200</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;/</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการทางคณิตศาตร์ "หาร" ทำการหาร หรือถอนค่า&nbsp;&nbsp;<br>&nbsp;&nbsp;ระหว่างตัวถูกดำเนินการ&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;B / A will give 2</td>
      </tr>
      <tr height="ึ65" valign="top">
        <td>&nbsp;&nbsp;%</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการทางคณิตศาตร์ "หารแล้วเอาแต่เศษ" ทำการ&nbsp;&nbsp;<br>&nbsp;&nbsp;หารแล้วเอาแต่เศษระหว่างตัวถูกดำเนินการ&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;B % A will give 0</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;++</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการทางคณิตศาตร์ "เพิ่มค่า" ทำการเพิ่มค่าให้ตัว&nbsp;&nbsp;<br>&nbsp;&nbsp;ดำเนินการ 1&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;A++ will give 11</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;--</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการทางคณิตศาตร์ "หลดค่า" ทำการลดค่าตัว&nbsp;&nbsp;<br>&nbsp;&nbsp;ตำเนินการ 1&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;A-- will give 9</td>
      </tr>
    </table><br>

    <p><b>Comparison Operators</b></p>
    <table>
      <tr>
        <th width="120" height="35"><center> Operator </center></th>
        <th width="300"><center> Description </center></th>
        <th width="200"><center> Example </center></th>
      </tr>
      <tr height="65" valign="top">
        <td >&nbsp;&nbsp;==</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการเปรียบเทียบ "ตรวจสอบว่าเท่ากันหรือไม่" ทำการตรวจสอบ&nbsp;&nbsp;<br>&nbsp;&nbsp;ว่าตัวถูกดำเนินการเท่ากันหรือไม่เท่ากัน&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;(A == B) is not true.</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;!=</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการเปรียบเทียบ "ตรวจสอบว่าไม่เท่ากันหรือไม่" ทำการตรวจสอบ&nbsp;&nbsp;<br>&nbsp;&nbsp;ว่าตัวถูกดำเนินการไม่เท่ากันใช่หรือไม่&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;(A != B) is true.</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;></td>
        <td>&nbsp;&nbsp;ตัวดำเนินการเปรียบเทียบ "ตรวจสอบว่ามากกว่าหรือไม่" ทำการตรวจสอบ&nbsp;&nbsp;<br>&nbsp;&nbsp;ว่าตัวถูกดำเนินการฝั่งซ้ายมากกว่าตัวถูกดำเนินการฝั่งขวาหรือไม่&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;(A > B) is not true.</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;<</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการเปรียบเทียบ "ตรวจสอบว่าน้อยกว่าหรือไม่" ทำการตรวจสอบ&nbsp;&nbsp;<br>&nbsp;&nbsp;ว่าตัวถูกดำเนินการฝั่งซ้ายน้อยกว่าตัวถูกดำเนินการฝั่งขวาหรือไม่&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;(A < B) is true.</td>
      </tr>
      <tr height="ึ65" valign="top">
        <td>&nbsp;&nbsp;>=</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการเปรียบเทียบ "ตรวจสอบว่ามากกว่าหรือเท่ากับหรือไม่" ทำการ&nbsp;&nbsp;<br>&nbsp;&nbsp;ตรวจสอบว่าตัวถูกดำเนินการฝั่งซ้ายมากกว่าหรือเท่ากับตัวถูกดำเนินการฝั่ง&nbsp;&nbsp;<br>&nbsp;&nbsp;ขวาหรือไม่&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;(A >= B) is not true.</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;<=</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการเปรียบเทียบ "ตรวจสอบว่าน้อยกว่าหรือเท่ากับหรือไม่" ทำการ&nbsp;&nbsp;<br>&nbsp;&nbsp;ตรวจสอบว่าตัวถูกดำเนินการฝั่งซ้ายน้อยกว่าหรือเท่ากับตัวถูกดำเนินการฝั่ง&nbsp;&nbsp;<br>&nbsp;&nbsp;ขวาหรือไม่&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;(A <= B) is true.</td>
      </tr>
    </table><br>

    <p><b>Logical Operators</b></p>
    <table>
      <tr>
        <th width="120" height="35"><center> Operator </center></th>
        <th width="300"><center> Description </center></th>
        <th width="200"><center> Example </center></th>
      </tr>
      <tr height="65" valign="top">
        <td >&nbsp;&nbsp;and</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการตรรกศาสตร์ "และ" คือถ้าเป็นจริงใน&nbsp;&nbsp;<br>&nbsp;&nbsp;ทุกๆตัวถูกดำเนินการ จะทำให้เงื่อนไขเป็นจริง&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;(A and B) is true.</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;or</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการตรรกศาสตร์ "หรือ" คือถ้าเป็นจริงเพียง&nbsp;&nbsp;<br>&nbsp;&nbsp;ตัวหนึ่งในตัวถูกดำเนินการ จะทำให้เงื่อนไขเป็นจริง&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;(A or B) is true.</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;&&</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการตรรกศาสตร์ "และ" คือถ้าไม่เป็นศูนย์ใน&nbsp;&nbsp;<br>&nbsp;&nbsp;ทุกๆตัวถูกดำเนินการ จะทำให้เงื่อนไขเป็นจริง&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;(A && B) is true.</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;||</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการตรรกศาสตร์ "หรือ" คือถ้าไม่เป็นศูนย์เพียง&nbsp;&nbsp;<br>&nbsp;&nbsp;ตัวหนึ่งในตัวถูกดำเนินการ จะทำให้เงื่อนไขเป็นจริง&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;(A || B) is true.</td>
      </tr>
      <tr height="ึ65" valign="top">
        <td>&nbsp;&nbsp;!</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการตรรกศาสตร์ "ไม่เท่ากับ" ใช้เพื่อหาสิ่งที่&nbsp;&nbsp;<br>&nbsp;&nbsp;ตรงข้ามกับตัวดำเนินการนั้นๆ ซึ่งให้เงื่อนไขจะเป็น&nbsp;&nbsp;<br>&nbsp;&nbsp;จริงเมื่อตัวดำเนินการนี้ไม่เป็นจริง&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;!(A && B) is false.</td>
      </tr>
    </table><br>

    <p><b>Assignment Operators</b></p>
    <table>
      <tr>
        <th width="120" height="35"><center> Operator </center></th>
        <th width="300"><center> Description </center></th>
        <th width="200"><center> Example </center></th>
      </tr>
      <tr height="65" valign="top">
        <td >&nbsp;&nbsp;=</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการกำหนดค่า "เท่ากับ" ทำการกำหนดค่า&nbsp;&nbsp;<br>&nbsp;&nbsp;ตัวถูกดำเนินการฝั่งขวาให้เท่ากับทางฝั่งซ้ายของตัว&nbsp;&nbsp;<br>&nbsp;&nbsp;ดำเนินการ&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;C=A+B will assign value of A+B&nbsp;&nbsp;<br>&nbsp;&nbsp;into C</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;+=</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการกำหนดค่า "บวกเท่ากับ" ทำการกำหนดค่า&nbsp;&nbsp;<br>&nbsp;&nbsp;ตัวถูกดำเนินการฝั่งขวาให้บวก หรือเพิ่มค่าด้วยค่าทางฝั่ง&nbsp;&nbsp;<br>&nbsp;&nbsp;ซ้ายของตัวดำเนินการ&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;C += A is equivalent to C=C+A</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;-=</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการกำหนดค่า "ลบเท่ากับ" ทำการกำหนดค่า&nbsp;&nbsp;<br>&nbsp;&nbsp;ตัวถูกดำเนินการฝั่งขวาให้ลบ หรือลดค่าด้วยค่าทางฝั่ง&nbsp;&nbsp;<br>&nbsp;&nbsp;ซ้ายของตัวดำเนินการ&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;C -= A is equivalent to C=C-A</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;*=</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการกำหนดค่า "คูณเท่ากับ" ทำการกำหนดค่า&nbsp;&nbsp;<br>&nbsp;&nbsp;ตัวถูกดำเนินการฝั่งขวาให้คูณ หรือทวีค่าด้วยค่าทางฝั่ง&nbsp;&nbsp;<br>&nbsp;&nbsp;ซ้ายของตัวดำเนินการ&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;C *= A is equivalent to C=C*A</td>
      </tr>
      <tr height="ึ65" valign="top">
        <td>&nbsp;&nbsp;/=</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการกำหนดค่า "หารเท่ากับ" ทำการกำหนดค่า&nbsp;&nbsp;<br>&nbsp;&nbsp;ตัวถูกดำเนินการฝั่งขวาให้หาร หรือถอนค่าด้วยค่าทางฝั่ง&nbsp;&nbsp;<br>&nbsp;&nbsp;ซ้ายของตัวดำเนินการ&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;C /= A is equivalent to C=C/A</td>
      </tr>
      <tr height="65" valign="top">
        <td>&nbsp;&nbsp;%=</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการกำหนดค่า "หารเอาแต่เศษเท่ากับ" ทำการ&nbsp;&nbsp;<br>&nbsp;&nbsp;กำหนดค่าตัวถูกดำเนินการฝั่งขวาให้หารเอาแต่เศษด้วย&nbsp;&nbsp;<br>&nbsp;&nbsp;ค่าทางฝั่งซ้ายของตัวดำเนินการ&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;C %= A is equivalent to C=C%A</td>
      </tr>
    </table><br>

    <p><b>Conditional Operators</b></p>
    <table>
      <tr>
        <th width="120" height="35"><center> Operator </center></th>
        <th width="300"><center> Description </center></th>
        <th width="220"><center> Example </center></th>
      </tr>
      <tr height="65" valign="top">
        <td >&nbsp;&nbsp;? :</td>
        <td>&nbsp;&nbsp;ตัวดำเนินการกำหนดเงื่อนไข "นิพจน์เงื่อนไข" ทำการ&nbsp;&nbsp;<br>&nbsp;&nbsp;กำหนดค่าของตัวถูกดำเนินการเมื่อตรงกับเงื่อนไข และ&nbsp;&nbsp;<br>&nbsp;&nbsp;ไม่ตรงกับเงื่อนไข&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;If Condition is true ? Then &nbsp;&nbsp;<br>&nbsp;&nbsp;value X : Otherwise value Y</td>
      </tr>
    </table><br>
    ';
?>

<?php include('single.php'); ?>
