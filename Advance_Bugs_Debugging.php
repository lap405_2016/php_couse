<?php
	$page = 23;
	$the_title = 'Bugs Debugging';
	$the_content = '<p>โปรแกรมที่ทำงานได้ไม่ค่อยถูกต้องในครั้งแรก หลายอย่างสามารถผิดในโปรแกรมของคุณ คุณมีทางเลือกเกี่ยวกับข้อความที่ผิดพลาด ข้อความสามารถส่งไปพร้อมกับ Output ของโปรแกรมอื่นๆบน Web Browser มันรวมอยู่ใน web server error log</p>
	                <p>ทำจอแสดงผลข้อความ error ใน browser ตั้งค่า display_errors ให้เป็น on จะส่ง error ไปยัง web server error log ตั้งค่า log_errors เป็น on คุณสามารถตั้วค่าทั้งคู่ ถ้าต้องการข้อความผิดพลาดทั้งคู่</p>
	                <p>PHP กำหนดค่าคงที่ คุณสามารถตั้งค่าของ error_reporting ข้อผิดพลาดของบางประเภทได้รับรายงาน : E_ALL (for all errors except strict notices), E_PARSE (parse errors), E_ERROR (fatal errors), E_WARNING (warnings), E_NOTICE (notices), and E_STRICT (strict notices)</p><p>ในขณะที่เขียนโปรแกรม PHP มันเป็นความคิดที่ดีที่จะใช้ PHP-aware editors เหมือน BBEdit หรือ Emacs 1 feature พิเศษของ editor คือ systax highlighting มันจะเปลี่ยนสีของส่วนต่างๆ ตัวอย่างเช่น string เป็นสีชมพู if หรือ while เป็น สีน้ำเงิน comment เป็นสีเทา และตัวแปรเป็นสีดำ</p>
	                <p>feature อื่นๆ เป็นการจับคู่ quote และ bracket ซึ่งช่วยให้แน่ใจกับสมดุลของ quotes and brackets เมื่อคุณใช้ปิดด้วย } ตัว highlightจะ เปิด { ในการจับคู่</p>
	                <p>จุดต่อไปนี้จำเป็นในการดูการ debugging ของโปรแกรมคุณ</p>
	                <p><ul><b>Missing Semicolons</b> - ทุกๆการจบ state ต้องมี (;) ปิดท้าย PHP จะไม่หยุดอ่าน state จนกระทั่ง เจอ semicolon ถ้าออกจาก semicolon ที่บรรทัดจบ PHP จะอ่านต่อไปบนบรรทัดต่อไป</ul></p>
	                <p><ul><b>Not Enough Equal Signs</b> - เมื่อคุณถามว่าค่า 2 ค่าที่เปรียบเทียบกันเท่ากัน คุณจำเป็นต้องใช้ (==) ใช้เครื่องหมาย = เป็นข้อผิดพลาดที่พบบ่อย</ul></p>
	                <p><ul><b>Misspelled Variable Names</b> - ถ้าสะกดตัวแปรผิดมันจะกลายเป็นอีกตัวแปรนึงไปเลย $test ไม่เท่ากับ $Test</ul></p>
	                <p><ul><b>Missing Dollar Signs</b> - การไม่ใส่ $ หน้าตัวแปรเป็นเรื่องยากที่จะเห็น แต่มันก็ส่งผลในข้อผิดพลาด</ul></p>
	                <p><ul><b>Troubling Quotes</b> - มีมากไป หรือ น้อยไป ควรจะมีจำนวนของ quotes ให้เหมาะสม</ul></p>
	                <p><ul><b>Missing Parentheses and curly brackets</b> - ควรจะเป็นคู่ </ul></p>
	                <p><ul><b>Array Index</b> - อาเรย์ทั้งหมดควรจะเริ่มจาก 0 แทนที่ 1</ul></p>
	                <p>นอกจากนี้ จัดการข้อผิดพลาดทั้งหมด และข้อความโดยตรงใน system log file ดังนั้น ถ้าเกิดปัญหาขึ้นแล้ว มันจะบันทึกไว้ใน system log file คุณสามารถ debug เมื่อเกิดปัญหาได้</p>';
?>

<?php include('single.php'); ?>