<?php
$page = 6;
	$the_title = 'Decision Making';
	$the_content = '<p><ul>คำสั่ง if,elseif...else และ switch มีการใช้งานที่แตกต่างกันออกไป</ul></p>
	                <p><ul>คุณสามารถเรียกใช้งานcondition ตามความเหมาะสม หรือการตัดสินใจของคุณเอง</ul></p>
	                <ul><img src="images/decision_making.jpg" alt="Decision making statements in PHP"/></ul>
                    <p><ul><b>if else:</b>ใช้เงื่อนไขนี้ถ้าเงื่อนไขเป็นจริงและเอีกเงื่อนไขเป็นเท็จ</ul></p>
                    <p><ul><b>elseif:</b>จะใช้เมื่อมีหลาย condition ที่เป็นจริง</ul></p>
                    <p><ul><b>switch:</b>ใช้เมื่อต้องการเลือก หนึ่งในหลายๆกลุ่ม เป็นการหลีกเลี่ยงการใช้ if else ยาวๆ</ul></p>
                    <h3><b>if..else</b></h3>
                    <h4><b><br>Syntax<br></b></h4>
                    <pre><br>if (<i>condition</i>)</br><ul><i>code to be executed if condition is true</i>;</ul>else<br><ul><i>code to be executed if condition is false;</ul></i></br></pre>
                    <h4><b>example</b></h4>
                    <p><ul>ตัวอย่างต่อไปนี้แสดง output "yes!" ถ้าเป็นHuman แต่ถ้า output "No!" คือวันที่ไม่ใช่Human</ul></p>
                    <pre class="prettyprint notranslate tryit"><br>&lt;?php</br><ul><br>$d = "human";</br>if ($d == &quot;human&quot;)    <ul>echo &quot;yes!&quot;;</ul>else    <br><ul>echo &quot;no!&quot;;</ul></br></ul> <br>?&gt;</br> </pre>
                    <p>ผลลัพธ์</p>
                    <pre> yes!</pre>
                    <h3><b>elseif</b></h3>
                    <h4><br><b>Syntax</b><br></h4>
                    <pre><br>if (<i>condition</i>)</br><ul><i>code to be executed if condition is true</i>;</ul><br>elseif (<i>condition</i>)</br><ul><i>code to be executed if condition is true</i>;</ul>else<br><ul><i>code to be executed if condition is false;</ul></i></br></pre>
                    <h4><b>example</b></h4>
                    <p><ul>ตัวอย่างต่อไปนี้แสดง output "very good!" คือจะได้เกรดเอ ถ้า output "good!" คือเงื่อนไขจะไปตรงเกรดเท่ากับบี แต่ถ้าไม่ตรงตามเงื่อนไขเลย จะได้ output soso! </ul></p>
                    <pre>?php</br><ul><br>$grade = "B";</br><br>if ($grade == "A")</br><ul><i>echo "very good!"</i>;</ul><br>elseif ($grade == "B")</br><ul><i>echo "good!"</i>;</ul>else<br><ul><i>echo soso;</ul></i></br></ul>?&gt;</pre>
                    <p>ผลลัพธ์</p>
                    <pre> good!</pre>
                    <h3><b>Switch</b></h3>
                    <h4><b><br>Syntax<br></b></h4>
                    <pre class="result notranslate">switch (<i>expression</i>){ <ul>case <i>label1:</i><ul><i>code to be executed if expression = label1;</i></ul><ul>break;</ul></ul><ul>case <i>label2:</i><br><ul><i>code to be executed if expression = label2;</i></br></ul><ul>break;</ul><ul>default:</ul></ul><ul><i>code to be executed</ul><ul>if expression is different</ul><ul>from both label1 and label2;</ul></i>}</pre>
                    <h4><b>example</b></h4>
                    <p><ul>ตัวอย่างการแสดงการทำงานของการใช้ switch มันจะแสดงค่าที่ตรงกันออกมา ถ้าค่าไม่ตรงกันเลยมันจะแสดงค่าจาก Default ออกมา</ul></p>
                    <pre class="prettyprint notranslate tryit">  &lt;?php
         $grade = date("C");
         
         switch ($grade){
            case "A":
               echo &quot;this is A&quot;;
               break;
            
            case "B":
               echo &quot;This is B&quot;;
               break;
            
            case "C":
               echo &quot;This is C&quot;;
               break;
            
            case "D":
               echo &quot;This is D&quot;;
               break;
            
            case "F":
               echo &quot;This is F&quot;;
               break;
            
            default:
               echo &quot;forget for grade&quot;;
         }
      ?&gt;
      </pre>
                    <p>ผลลัพธ์</p>
                    <pre> This is C</pre>


';
?>

<?php include('single.php'); ?>