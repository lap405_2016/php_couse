<?php
    $page = 14;
	$the_title = 'Functions';
	$the_content = '<ul>function เป็นการทำงานส่วนหนึ่งของ code โดยต้องการอย่างน้อย 1 input เพื่อนำไปประมวลผลการทำงานเพื่อให้ได้ค่าคำตอบออก</ul>
	<p>มา หรือก็คือ function เป็น code ชุดหนึ่งที่มีกระบวนการทำงานภายในชุด code นั้นๆ ซึ่งสามารถนำไปใช้ซ้ำโดยการเรียกด้วยชื่อ function </p>
	<p>แทนการเขียน code ใหม่อีกรอบได้</p>
	</br>
	<p><u>การสร้าง function เบื้องต้น</u></p>
	<p>ฟังก์ชันสำหรับเขียนข้อความ</p>
	<pre>&lt?php<br>   /*Defining a PHP Function*/<br>   function writeMessage() {<br>      echo "You are really a nice person, Have a nice time!";<br>   }<br><br>   /*Calling a PHP Function*/<br>   writeMessage();<br>?&gt</pre>
	<p>Result:<pre>You are really a nice person, Have a nice time!<br></pre></p><br>
    <p><u>การสร้าง function โดยใช้พารามิเตอร์</u></p>
	<p>ก่อนที่จะเขียนการทำงานภายในฟังก์ชัน ต้องประกาศพารามิเตอร์ที่จะใช้ภายในฟังก์ชันก่อน ซึ่งต้องการใช้กี่พารามิเตอร์ก็ใส่ได้ตามที่ต้องการ</p>
	<pre>&lt?php<br>   function addFunction($num1, $num2) {<br>      $sum = $num1 + $num2;<br>      echo "Sum of the two numbers is : $sum";<br>   }<br><br>   addFunction(10, 20);<br>?&gt</pre>
	<p>Result:<pre>Sum of the two numbers is : 30<br></pre></p><br>
    <p><u>การส่งผ่านค่าพารามิเตอร์ด้วยฟังก์ชัน</u></p>
    <pre>&lt?php<br>   function addFive($num) {<br>      $num += 5;<br>   }<br><br>   function addSix(&$num) {<br>      $num += 6;<br>   }<br><br>   $orignum = 10;<br>   addFive( $orignum );<br><br>   echo "Original Value is $orignum &ltbr/&gt";<br><br>   addSix( $orignum );<br>   echo "Original Value is $orignum &ltbr/&gt";<br>?&gt</pre>
    <p>Result:<pre>Original Value is 10<br>Original Value is 16<br></pre></p><br>
    <p><u>การคืนค่าด้วยการทำงานของฟังก์ชัน</u></p>
    <pre>&lt?php<br>   function addFunction($num1, $num2) {<br>      $sum = $num1 + $num2;<br>      return $sum;<br>   }<br>   $return_value = addFunction(10, 20);<br><br>   echo "Returned value from the function : $return_value";<br>?&gt</pre>
    <p>Result:<pre>Returned value from the function : 30<br></pre></p><br>
    <p><u>การตั้งค่าพื้นฐานสำหรับพารามิเตอร์ของฟังก์ชัน</u></p>
    <pre>&lt?php<br>   function printMe($param = NULL) {<br>      print $param;<br>   }<br><br>   printMe("This is test");<br>   printMe();<br>?&gt</pre>
    <p>Result:<pre>This is test</pre></p><br>
    <p><u>การเรียกใช้ฟังก์ชันแบบไดนามิก</u></p>
    <pre>&lt?php<br>   function sayHello() {<br>      echo "Hello<br />";<br>   }<br><br>   $function_holder = "sayHello";<br>   $function_holder();<br>?&gt</pre>
    <p>Result:<pre>Hello</pre></p><br>
    ';
?>

<?php include('single.php'); ?>