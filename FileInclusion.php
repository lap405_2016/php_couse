<?php
	$page = 12;
	$the_title = 'File Inclusion';
	$the_content = '<p><ul>คุณสามารถรวมเนื้อหา(include) PHP ไฟล์หนึ่งไปไว้ใน PHP อีกไฟล์หนึ่งก่อนที่เซิร์ฟเวอร์จะประมวลผลไฟล์นั้น ซึ่ง PHP มีฟังก์ชันอยู่2ฟังก์ชันที่สามารถรวมเนื้อหา(include) PHP ไฟล์หนึ่งไปไว้ใน PHP อีกไฟล์หนึ่ง</ul></p>
	<ul><ul><p><li>include()</li></p>
	<p><li>require()</li></p></ul></ul>
	<p><ul>ซึ่งสิ่งนี้ถือเป็นจุดเด่นที่ช่วยให้การสร้าง ฟังก์ชัน , header , footer , หรือ องค์ประกอบต่างๆ สามารถใช้ซ้ำในหลายๆเพจได้  <br>ช่วยในการพัฒนาทำให้ง่ายแก่การเปลี่ยนแปลง หากต้องมีการเปลี่ยนแปลงก็สามารถเปลี่ยนแปลงไฟล์เดียวได้เลย ไม่จำเป็นต้องไปเปลี่ยนแปลงทุกๆไฟล์ในกรณีที่ไม่ได้ใช้การ include </ul></p>
	
	<h3><b>The include() Function</b></h3>
	<p><ul><b>include()</b> เป็นฟังก์ชันเป็นการเอาข้อความเนื้อหาทั้งหมดในไฟล์ที่ระบุและคัดลอกมายังไฟล์ที่มีการใช้ฟังก์ชัน <b>include()</b> นั้น <br>หากมีปัญหาในการโหลดไฟล์ <b>include()</b> ฟังก์ชันจะมีการแจ้งเตือนแต่การประมวลผลของสคริปจะยังทำงานต่อไป</ul><p>
	<p>สมมติว่าต้องการสร้างไฟล์ menu.php สำหรับเว็บไซต์ ที่มีเนื้อหาดังนี้</p>
	<p><pre><z style="color:#0000FF;">&lt;a</z> <z style="color:#990099;">href</z>=<z style="color:#009900;">"http://www.tutorialspoint.com/index.htm"</z><z style="color:#0000FF;">></z>Home<z style="color:#0000FF;">&lt;/a&gt;</z>&lt;br&gt; <br><z style="color:#0000FF;">&lt;a</z> <z style="color:#990099;">href</z>=<z style="color:#009900;">"http://www.tutorialspoint.com/ebxml"</z><z style="color:#0000FF;">></z>ebXML<z style="color:#0000FF;">&lt;/a&gt;</z>&lt;br&gt; <br><z style="color:#0000FF;">&lt;a</z> <z style="color:#990099;">href</z>=<z style="color:#009900;">"http://www.tutorialspoint.com/ajax"</z><z style="color:#0000FF;">></z>AJAX<z style="color:#0000FF;">&lt;/a&gt;</z>&lt;br&gt; <br><z style="color:#0000FF;">&lt;a</z> <z style="color:#990099;">href</z>=<z style="color:#009900;">"http://www.tutorialspoint.com/perl"</z><z style="color:#0000FF;">></z>PERL<z style="color:#0000FF;">&lt;/a&gt;</z>&lt;br&gt;</pre></p>
	<p>หลังจากนี้คุณสามารถสร้างเพจขึ้นมากี่หน้าก็ได้แล้วอาศัยการ include ไฟล์นี้เพื่อสร้าง header สมมติสร้างไฟล์ test.php ซึ่งมีเนื้อหาดังนี้</p>
	<p><pre>&lt;?php <b>include</b>(<z style="color:#009900;">"menu.php"</z>); ?> <br><z style="color:#0000FF;">&lt;p&gt;</z>This is an example to show how to include PHP file!<z style="color:#0000FF;">&lt;p&gt;</z></pre><p>
	<p>ซึ่งจะให้ผลลัพธ์ดังนี้</p>
	<p><pre><a href="http://www.tutorialspoint.com/index.htm">Home</a> <br><a href="http://www.tutorialspoint.com/ebxml">ebXML</a> <br><a href="http://www.tutorialspoint.com/ajax">AJAX</a> <br><a href="http://www.tutorialspoint.com/perl">PERL</a><br></pre></p>
	
	<h3><b>The require() Function</b></h3>
	<p><ul><b>require()</b> เป็นฟังก์ชันเป็นการเอาข้อความเนื้อหาทั้งหมดในไฟล์ที่ระบุและคัดลอกมายังไฟล์ที่มีการใช้ฟังก์ชัน <b>require()</b> นั้น <br>หากมีปัญหาในการโหลดไฟล์ <b>require()</b> ฟังก์ชันจะมีการแจ้งความผิดพลาดและจะหยุดการรประมวลผลของสคริปนั้น</ul><p>
	<p><ul>ดังนั้นจึงไม่มีความแตกต่างระหว่าง <b>require()</b> และ <b>include()</b> ยกเว้นแต่การจัดการกับความผิดพลาด 
	<br>ซึ่ง <b>require()</b> จะเป็นที่แนะนำให้ใช้งานแทนการใช้ <b>include()</b> เพราะว่าสคริปไม่ควรประมวลผลต่อหากไม่มีไฟล์ที่ include หรือชื่อผิด</ul><p>
	<p><ul>คุณสามารถทดลองใช้ <b>require()</b> กับตัวอย่างด้านบน ซึ่งจะพบว่าให้ผลลัพธ์ที่เหมือนกัน แต่ถ้าหากลองทำตามตัวอย่างด้านล่างนี้ในกรณีที่ไม่มีไฟล์นั้นอยู่ คุณจะพบผลลัพธ์ที่ต่างกัน</ul><p>
	<p><pre>&lt;?php <b>include</b>(<z style="color:#009900;">"xxmenu.php"</z>); ?> <br><z style="color:#0000FF;">&lt;p&gt;</z>This is an example to show how to include wrong PHP file!<z style="color:#0000FF;">&lt;p&gt;</z></pre><p>
	<p>ซึ่งจะให้ผลลัพธ์ดังนี้</p>
	<p><pre>This is an example to show how to include wrong PHP file!</pre></p>
	<p>เมื่อทดลองในตัวอย่างเดียวกันด้วยการใช้ require()</p>
	<p><pre>&lt;?php <z style="color:#0000FF;"><b>require</b></z>(<z style="color:#009900;">"xxmenu.php"</z>); ?> <br><z style="color:#0000FF;">&lt;p&gt;</z>This is an example to show how to include wrong PHP file!<z style="color:#0000FF;">&lt;p&gt;</z></pre><p>
	<p>ในกรณีนี้การประมวลผลจะถูกหยุดลงและไม่มีการแสดงข้อความใดๆ</p>
	';
?>

<?php include('single.php'); ?>