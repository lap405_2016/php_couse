<?php
    $page = 8;
	$the_title = 'Arrays';
	$the_content = '<p>การที่จะเก็บขอ้ฒุลไว้ในส่วนกลางของโปรแกรม PHP นั้นเราจำเป็นต้องใช้สิ่งที่เรียกว่า variable(ตัวแปร)</p>
	</br>
	<p><u>ประเภทของ array</u></p>
	<p>1. Numeric array</p>
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;เป็น array ที่สามารถเก็บตัวเลข ตัวอักษร หรือแม้แต่ทุกๆ object โดยที่ index ของ array จะแสดงเป็นตัวเลข ซึ่งจะเริ่มที่ 0</p>
	<p>2. Associative array</p>
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;ลักษณะคล้ายกับ Numeric array แต่ index ของ array จะแสดงเป็นรูปแบบตัวอักษร ซึ่งสามารถสร้างความสัมพันธ์ระหว่าง key และ value(ชื่อ และค่าของมัน) ได้</p>
	<p>3. Multidimensional array</p>
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;เป็น array ที่สามารถสามารถมีองค์ประกอบย่อยภายใน(element) เป็น array ได้หรือเป็น sub-array นั่นเอง ทำให้สามารถที่จะใช้รูปแบบในการเก็บข้อมูลเป็นแบบ multiple index ได้</p><br>
    <p><u>การใช้งาน array แต่ละประเภท</u></p>
    <p><b>Numeric array</b></p>
    <pre>/*First method to create array.*/<br>&lt?php<br>   $numbers = array( 1, 2, 3, 4, 5);<br><br>   foreach( $numbers as $value ) {<br>      echo "Value is $value &ltbr/&gt";<br>   }<br>?&gt<br><br>/*Second method to create array.*/<br>&lt?php<br>   $numbers[0] = "one";<br>   $numbers[1] = "two";<br>   $numbers[2] = "three";<br><br>   foreach( $numbers as $value ) {<br>      echo "Value is $value &ltbr/&gt";<br>   }<br>?&gt</pre>
    <p>Result:<pre>Value is 1<br>Value is 2<br>Value is 3<br>Value is 4<br>Value is 5<br>Value is one<br>Value is two<br>Value is three<br></pre></p>
    <p><b>Associative array</b></p>
    <pre>/*First method to associate create array.*/<br>&lt?php<br>   $salaries = array("mohammad" => 2000, "qadir" => 1000, "zara" => 500);<br><br>   echo "Salary of mohammad is ". $salaries[\'mohammad\'] . &ltbr/&gt";<br>   echo "Salary of qadir is ".  $salaries[\'qadir\']. "&ltbr/&gt";<br>   echo "Salary of zara is ".  $salaries[\'zara\']. "&ltbr/&gt";<br>?&gt<br><br>/*Second method to create array.*/<br>&lt?php<br>   $salaries[\'mohammad\'] = "high";<br>   $salaries[\'qadir\'] = "medium";<br>   $salaries[\'zara\'] = "low";<br><br>   echo "Salary of mohammad is ". $salaries[\'mohammad\'] . "&ltbr/&gt";<br>   echo "Salary of qadir is ".  $salaries[\'qadir\']. "&ltbr/&gt";<br>   echo "Salary of zara is ".  $salaries[\'zara\']. "&ltbr/&gt";<br>?&gt</pre>
    <p>Result:<pre>Salary of mohammad is 2000<br>Salary of qadir is 1000<br>Salary of zara is 500<br>Salary of mohammad is high<br>Salary of qadir is medium<br>Salary of zara is low<br></pre></p>
    <p><b>Multidimensional array</b></p>
    <pre>&lt?php<br>   $marks = array(<br>      "mohammad" => array (<br>         "physics" => 35,<br>         "maths" => 30,<br>         "chemistry" => 39<br>      ),<br><br>      "qadir" => array (<br>         "physics" => 30,<br>         "maths" => 32,<br>         "chemistry" => 29<br>      ),<br>      "zara" => array (<br>         "physics" => 31,<br>         "maths" => 22,<br>         "chemistry" => 39<br>      ),<br>   );<br><br>   /*Accessing multi-dimensional array values*/<br>   echo "Marks for mohammad in physics : " ;<br>   echo $marks[\'mohammad\'][\'physics\'] . "&ltbr/&gt";<br><br>   echo "Marks for qadir in maths : ";<br>   echo $marks[\'qadir\'][\'maths\'] . "&ltbr/&gt";<br><br>   echo "Marks for zara in chemistry : " ;<br>   echo $marks[\'zara\'][\'chemistry\'] . "&ltbr/&gt";<br>?&gt</pre>
    <p>Result:<pre>Marks for mohammad in physics : 35<br>Marks for qadir in maths : 32<br>Marks for zara in chemistry : 39<br></pre></p>
    ';
?>

<?php include('single.php'); ?>