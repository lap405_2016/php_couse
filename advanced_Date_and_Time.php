<?php
	$page = 24;
	date_default_timezone_set('Asia/Bangkok');
	$the_title = 'Date & Time';
	$the_content = 	'<p>วันเวลาเป็นสิ่งที่พบเจอได้มากในชีวิตประจำวัน มันช่วยให้ชีวิตประจำวันง่ายขึ้นทำให้เราไม่ต้องนึกถึงเหตุการณ์ต่างๆว่าเกิดขึ้นเมื่อใด<br>PHP มีเครื่องมือเกี่ยวกับวันเวลาที่ทรงอานุภาพให้สำหรับการใช้งานและจัดการกับการคำนวณของวันและเวลา</p>
					<h3><b>Getting the Time Stamp with time()</b></h3>
					<p>ฟังก์ชัน time() เป็นฟังก์ชันที่ให้ข้อมูลทุกอย่างเกี่ยวกับวันและเวลาปัจจุบัน เป็นฟังก์ชันที่ไม่ต้องการพารามิเตอร์แต่ให้ผลลัพธ์กลับเป็นเลขจำนวนเต็ม</p>
					<p>เลขจำนวนเต็มที่ถูกให้ผลลัพธ์กลับโดยฟังก์ชัน time() นั้นเป็นตัวเลขที่แสดงถึงวินาทีที่ผ่านไปนับจาก เที่ยงคืน(Greenwich Mean Time,GMT) ของวันที่ 1 มกราคม 1970<br>ซึ่งเป็นที่รู้กันว่าเป็นสมัยของ UNIX และจำนวนวินาทีที่ว่านี้ถูกเรียกว่า time stamp</p>
					<p><pre>&lt;?php<br><ul><z style="color:#0000FF;">print</z> time();</ul>?&gt;</pre></p>
					<p>ซึ่งจะให้ผลลัพธ์ดังนี้</p>
					<p><pre>' . time() . '</pre></p>
					<p>ซึ่งสิ่งนี้เป็นสิ่งที่ค่อนข้างทำความเข้าใจได้ยาก แต่ PHP มีเครื่องมือที่ยอดเยี่ยมที่จะแปลง time stamp นี้ให้กลายเป็นรูปแบบที่มนุษย์เข้าใจได้ง่าย</p>
					<h3><b>Converting a Time Stamp with getdate()</b></h3>
					<p>ฟังก์ชัน <b>getdate()</b> สามารถรับ time stamp แล้วให้ผลลัพธ์กลับ array ที่ประกอบด้วยข้อมูลของวันเวลา แต่หากไม่ใส่ time stamp จะเป็นการใช้ time stamp ขณะนั้นซึ่งเหมือนกับการใช้ฟังก์ชัน time()</p>
					<p>ตารางต่อไปนี้เป็นการแสดงองค์ประกอบของ array ที่ให้ผลลัพธ์กลับโดยฟังก์ชัน <b>getdate()</b></p>
					<table class="table table-bordered">
					<tbody><tr><th width="10%">Sr.No</th><th width="80%">Key &amp; Description</th><th width="10%">Example</th></tr>

					<tr><td>1</td><td><p><b>seconds</b></p><p>วินาที(0-59)</p></td><td>'.getdate(time())['seconds'].'</td></tr>
					<tr><td>2</td><td><p><b>minutes</b></p><p>นาที(0 - 59)</p></td><td>'.getdate(time())['minutes'].'</td></tr>
					<tr><td>3</td><td><p><b>hours</b></p><p>ชั่วโมง(0 - 23)</p></td><td>'.getdate(time())['hours'].'</td></tr>
					<tr><td>4</td><td><p><b>mday</b></p><p>วันที่ของเดือน(1 - 31)</p></td><td>'.getdate(time())['mday'].'</td></tr>
					<tr><td>5</td><td><p><b>wday</b></p><p>วันที่ของสัปดาห์เริ่มนับจากวันอาทิตย์(0 - 6)</p></td><td>'.getdate(time())['wday'].'</td></tr>
					<tr><td>6</td><td><p><b>mon</b></p><p>เดือนที่ของปี(1 - 12)</p></td><td>'.getdate(time())['mon'].'</td></tr>
					<tr><td>7</td><td><p><b>year</b></p><p>ปีคริสตศักราช(4 digits)</p></td><td>'.getdate(time())['year'].'</td></tr>
					<tr><td>8</td><td><p><b>yday</b></p><p>วันที่ของปี( 0 - 365 )</p></td><td>'.getdate(time())['yday'].'</td></tr>
					<tr><td>9</td><td><p><b>weekday</b></p><p>วันของสัปดาห์</p></td><td>'.getdate(time())['weekday'].'</td></tr>
					<tr><td>10</td><td><p><b>month</b></p><p>เดือนของปี</p></td><td>'.getdate(time())['month'].'</td></tr>
					<tr><td>11</td><td><p><b>0</b></p><p>Timestamp</p></td><td>'.getdate(time())[0].'</td></tr></tbody></table>
					
					<p>ตอนนี้คุณสามารถควบคุมการใช้งานในส่วนของวันที่และเวลาได้แล้ว คุณสามารถจัดรูปแบบวันที่และเวลาตามที่คุณต้องการ</p>
					<h4><b>Example</b></h4>
					<p><pre>&lt;?php<br><ul>$date_array = getdate();<br><br><z style="color:#0000FF;">foreach </z>( $date_array <z style="color:#0000FF;">as</z> $key => $val ){<br><ul><z style="color:#0000FF;">print </z><z style="color:#009900;">"$key = $val&lt;br&gt;"</z>;</ul>}<br><br>$formated_date  = <z style="color:#009900;">"Today\'s date: "</z>;<br>$formated_date  .= $date_array[<z style="color:#009900;">\'mday\'</z>] . <z style="color:#009900;">"/"</z>;<br>$formated_date  .= $date_array[<z style="color:#009900;">\'mon\'</z>] . <z style="color:#009900;">"/"</z>;<br>$formated_date  .= $date_array[<z style="color:#009900;">\'year\'</z>];<br><br><z style="color:#0000FF;">print </z>$formated_date;(</ul>?&gt;</pre></p>
					<p>ซึ่งจากโค้ดจะให้ผลลัพธ์ดังนี้</p>
					<p><pre>seconds = '.getdate(time())['seconds'].'<br>minutes = '.getdate(time())['minutes'].'<br>hours = '.getdate(time())['hours'].'<br>mday = '.getdate(time())['mday'].'<br>wday = '.getdate(time())['wday'].'<br>mon = '.getdate(time())['mon'].'<br>year = '.getdate(time())['year'].'<br>yday = '.getdate(time())['yday'].'<br>weekday = '.getdate(time())['weekday'].'<br>month = '.getdate(time())['month'].'<br>0 = '.getdate(time())[0].'<br>Today\'s date: '.getdate(time())['mday'].'/'.getdate(time())['mon'].'/'.getdate(time())['year'].'</pre></p>
					<h3><b>Converting a Time Stamp with date()</b></h3>
					<p>ฟังก์ชัน<b>date()</b>จะส่งผลลัพธ์กลับด้วยรูปแบบของชุดอักษรที่แสดงถึงวันที่และเวลา คุณสามารถที่จะควบคุมรูปแบบการส่งผลลัพธ์กลับของฟังก์ชัน <b>date()ได้อิสระตามที่คุณต้องการโดยการส่งพารามิเตอร์ชุดอักษรที่จะใช้ควบคุมรูปแบบ</b></p>
					<p><pre>date(format,timestamp)</pre></p>
					<p>ฟังก์ชัน<b>date()</b>สามารถรับ time stamp ได้ แต่หากไม่ใส่ time stamp จะเป็นการใช้ time stamp ขณะนั้น  ในส่วนของ format หากใส่องค์ประกอบไปอย่างไรเมื่อฟังก์ชันส่งผลลัพธ์กลับมาจะได้ในรูปแบบที่ประกอบไปด้วยองค์ประกอบเหล่านั้น</p>
					<p>ซึ่งในตารางนี้จะประกอบไปองค์ประกอบที่จะใช้ได้ในรูปแบบ</p>
					
					<table class="table table-bordered">
					<tbody><tr><th width="10%">Sr.No</th><th width="60%">Format &amp; Description</th><th width="20%">Example</th></tr>
					<tr><td>1</td><td><p><b>a</b></p><p>]\'am\' หรือ \'pm\' ตัวพิมพ์เล็ก</p></td><td>'.date("a").'</td></tr>
					<tr><td>2</td><td><p><b>A</b></p><p>\'AM\' หรือ \'PM\' ตัวพิมพ์ใหญ่</p></td><td>PM</td></tr>
					<tr><td>3</td><td><p><b>d</b></p><p>วันที่ของเดือน จะขึ้นต้นด้วย 0 กรณีที่เป็นหลักเดียว</p></td><td>20</td></tr>
					<tr><td>4</td><td><p><b>D</b></p><p>วันของสัปดาห์(3ตัวอักษร)</p></td><td>Thu</td></tr>
					<tr><td>5</td><td><p><b>F</b></p><p>เดือนของปี</p></td><td>January</td></tr>
					<tr><td>6</td><td><p><b>h</b></p><p>ชั่วโมง รูปแบบแบบ 12 ชั่วโมง ขึ้นต้นด้วย 0 กรณีที่เป็นหลักเดียว</p></td><td>12</td></tr>
					<tr><td>7</td><td><p><b>H</b></p><p>ชั่วโมง รูปแบบแบบ 24 ชั่วโมง ขึ้นต้นด้วย 0 กรณีที่เป็นหลักเดียว</p></td><td>22</td></tr>
					<tr><td>8</td><td><p><b>g</b></p><p>ชั่วโมง รูปแบบแบบ 12 ชั่วโมง  </p></td><td>12</td></tr>
					<tr><td>9</td><td><p><b>G</b></p><p>ชั่วโมง รูปแบบแบบ 24 ชั่วโมง </p></td><td>22</td></tr>
					<tr><td>10</td><td><p><b>i</b></p><p>นาที ( 0 - 59 )</p></td><td>23</td></tr>
					<tr><td>11</td><td><p><b>j</b></p><p>วันที่ของเดือน</p></td><td>20</td></tr>
					<tr><td>12</td><td><p><b>l (Lower \'L\')</b></p><p>วันของสัปดาห์</p></td><td>Thursday</td></tr>
					<tr><td>13</td><td><p><b>L</b></p><p>Leap year (\'1\' คือใช่, \'0\'  คือไม่ )</p></td><td>1</td></tr>
					<tr><td>14</td><td><p><b>m</b></p><p>เดือนที่ของปี จะขึ้นต้นด้วย 0 กรณีที่เป็นหลักเดียว</p></td><td>1</td></tr>
					<tr><td>15</td><td><p><b>M</b></p><p>เดือนของปี(3ตัวอักษร)</p></td><td>Jan</td></tr>
					<tr><td>16</td><td><p><b>r</b></p><p>รูปแบบ RFC 2822</p></td><td>Thu, 21 Dec 2000 16:01:07 +0200</td></tr>
					<tr><td>17</td><td><p><b>n</b></p><p>เดือนที่ของปี</p></td><td>2</td></tr>
					<tr><td>18</td><td><p><b>s</b></p><p>วินาที</p></td><td>20</td></tr>
					<tr><td>19</td><td><p><b>U</b></p><p>Time stamp</p></td><td>948372444</td></tr>
					<tr><td>20</td><td><p><b>y</b></p><p>ปี(2ตัวอักษร)</p></td><td>06</td></tr>
					<tr><td>21</td><td><p><b>Y</b></p><p>ปี(4ตัวอักษร)</p></td><td>2006</td></tr>
					<tr><td>22</td><td><p><b>z</b></p><p>วันที่ของปี(0 - 365)</p></td><td>206</td></tr>
					<tr><td>23</td><td><p><b>Z</b></p><p>วินาทีที่ต่างจาก GMT</p></td><td>+5</td></tr></tbody></table>
					
					<h4><b>Example</b></h4>
					<p><pre>&lt;?php<br><br><ul><z style="color:#0000FF;">print </z>date(<z style="color:#009900;">"m/d/y G.i:s&lt;br&gt;"</z>, time());<br><z style="color:#0000FF;">print </z><z style="color:#009900;">"Today is "</z>;<br><z style="color:#0000FF;">print </z>date(<z style="color:#009900;">"j of F Y, \a\\t g.i a"</z>, time());</ul>?&gt;</pre></p>
					<p>ซึ่งจากโค้ดจะให้ผลลัพธ์ดังนี้</p>
					<p><pre>'.date("m/d/y G.i:s<br>", time()).'Today is '.date("j of F Y, \a\\t g.i a", time()).'</pre></p>';
?>

<?php include('single.php'); ?>