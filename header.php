<?php include('functions.php'); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php echo do_html_title($the_title); ?></title>
		<!-- Stylesheets -->
		<link rel="stylesheet" href="style.css">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

       <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
    </head>
	<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/apps/scripts/calendar.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
		<!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="index.html">
                        <img src="images/1459870313PHP-logo.svg.png" alt="logo" style="width:70px;height:40px;" /> </a>
                    <div class="menu-toggler sidebar-toggler"> </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">

                       <!--  <li class="dropdown dropdown-quick-sidebar-toggler">
                            <a href="index.html" class="dropdown-toggle">
                                <i class="icon-logout"></i>
                            </a>
                        </li> -->
                        <!-- END QUICK SIDEBAR TOGGLER -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->


        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
             <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                          
                            <li class="heading">
                                <h3 class="uppercase">PHP Tutorial</h3>
                            </li>
                            <li class="nav-item <?php if($page == 1){echo 'active open';}  ?>">
                                <a href="Introduction.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Introduction</span>
                                    <?php if($page == 1){echo "<span class='selected'></span>";} ?>
                                    
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 2){echo 'active open';}  ?>">
                                <a href="tutorial_Syntax_Overview.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Syntax Overview</span>
                                    <?php if($page == 2){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 3){echo 'active open';}  ?>">
                                <a href="VariableType.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Variable Types</span>   
                                    <?php if($page == 3){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 4){echo 'active open';}  ?>">
                                <a href="php-constants_types.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Constants Types</span>
                                    <?php if($page == 4){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 5){echo 'active open';}  ?>">
                                <a href="OperatorTypes.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Operator Types</span>
                                    <?php if($page == 5){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 6){echo 'active open';}  ?>">
                                <a href="Tutorial_Decision_Making.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Decision Making</span>
                                    <?php if($page == 6){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 7){echo 'active open';}  ?>">
                                <a href="LoopType.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Loop Types</span>
                                    <?php if($page == 7){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 8){echo 'active open';}  ?>">
                                <a href="tutorial_Arrays.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Arrays</span>
                                    <?php if($page == 8){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 9){echo 'active open';}  ?>">
                                <a href="php-strings.php" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Strings</span>
                                    <?php if($page == 9){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 10){echo 'active open';}  ?>">
                                <a href="php-Web_Concepts.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Web Concepts</span>
                                    <?php if($page == 10){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 11){echo 'active open';}  ?>">
                                <a href="Tutorial_Get&Post_Method.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - GET & POST Methods</span>
                                    <?php if($page == 11){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                           <li class="nav-item <?php if($page == 12){echo 'active open';}  ?>">
                                <a href="FileInclusion.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - File Inclusion</span>
                                    <?php if($page == 12){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 13){echo 'active open';}  ?>">
                                <a href="php-File_&_IO.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Files & I/O</span>
                                    <?php if($page == 13){echo "<span class='selected'></span>";} ?>
                                 </a>
                            </li>
                            <li class="nav-item <?php if($page == 14){echo 'active open';}  ?>">
                                <a href="tutorial_Functions.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Functions</span>
                                    <?php if($page == 14){echo "<span class='selected'></span>";} ?>
                                 </a>
                            </li>
                             <li class="nav-item <?php if($page == 15){echo 'active open';}  ?>">
                                <a href="Tutorial_Cookies.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Cookies</span>
                                    <?php if($page == 15){echo "<span class='selected'></span>";} ?>
                                 </a>
                            </li>
                             <li class="nav-item <?php if($page == 16){echo 'active open';}  ?>">
                                <a href="tutorial_Sessions.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Sessions</span>
                                    <?php if($page == 16){echo "<span class='selected'></span>";} ?>
                                 </a>
                            </li>
                             <li class="nav-item <?php if($page == 17){echo 'active open';}  ?>">
                                <a href="tutorial_SendingEmail.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Sending Emails</span>
                                    <?php if($page == 17){echo "<span class='selected'></span>";} ?>
                                 </a>
                            </li>
                             <li class="nav-item <?php if($page == 18){echo 'active open';}  ?>">
                                <a href="php-File_Uploading.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - File Uploading</span>
                                    <?php if($page == 18){echo "<span class='selected'></span>";} ?>
                                 </a>
                            </li>
                             <li class="nav-item <?php if($page == 19){echo 'active open';}  ?>">
                                <a href="CodingStandard.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Coding Standard</span>
                                    <?php if($page == 19){echo "<span class='selected'></span>";} ?>
                                 </a>
                            </li>


                            <li class="heading">
                                <h3 class="uppercase">Advanced PHP</h3>
                            </li>

                            <li class="nav-item <?php if($page == 20){echo 'active open';}  ?>">
                                <a href="php-predefined_variables.php" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Predefined Variables</span>
                                    <?php if($page == 20){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                           <li class="nav-item <?php if($page == 21){echo 'active open';}  ?>">
                                <a href="Advance_regular_expression.php" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Regular Expressions</span>
                                    <?php if($page == 21){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 22){echo 'active open';}  ?>">
                                <a href="advanced_Error_Handling.php" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Error Handling</span>
                                    <?php if($page == 22){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 23){echo 'active open';}  ?>">
                                <a href="Advance_Bugs_Debugging.php" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Bugs Debugging</span>
                                    <?php if($page == 23){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 24){echo 'active open';}  ?>">
                                <a href="advanced_Date_and_Time.php" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Date & Time</span>
                                    <?php if($page == 24){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 25){echo 'active open';}  ?>">
                                <a href="advance_php&mySQL.php" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP & MySQL</span>
                                    <?php if($page == 25){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 26){echo 'active open';}  ?>">
                                <a href="advanced_AJAX.php" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP & AJAX</span>
                                    <?php if($page == 26){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 27){echo 'active open';}  ?>">
                                <a href="Advance_XML.php" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP & XML</span>
                                    <?php if($page == 27){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                             <!-- <li class="nav-item  ">
                                <a href="layout_blank_page.html" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - Object Oriented</span>
                                </a>
                            </li>
                             <li class="nav-item  ">
                                <a href="layout_blank_page.html" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - For C Developers</span>
                                </a>
                            </li>
                             <li class="nav-item  ">
                                <a href="layout_blank_page.html" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">PHP - For PERL Developers</span>
                                </a>
                            </li> -->


                            <li class="heading">
                                <h3 class="uppercase">Phalcon Tutorial</h3>
                            </li>
                            
                            <li class="nav-item <?php if($page == 31){echo 'active open';}  ?>">
                                <a href="phalcon_installation.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">Phalcon Installation</span>
                                    <?php if($page == 31){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 32){echo 'active open';}  ?>">
                                <a href="phalcon_mvc.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">Phalcon MVC</span>
                                    <?php if($page == 32){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                            <li class="nav-item <?php if($page == 33){echo 'active open';}  ?>">
                                <a href="phalcon_form.php" class="nav-link nav-toggle">
                                    <i class="icon-docs"></i>
                                    <span class="title">Phalcon Form</span>
                                    <?php if($page == 33){echo "<span class='selected'></span>";} ?>
                                </a>
                            </li>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
            <!-- END SIDEBAR -->
			
			