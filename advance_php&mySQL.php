<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    background: #CAC5FF;
}
</style>

<?php
  $page = 25;
	$the_title = 'PHP & MySQL';
	$the_content = '<p>PHP สามารถที่จะทำงานร่วมกับทุกซอฟต์แวร์ฐานข้อมูล เช่น Oracle,Sybase แต่ที่นิยมใช้กันมากคือ MySQL</p>
	</br>
	<p><u>สิ่งที่ควรเตรียมก่อนเริ่มใช้งาน PHP กับ MySQL</u></p>
	<p>1. โปรแกรมเวอร์ชั่นล่าสุดของ MySQL</p>
	<p>2. พื้นฐานการใช้งาน MySQL</p>
	<p>3. สร้าง database ด้วยชื่อ user "guest" และ password "guest123"</p><br>
	<p><u>การเชื่อมต่อ MySQL database ด้วย PHP</u></p>
	<p><b>การเปิดการเชื่อมต่อฐานข้อมูล</b></p>
	<ul>ใน php จะมีการใช้ฟังก์ชัน mysql_connect เพื่อเปิดการเชื่อมต่อฐานข้อมูล โดยฟังก์ชันการใช้งานนี้จะ</ul>
	<p>ส่ง MySQL link identifier ถ้าเชื่อมต่อฐานข้อมูลได้สำเร็จ และส่งค่า FALSE ถ้าเชื่อมต่อกับฐานข้อมูลล้มเหลว</p>
	<p>Syntax(รูปแบบการใช้งาน) :</p>
	<pre>connection mysql_connect(server,user,passwd,new_link,client_flag);</pre>
	<table>
    <tr height="35">
    <th width="10%"><center>Sr.No</center></th>
    <th><center>Parameter &amp; Description</center></th>
    </tr>
    <tr valign="top">
    <td>&nbsp;&nbsp;1</td>
    <td>
    <p><b>&nbsp;&nbsp;server</b></p>
    <p>&nbsp;&nbsp;Optional &minus; The host name running database server. If not specified then default value is <b>localhost:3306</b>.</p>
    </td>
    </tr>
    <tr valign="top">
    <td>&nbsp;&nbsp;2</td>
    <td>
    <p><b>&nbsp;&nbsp;user</b></p>
    <p>&nbsp;&nbsp;Optional &minus; The username accessing the database. If not specified then default is the name of the user that owns</p>
    <p>&nbsp;&nbsp;the server process.</p>
    </td>
    </tr>
    <tr valign="top">
    <td>&nbsp;&nbsp;3</td>
    <td>
    <p><b>&nbsp;&nbsp;passwd</b></p>
    <p>&nbsp;&nbsp;Optional &minus; The password of the user accessing the database. If not specified then default is an empty password.</p>
    </td>
    </tr>
    <tr valign="top">
    <td>&nbsp;&nbsp;4</td>
    <td>
    <p><b>&nbsp;&nbsp;new_link</b></p>
    <p>&nbsp;&nbsp;Optional &minus; If a second call is made to mysql_connect() with the same arguments, no new connection will be</p>
    <p>&nbsp;&nbsp;established; instead, the identifier of the already opened connection will be returned.</p>
    </td>
    </tr>
    <tr valign="top">
    <td>&nbsp;&nbsp;5</td>
    <td>
    <p><b>&nbsp;&nbsp;client_flags</b></p>
    <p>&nbsp;&nbsp;Optional &minus; A combination of the following constants &minus;</p>
    <ul class="list">
    <li><p><b>MYSQL_CLIENT_SSL</b> &minus; Use SSL encryption</p></li>
    <li><p><b>MYSQL_CLIENT_COMPRESS</b> &minus; Use compression protocol</p></li>
    <li><p><b>MYSQL_CLIENT_IGNORE_SPACE</b> &minus; Allow space after function names</p></li>
    <li><p><b>MYSQL_CLIENT_INTERACTIVE</b> &minus; Allow interactive timeout seconds of inactivity before closing the connection</p></li>
    </ul>
    </td>
    </tr>
    </table>
    <p>*เพิ่มเติม : คุณสามารถกำหนด server, user, password ในไฟล์ php.ini ได้สำหรับการใช้งานในทุกๆ script โดยไม่ต้องเรียกซ้ำในทุกครั้งที่ใช้งานใหม่</p><br>
    <p><b>การปิดการเชื่อมต่อฐานข้อมูล</b></p>
    <ul>ใน php จะมีการใช้ฟังก์ชัน mysql_close เพื่อปิดการเชื่อมต่อฐานข้อมูล โดยฟังก์ชันการใช้งานนี้จะ</ul>
    <p>รับค่าจากฟังก์ชัน mysql_connect โดยถ้าได้รับค่า TRUE แสดงว่าเรียกใช้ฟังก์ชันนี้สำเร็จ และจะได้รับค่า</p>
    <p>FALSE เรียกใช้ฟังก์ชันนี้ล้มเหลว</p>
    <p>Syntax(รูปแบบการใช้งาน) :</p>
    <pre>bool mysql_close ( resource $link_identifier );</pre>
    <p>ตัวอย่างการใช้งานฟังก์ชัน</p>
    <pre>&lt;?php
           $dbhost = \'localhost:3036\';
           $dbuser = \'guest\';
           $dbpass = \'guest123\';
           $conn = mysql_connect($dbhost, $dbuser, $dbpass);

           if(! $conn ) {
              die(\'Could not connect: \' . mysql_error());
           }

           echo \'Connected successfully\';
           mysql_close($conn);
    ?&gt;</pre><br>
    <p><u>การสร้าง MySQL database ด้วย PHP</u></p>
    <p><b>การสร้าง database</b></p>
    <ul>เพื่อที่จะ สร้าง(create) และลบ(delete) MySQL database ใน php มีฟังก์ชันสำหรับการทำงาน</ul>
    <p>ในส่วนนี้คือ mysql_query ซึ่งจะมีสองพารามิเตอร์ในการเรียกใช้งาน ซึ่งจะให้ค่า TRUE ถ้าทำฟังก์ชันนี้</p>
    <p>สำเร็จ และให้ค่า FALSE ถ้าทำฟังก์ชันนี้ล้มเหลว </p>
    <p>Syntax(รูปแบบการใช้งาน) :</p>
    <pre>bool mysql_query( sql, connection );</pre>
    <table>
    <tr height="35">
    <th width="90"><center>Sr.No</center></th>
    <th width="450"><center>Parameter &amp; Description</center></th>
    </tr>
    <tr valign="top">
    <td>&nbsp;&nbsp;1</td>
    <td>
    <p><b>&nbsp;&nbsp;sql</b></p>
    <p>&nbsp;&nbsp;Required - SQL query to create a database</p>
    </td>
    </tr>
    <tr valign="top">
    <td>&nbsp;&nbsp;2</td>
    <td>
    <p><b>&nbsp;&nbsp;connection</b></p>
    <p>&nbsp;&nbsp;Optional - if not specified then last opend connection</p>
    <p>&nbsp;&nbsp;by mysql_connect will be used.</p>
    </td>
    </tr>
    </table>
    <p>ตัวอย่างการใช้งานฟังก์ชัน</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';
       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       echo \'Connected successfully\';

       $sql = \'CREATE Database test_db\';
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not create database: \' . mysql_error());
       }

       echo "Database test_db created successfully\n";
       mysql_close($conn);
?&gt;</pre><br>
    <p><b>การเลือกใช้ database</b></p>
    <ul>ในการเชื่อมต่อ MySQL database จำเป็นต้องมีการเลือกใช้ database ที่เฉพาะเจาะจงเพื่อใช้ใน</ul>
    <p>การทำงาน เพราะว่าเราสามารถที่จะจัดการ database ได้เพียง database เดียวในการใช้งานแต่ละครั้ง</p>
    <p>ใน php จะมีการใช้ฟังก์ชัน mysql_select_db เพื่อเลือก database ที่ต้องการเชื่อมต่อ โดย</p>
    <p>จะให้ค่า TRUE ถ้าทำฟังก์ชันนี้สำเร็จ และให้ค่า FALSE ถ้าทำฟังก์ชันนี้ล้มเหลว </p>
    <p>Syntax(รูปแบบการใช้งาน) :</p>
    <pre>bool mysql_select_db( db_name, connection );</pre>
    <table>
    <tr height="35">
    <th width="90"><center>Sr.No</center></th>
    <th width="670"><center>Parameter &amp; Description</center></th>
    </tr>
    <tr valign="top">
    <td>&nbsp;&nbsp;1</td>
    <td><p><b>&nbsp;&nbsp;db_name</b></p>
    <p>&nbsp;&nbsp;Required - Database name to be selected</p></td>
    </tr>
    <tr valign="top">
    <td>&nbsp;&nbsp;2</td>
    <td><p><b>&nbsp;&nbsp;connection</b></p>
    <p>&nbsp;&nbsp;Optional - if not specified then last opend connection by mysql_connect will be used.</p>
    </td>
    </tr>
    </table>
    <p>ตัวอย่างการใช้งานฟังก์ชัน</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'guest\';
       $dbpass = \'guest123\';
       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       echo \'Connected successfully\';

       mysql_select_db( \'test_db\' );
       mysql_close($conn);
?&gt;</pre><br>
    <p><b>การสร้างตารางใน database</b></p>
    <ul>ในการสร้างตารางใน database จะคล้ายกับการสร้าง database ใหม่ซึ่งในขั้นแรกของการสร้าง</ul>
    <p>ตารางใน database จะมีการเรียกใช้ฟังก์ชัน mysql_query</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';
       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       echo \'Connected successfully\';

       $sql = \'CREATE TABLE employee( \'.
          \'emp_id INT NOT NULL AUTO_INCREMENT, \'.
          \'emp_name VARCHAR(20) NOT NULL, \'.
          \'emp_address  VARCHAR(20) NOT NULL, \'.
          \'emp_salary   INT NOT NULL, \'.
          \'join_date    timestamp(14) NOT NULL, \'.
          \'primary key ( emp_id ))\';
       mysql_select_db(\'test_db\');
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not create table: \' . mysql_error());
       }

       echo "Table employee created successfully\n";

       mysql_close($conn);
?&gt;</pre>
    <p>ในกรณีนี้คุณสามารถที่จะสร้างหลายตารางได้โดยการสร้าง text file ขึ้นมาก่อนแล้ววาง หรือใส่คำสั่ง MySQL ใน text file นั้น </p>
    <p>จากนั้นทำการโหลดไฟล์ไปในตัวแปร $sql และทำการประมวลผลคำสั่ง MySQL</p>
    <p></p>
    <p>ดูการใช้งานได้จากตัวอย่างจากไฟล์ sql_query.txt file ตามข้างล่างนี้</p>
<pre>
CREATE TABLE employee(
   emp_id INT NOT NULL AUTO_INCREMENT,
   emp_name VARCHAR(20) NOT NULL,
   emp_address  VARCHAR(20) NOT NULL,
   emp_salary   INT NOT NULL,
   join_date    timestamp(14) NOT NULL,
   primary key ( emp_id ));
</pre>
        <p></p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';
       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       $query_file = \'sql_query.txt\';

       $fp = fopen($query_file, \'r\');
       $sql = fread($fp, filesize($query_file));
       fclose($fp);

       mysql_select_db(\'test_db\');
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not create table: \' . mysql_error());
       }

       echo "Table employee created successfully\n";
       mysql_close($conn);
?&gt;</pre><br>
    <p><u>การลบ MySQL database ด้วย PHP</u></p>
    <p>การลบ database สามารถเรียกใช้งานจากฟังก์ชัน mysql_query ได้</p>
    <p>ตัวอย่างการลบ database</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';
       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       $sql = \'DROP DATABASE test_db\';
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not delete database db_test: \' . mysql_error());
       }

       echo "Database deleted successfully\n";

       mysql_close($conn);
?&gt;</pre><br>
    <p><b>การลบตารางใน database</b></p>
    <ul>ในการลบตารางใน database วรทำอย่างระมัดระวัง เพราะคุณอาจจะเผลอลบข้อมูลส่วนที่สำคัญไปได้</ul>
    <p>ตัวอย่างการลบตารางใน database</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';
       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       $sql = \'DROP TABLE employee\';
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not delete table employee: \' . mysql_error());
       }

       echo "Table deleted successfully\n";

       mysql_close($conn);
?&gt;</pre><br>
    <p><u>การเพิ่มข้อมูลใน MySQL database ด้วย PHP</u></p>
    <p>การเพิ่มข้อมูลใน database ได้ผ่านคำสั่ง INSERT ของ SQL</p>
    <p>ผ่านการใช้งานฟังก์ชัน mysql_query ของ php</p>
    <p>ตัวอย่างการเพิ่มข้อมูลใน database</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';
       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       $sql = \'INSERT INTO employee \'.
          \'(emp_name,emp_address, emp_salary, join_date) \'.
          \'VALUES ( "guest", "XYZ", 2000, NOW() )\';

       mysql_select_db(\'test_db\');
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not enter data: \' . mysql_error());
       }

       echo "Entered data successfully\n";

       mysql_close($conn);
?&gt;</pre>
    <p>ในการใช้งานจริงนั้น ทุกๆข้อมูลสามารถนำมาด้วยรูปแบบ HTML ได้โดยข้อมูลเหล่านั้นจะถูกเก็บ หรือดึงมาด้วยสคริป์ของ PHP</p>
    <p>ซึ่งสามารถใช้งานได้ดังตัวอย่างข้างล่างนี้</p>
    <pre>
&lt;html&gt;
   &lt;head&gt;
      &lt;title&gt;Add New Record in MySQL Database&lt;/title&gt;
   &lt;/head&gt;

   &lt;body&gt;
      &lt;?php
         if(isset($_POST[\'add\'])) {
            $dbhost = \'localhost:3036\';
            $dbuser = \'root\';
            $dbpass = \'rootpassword\';
            $conn = mysql_connect($dbhost, $dbuser, $dbpass);

            if(! $conn ) {
               die(\'Could not connect: \' . mysql_error());
            }

            if(! get_magic_quotes_gpc() ) {
               $emp_name = addslashes ($_POST[\'emp_name\']);
               $emp_address = addslashes ($_POST[\'emp_address\']);
            }else {
               $emp_name = $_POST[\'emp_name\'];
               $emp_address = $_POST[\'emp_address\'];
            }

            $emp_salary = $_POST[\'emp_salary\'];

            $sql = "INSERT INTO employee ". "(emp_name,emp_address, emp_salary,
               join_date) ". "VALUES(\'$emp_name\',\'$emp_address\',$emp_salary, NOW())";

            mysql_select_db(\'test_db\');
            $retval = mysql_query( $sql, $conn );

            if(! $retval ) {
               die(\'Could not enter data: \' . mysql_error());
            }

            echo "Entered data successfully\n";

            mysql_close($conn);
         }else {
            ?&gt;

               &lt;form method = "post" action = "&lt;?php $_PHP_SELF ?&gt;"&gt;
                  &lt;table width = "400" border = "0" cellspacing = "1"
                     cellpadding = "2"&gt;

                     &lt;tr&gt;
                        &lt;td width = "100"&gt;Employee Name&lt;/td&gt;
                        &lt;td&gt;&lt;input name = "emp_name" type = "text"
                           id = "emp_name"&gt;&lt;/td&gt;
                     &lt;/tr&gt;

                     &lt;tr&gt;
                        &lt;td width = "100"&gt;Employee Address&lt;/td&gt;
                        &lt;td&gt;&lt;input name = "emp_address" type = "text"
                           id = "emp_address"&gt;&lt;/td&gt;
                     &lt;/tr&gt;

                     &lt;tr&gt;
                        &lt;td width = "100"&gt;Employee Salary&lt;/td&gt;
                        &lt;td&gt;&lt;input name = "emp_salary" type = "text"
                           id = "emp_salary"&gt;&lt;/td&gt;
                     &lt;/tr&gt;

                     &lt;tr&gt;
                        &lt;td width = "100"&gt;&nbsp;&lt;/td&gt;
                        &lt;td&gt;&nbsp;&lt;/td&gt;
                     &lt;/tr&gt;

                     &lt;tr&gt;
                        &lt;td width = "100"&gt;&nbsp;&lt;/td&gt;
                        &lt;td&gt;
                           &lt;input name = "add" type = "submit" id = "add"
                              value = "Add Employee"&gt;
                        &lt;/td&gt;
                     &lt;/tr&gt;

                  &lt;/table&gt;
               &lt;/form&gt;

            &lt;?php
         }
      ?&gt;

   &lt;/body&gt;
&lt;/html&gt;</pre><br>
    <p><u>การนำข้อมูลใน MySQL database มาใช้งานด้วย PHP</u></p>
    <p>ข้อมูลสามารถนำมาจากตารางใน database ได้ผ่านคำสั่ง SELECT ของ SQL ผ่านการใช้งานฟังก์ชัน</p>
    <p>mysql_query ของ php และในการใช้งานบ่อยครั้งจะมีการเรียกใช้ฟังก์ชัน mysql_fetch_array</p>
    <p>วึ่งเป็นฟังก์ชันในการคืนค่าข้อมูลใน array หรืออาจเป็น ตัวเลขในแต่ละแถวของตาราง</p>
    <p>ตัวอย่างการแสดงข้อมูลจากตาราง employee</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';

       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       $sql = \'SELECT emp_id, emp_name, emp_salary FROM employee\';
       mysql_select_db(\'test_db\');
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not get data: \' . mysql_error());
       }

       while($row = mysql_fetch_array($retval, MYSQL_ASSOC)) {
          echo "EMP ID :{$row[\'emp_id\']}  &lt;br&gt; ".
             "EMP NAME : {$row[\'emp_name\']} &lt;br&gt; ".
             "EMP SALARY : {$row[\'emp_salary\']} &lt;br&gt; ".
             "--------------------------------&lt;br&gt;";
       }

       echo "Fetched data successfully\n";

       mysql_close($conn);
?&gt;</pre>
    <p>ชื่อของแถวสามารถกำหนดเพื่อเรียกข้อมูลในแต่ละแถวได้โดยใช้ตัวแปร $row แล้วข้อมูลในแถวนั้นจะถูกแสดงออกมา</p><br>
    <p>ใน php ยังสามารถใช้ฟังก์ชันอื่นได้อย่างฟังก์ชัน mysql_fetch_assoc ที่ใช้สำหรับการคืนค่าในแถวของ associative array</p>
    <p>ตัวอย่างการใช้งานฟังก์ชันนี้ในรูปแบบการเรียกด้วยชื่อแถว</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';

       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       $sql = \'SELECT emp_id, emp_name, emp_salary FROM employee\';
       mysql_select_db(\'test_db\');
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not get data: \' . mysql_error());
       }

       while($row = mysql_fetch_assoc($retval)) {
          echo "EMP ID :{$row[\'emp_id\']}  &lt;br&gt; ".
             "EMP NAME : {$row[\'emp_name\']} &lt;br&gt; ".
             "EMP SALARY : {$row[\'emp_salary\']} &lt;br&gt; ".
             "--------------------------------&lt;br&gt;";
       }

       echo "Fetched data successfully\n";

       mysql_close($conn);
?&gt;</pre>
    <p>ตัวอย่างการใช้งานฟังก์ชันนี้ในรูปแบบการเรียกด้วยตัวเลขแถว</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';

       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       $sql = \'SELECT emp_id, emp_name, emp_salary FROM employee\';
       mysql_select_db(\'test_db\');
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not get data: \' . mysql_error());
       }

       while($row = mysql_fetch_array($retval, MYSQL_NUM)) {
          echo "EMP ID :{$row[0]}  &lt;br&gt; ".
             "EMP NAME : {$row[1]} &lt;br&gt; ".
             "EMP SALARY : {$row[2]} &lt;br&gt; ".
             "--------------------------------&lt;br&gt;";
       }

       echo "Fetched data successfully\n";

       mysql_close($conn);
?&gt;</pre><br>
    <p><b>การ Releasing Memory</b></p>
    <p>สามารถเรียกใช้งานได้โดยใช้คำสั่ง SELECT ของ SQL ผ่านคำสั่ง mysql_free_result ของ php</p>
    <p>ตัวอย่างการใช้งานฟังก์ชัน</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';

       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       $sql = \'SELECT emp_id, emp_name, emp_salary FROM employee\';
       mysql_select_db(\'test_db\');
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not get data: \' . mysql_error());
       }

       while($row = mysql_fetch_array($retval, MYSQL_NUM)) {
          echo "EMP ID :{$row[0]}  &lt;br&gt; ".
             "EMP NAME : {$row[1]} &lt;br&gt; ".
             "EMP SALARY : {$row[2]} &lt;br&gt; ".
             "--------------------------------&lt;br&gt;";
       }

       mysql_free_result($retval);
       echo "Fetched data successfully\n";

       mysql_close($conn);
?&gt;</pre>
    <p><u>การกำหนดเลขหน้าผ่าน PHP</u></p>
    <p>php สามารถให้คุณทำการแสดงผลจาก database ทีละหลายๆหน้าได้ในการแสดงนวมเป็นหน้ายาวๆหน้าเดียว</p>
    <p>สามารถกำหนดการใช้เลขหน้านี้ผ่านคำสั่ง LIMIT ของ SQL</p>
    <p>ตัวอย่างการใช้งานฟังก์ชันนี้</p>
    <pre>&lt;html&gt;
       &lt;head&gt;
          &lt;title&gt;Paging Using PHP&lt;/title&gt;
       &lt;/head&gt;

       &lt;body&gt;
          &lt;?php
             $dbhost = \'localhost:3036\';
             $dbuser = \'root\';
             $dbpass = \'rootpassword\';

             $rec_limit = 10;
             $conn = mysql_connect($dbhost, $dbuser, $dbpass);

             if(! $conn ) {
                die(\'Could not connect: \' . mysql_error());
             }
             mysql_select_db(\'test_db\');

             /* Get total number of records */
             $sql = "SELECT count(emp_id) FROM employee ";
             $retval = mysql_query( $sql, $conn );

             if(! $retval ) {
                die(\'Could not get data: \' . mysql_error());
             }
             $row = mysql_fetch_array($retval, MYSQL_NUM );
             $rec_count = $row[0];

             if( isset($_GET{\'page\'} ) ) {
                $page = $_GET{\'page\'} + 1;
                $offset = $rec_limit * $page ;
             }else {
                $page = 0;
                $offset = 0;
             }

             $left_rec = $rec_count - ($page * $rec_limit);
             $sql = "SELECT emp_id, emp_name, emp_salary ".
                "FROM employee ".
                "LIMIT $offset, $rec_limit";

             $retval = mysql_query( $sql, $conn );

             if(! $retval ) {
                die(\'Could not get data: \' . mysql_error());
             }

             while($row = mysql_fetch_array($retval, MYSQL_ASSOC)) {
                echo "EMP ID :{$row[\'emp_id\']}  &lt;br&gt; ".
                   "EMP NAME : {$row[\'emp_name\']} &lt;br&gt; ".
                   EMP SALARY : {$row[\'emp_salary\']} &lt;br&gt; ".
                   "--------------------------------&lt;br&gt;";
             }

             if( $page &gt; 0 ) {
                $last = $page - 2;
                echo "&lt;a href = \"$_PHP_SELF?page = $last\"&gt;Last 10 Records&lt;/a&gt; |";
                echo "&lt;a href = \"$_PHP_SELF?page = $page\"&gt;Next 10 Records&lt;/a&gt;";
             }else if( $page == 0 ) {
                echo "&lt;a href = \"$_PHP_SELF?page = $page\"&gt;Next 10 Records&lt;/a&gt;";
             }else if( $left_rec &lt; $rec_limit ) {
                $last = $page - 2;
                echo "&lt;a href = \"$_PHP_SELF?page = $last\"&gt;Last 10 Records&lt;/a&gt;";
             }

             mysql_close($conn);
          ?&gt;

       &lt;/body&gt;
&lt;/html&gt;</pre><br>
    <p><u>การอัพเดตข้อมูลไปที่ MySQL database</u></p>
    <p>ข้อมูลสามารถถูกอัพเดตไปยังตารางใน MySQL table ได้โดยการใช้คำสั่ง UPDATE ของ SQL ผ่านการ</p>
    <p>เรียกใช้ฟังก์ชัน mysql_query ของ php</p>
    <p>ตัวอย่างการใช้งานฟังก์ชันนี้</p>
    <pre>&lt;html&gt;
       &lt;head&gt;
          &lt;title&gt;Update a Record in MySQL Database&lt;/title&gt;
       &lt;/head&gt;

       &lt;body&gt;
          &lt;?php
             if(isset($_POST[\'update\'])) {
                $dbhost = \'localhost:3036\';
                $dbuser = \'root\';
                $dbpass = \'rootpassword\';

                $conn = mysql_connect($dbhost, $dbuser, $dbpass);

                if(! $conn ) {
                   die(\'Could not connect: \' . mysql_error());
                }

                $emp_id = $_POST[\'emp_id\'];
                $emp_salary = $_POST[\'emp_salary\'];

                $sql = "UPDATE employee ". "SET emp_salary = $emp_salary ".
                   "WHERE emp_id = $emp_id" ;
                mysql_select_db(\'test_db\');
                $retval = mysql_query( $sql, $conn );

                if(! $retval ) {
                   die(\'Could not update data: \' . mysql_error());
                }
                echo "Updated data successfully\n";

                mysql_close($conn);
             }else {
                ?&gt;
                   &lt;form method = "post" action = "&lt;?php $_PHP_SELF ?&gt;"&gt;
                      &lt;table width = "400" border =" 0" cellspacing = "1"
                         cellpadding = "2"&gt;

                         &lt;tr&gt;
                            &lt;td width = "100"&gt;Employee ID&lt;/td&gt;
                            &lt;td&gt;&lt;input name = "emp_id" type = "text"
                               id = "emp_id"&gt;&lt;/td&gt;
                         &lt;/tr&gt;

                         &lt;tr&gt;
                            &lt;td width = "100"&gt;Employee Salary&lt;/td&gt;
                            &lt;td&gt;&lt;input name = "emp_salary" type = "text"
                               id = "emp_salary"&gt;&lt;/td&gt;
                         &lt;/tr&gt;

                         &lt;tr&gt;
                            &lt;td width = "100"&gt;&nbsp;&lt;/td&gt;
                            &lt;td&gt;&nbsp;&lt;/td&gt;
                         &lt;/tr&gt;

                         &lt;tr&gt;
                            &lt;td width = "100"&gt;&nbsp;&lt;/td&gt;
                            &lt;td&gt;
                               &lt;input name = "update" type = "submit"
                                  id = "update" value = "Update"&gt;
                            &lt;/td&gt;
                         &lt;/tr&gt;

                      &lt;/table&gt;
                   &lt;/form&gt;
                &lt;?php
             }
          ?&gt;

       &lt;/body&gt;
&lt;/html&gt;</pre>
    <p><u>การลบข้อมูลจาก MySQL database</u></p>
    <p>ข้อมูลสามารถถูกลบจากตารางใน MySQL table ได้โดยการใช้คำสั่ง DELETE ของ SQL ผ่านการ</p>
    <p>เรียกใช้ฟังก์ชัน mysql_query ของ php</p>
    <p>ตัวอย่างการใช้งานฟังก์ชันนี้</p>
    <pre>&lt;html&gt;
       &lt;head&gt;
          &lt;title&gt;Delete a Record from MySQL Database&lt;/title&gt;
       &lt;/head&gt;
       &lt;body&gt;
          &lt;?php
             if(isset($_POST[\'delete\'])) {
                $dbhost = \'localhost:3036\';
                $dbuser = \'root\';
                $dbpass = \'rootpassword\';
                $conn = mysql_connect($dbhost, $dbuser, $dbpass);

                if(! $conn ) {
                   die(\'Could not connect: \' . mysql_error());
                }

                $emp_id = $_POST[\'emp_id\'];

                $sql = "DELETE FROM employee WHERE emp_id = $emp_id" ;
                mysql_select_db(\'test_db\');
                $retval = mysql_query( $sql, $conn );

                if(! $retval ) {
                   die(\'Could not delete data: \' . mysql_error());
                }

                echo "Deleted data successfully\n";

                mysql_close($conn);
             }else {
                ?&gt;
                   &lt;form method = "post" action = "&lt;?php $_PHP_SELF ?&gt;"&gt;
                      &lt;table width = "400" border = "0" cellspacing = "1"
                         cellpadding = "2"&gt;

                         &lt;tr&gt;
                            &lt;td width = "100"&gt;Employee ID&lt;/td&gt;
                            &lt;td&gt;&lt;input name = "emp_id" type = "text"
                               id = "emp_id"&gt;&lt;/td&gt;
                         &lt;/tr&gt;

                         &lt;tr&gt;
                            &lt;td width = "100"&gt;&nbsp;&lt;/td&gt;
                            &lt;td&gt;&nbsp;&lt;/td&gt;
                         &lt;/tr&gt;

                         &lt;tr&gt;
                            &lt;td width = "100"&gt;&nbsp;&lt;/td&gt;
                            &lt;td&gt;
                               &lt;input name = "delete" type = "submit"
                                  id = "delete" value = "Delete"&gt;
                            &lt;/td&gt;
                         &lt;/tr&gt;

                      &lt;/table&gt;
                   &lt;/form&gt;
                &lt;?php
             }
          ?&gt;

       &lt;/body&gt;
&lt;/html&gt;</pre><br>
    <p><u>การสำรองข้อมูลของ MySQL database</u></p>
    <p>เพื่อการใช้งาน database อย่างมีประสิทธิภาพ และปลอดภัยจากการทำงานผิดพลาดต่างๆ เราสามารถทำการ</p>
    <p>สำรองข้อมูลเผื่อไว้ในกรณีที่เกรงว่าจะทำงานใดๆผิดพลาดได้ ได้โดยการใช้คำสั่ง SELECT ของ SQL ในการ</p>
    <p>สำรองข้อมูลในตารางต่างๆได้ แต่อาจต้องใช้คำสั่ง query แยกกันในแต่ละตาราง</p>
    <p>ตัวอย่างการใช้งานฟังก์ชันนี้</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';

       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       $table_name = "employee";
       $backup_file  = "/tmp/employee.sql";
       $sql = "SELECT * INTO OUTFILE \'$backup_file\' FROM $table_name";

       mysql_select_db(\'test_db\');
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not take data backup: \' . mysql_error());
       }

       echo "Backedup  data successfully\n";

       mysql_close($conn);
?&gt;</pre>
    <p>แล้วเราสามารถที่จะเรียกคืนข้อมูลที่เราสำรองไว้ได้ โดยใช้คำสั่ง LOAD DATA INFILE ของ SQL ดังนี้</p>
<pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';

       $conn = mysql_connect($dbhost, $dbuser, $dbpass);

       if(! $conn ) {
          die(\'Could not connect: \' . mysql_error());
       }

       $table_name = "employee";
       $backup_file  = "/tmp/employee.sql";
       $sql = "LOAD DATA INFILE \'$backup_file\' INTO TABLE $table_name";

       mysql_select_db(\'test_db\');
       $retval = mysql_query( $sql, $conn );

       if(! $retval ) {
          die(\'Could not load data : \' . mysql_error());
       }
       echo "Loaded  data successfully\n";

       mysql_close($conn);
?&gt;</pre><br>
    <p><b>การใช้งาน MySQL binary mysqldump ผ่าน php</b></p>
    <p>ตัวอย่างการ dump ใน php</p>
    <pre>&lt;?php
       $dbhost = \'localhost:3036\';
       $dbuser = \'root\';
       $dbpass = \'rootpassword\';

       $backup_file = $dbname . date("Y-m-d-H-i-s") . \'.gz\';
       $command = "mysqldump --opt -h $dbhost -u $dbuser -p $dbpass ". "test_db | gzip &gt; $backup_file";

       system($command);
?&gt;</pre><br>
    <p><b>* คุณสามารถที่จะเรียกใช้งานคำสั่งที่ได้สอนไปในข้างต้นผ่าน phpMyAdmin โดยใช้ User Interface ของเขาได้เลย</b></p>
    ';
?>

<?php include('single.php'); ?>