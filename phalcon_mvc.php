<?php
	$page = 32;
	$the_title = 'Phalcon MVC';
	$the_content = "
	<div>
		<p>
		<ul>
			Phalcon Framework นั้นรองรับการทำงานแบบ MVC(Model View Controller) ทำให้สามารถแบ่งการทำงานได้ง่าย เพื่อความสะดวกยิ่งขึ้น
		</ul>
		</p>
		<p>
		<ul>
			ในบทนี้จะสอนการสร้างไฟล์ Model View Controller และการใช้เบื้องต้น
		</ul>
		</p>
	</div>

	<div>
		<p>
		<ul>
			<h3>1. การสร้างไฟล์ Controllers </h3><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbspการสร้างไฟล์ Controller บน Phalcon มีอยู่ 2 แบบด้วยกัน แบบแรก Controller แบบเดียว ๆ และแบบที่สอง Controller แบบสืบทอด Object จาก Controller หลัก 
			<ul> <br>
				<b>คำสั่งไฟล์ Controller แบบเดียว</b> <br><br>
				สร้างไฟล์ Controller ชื่อว่า 'ExampleController.php' เก็บไว้ในไดเรคทอรี่ [root]\app\controllers แล้วพิมพ์ตามรูปด้านล่าง<br><br>
				<img src='images/Capture1.PNG' class='img-responsive' > <br><br>
				
			</ul>
			<ul> <br>
				<b>คำสั่งไฟล์ Controller แบบสืบทอด Object จาก Controller หลัก </b> <br><br>
				สร้างไฟล์ Controller หลัก ชื่อว่า 'ControllerBase.php' เก็บไว้ในไดเรคทอรี่ [root]\app\controllers <br><br>
				<img src='images/Capture2.PNG' class='img-responsive' > <br><br>

				จากนั้นสร้างไฟล์ Controller ชื่อว่า 'ExampleController.php' เก็บไว้ในไดเรคทอรี่ [root]\app\controllers และเขียนคำสั่งสืบทอด Object จากไฟล์ ControllerBase.php เข้ามา ซึ่งจะทำให้สามารถเรียกใช้ Method ของไฟล์ ControllerBase.php ได้จากไฟล์ ExampleController.php ได้เลย<br><br>
				<img src='images/Capture3.PNG' class='img-responsive' > <br><br>
				ข้อดีของการสืบทอดแบบนี้ คือไม่ต้องเขียนฟังก์ชั่นหรือ Method ที่ทำงานเหมือนๆกันหลายครั้ง สามารถเขียนเก็บไว้ใน Controller หลัก แล้วค่อยสืบทอดฟังก์ชั่นหรือ Method มาใช้ได้

			</ul>
		</ul>
		</p>
		<p>
		<ul>
			<h3>2. การสร้างไฟล์ Models </h3><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbspการสร้างไฟล์ Models บน Phalcon สร้างเพื่อนำไปใช้ในการติดต่อกับฐานข้อมูล ซึ่งเราจะสามารถดึงข้อมูลจากฐานข้อมูลผ่านทาง Models ที่เราสร้าง
			<ul> <br>
				<b>คำสั่งตัวอย่างไฟล์ Model</b> <br><br>
			
				<img src='images/Capture4.PNG' class='img-responsive' > <br><br>
				
			</ul>
		</ul>
		</p>
		<p>
		<ul>
			<h3>3. การสร้างไฟล์ Views </h3><br>
			&nbsp&nbsp&nbsp&nbsp&nbsp&nbspการสร้างไฟล์ Views นั้น มีกฏสำคัญอยู่ 1 ข้อที่สำคัญ คือ ไฟล์ Views ของแต่ละ Controller นั้น ๆ จะต้องมีไฟล์ Views อยู่ในโฟล์เดอร์ที่มีชื่อตรงกับชื่อของ Controller เช่น ไฟล์ ExampleController.php ไฟล์ Views จะต้องอยู่ภายในโฟล์เดอร์ 'example' เท่านั้น ซึ่งฟล์ Views จะถูกเก็บไว้ในไดเรคทอรี่ [root]\app\views ทั้งหมด
			<ul> <br>
				<b>สร้างไฟล์ Views ชื่อว่า 'index.volt'</b> <br><br>
			
				<img src='images/Capture5.PNG' class='img-responsive' > <br><br>

				จะต้องสร้างไว้ในโฟร์เดอร์ตามรูปด้านล่างนี้ <br><br>

				<img src='images/Capture6.PNG' class='img-responsive' > <br><br>

				เพื่อตรวจสอบว่าเราสร้างไฟล์ View สำเร็จหรือไม่ ให้เราเข้าไปที่ URL http://localhost/phalcon_demo/example เพื่อเรียกใช้งานไฟล์ ExampleController.php ที่เราสร้างไว้ หากสำเร็จจะได้ตามรูปด้านล่างนี้<br><br>

				<img src='images/Capture7.PNG' class='img-responsive' > <br><br>
				
			</ul>
		</ul>
		</p>

	</div>

	";
?>

<?php include('single.php'); ?>